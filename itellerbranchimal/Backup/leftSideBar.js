import React, {Component} from "react";
import userLogo from '../user.png';
import {NavLink} from 'react-router-dom';
const userName = localStorage.getItem('UserName')
class LeftSideBar extends Component {   
  render(){
    return (
      <div>
    <aside>
    <div id="sidebar" className="nav-collapse ">
      <ul className="sidebar-menu" id="nav-accordion">
        <p className="centered"><img src={userLogo} className="img-circle" width="80"/></p>
        <h5 className="centered">{userName}</h5>
        <li className="mt">
          <NavLink to="/" className="active">
            <i className="fa fa-dashboard"></i>  <span>Dashboard</span></NavLink>
        </li>
        <li className="sub-menu">
          <a href="javascript:;">
            <i className="fa fa-cogs"></i>  <span>Setup</span>
            </a>
          <ul className="sub">
        {/*  <li>
          <NavLink to="/pages/setup/user"> <i className="fa fa-users"></i> User</NavLink>
          </li>*/}
          <li>
          <NavLink to="/pages/setup/roleResources"> <i className="fa fa-th"></i> Role Resources</NavLink>
          </li>
         {/* <li>  <li>
          <NavLink to="/pages/setup/branch"> <i className="fa fa-bank"></i> Branch</NavLink>
          </li>
          <li>
          <NavLink to="/pages/setup/branch"> <i className="fa fa-money"></i> GL Account</NavLink>
          </li>*/}
            <li>
            <NavLink to="/pages/setup/cashDenominations"> <i className="fa fa-money"></i> Currency</NavLink>
            </li>
           {/* <li>
            <NavLink to="/pages/setup/till"> <i className="fa fa-bookmark"></i>  Till</NavLink>
            </li>
            <li>
          <NavLink to="/pages/setup/assignTill"> <i className="fa fa-tasks"></i>  Assign Till</NavLink>
          </li>*/}
          </ul>
        </li>
        <li className="sub-menu">
        <NavLink to="/pages/vaultMovement/vaultIn" className="active">
          <i className="fa fa-exchange"></i>  <span>Vault Movement</span></NavLink>
      </li>
        {/* <li className="sub-menu">
        <a href="javascript:;">
          <i className="fa fa-exchange"></i>  <span>Vault Movement</span>
          </a>
        <ul className="sub">
        <li>
        <NavLink to="/pages/vaultMovement/vaultIn"> <i className="fa fa-arrow-circle-right"></i> Vault In</NavLink>
        </li>
        <li>
        <NavLink to="/pages/vaultMovement/vaultOut"> <i className="fa fa-arrow-circle-left"></i> Vault Out</NavLink>
        </li>
        </ul>
      </li>*/}
        <li className="sub-menu">
        <a href="javascript:;">
          <i className="fa fa-tasks"></i>  <span>Management</span>
          </a>
        <ul className="sub">
       {/* <li>
        <NavLink to="/pages/management/openTill"> <i className="fa fa-tasks"></i> Open Till</NavLink>
        </li>*/}
        <li>
        <NavLink to="/pages/management/closeTill"> <i className="fa fa-tasks"></i> Close Till</NavLink>
        </li>
        <li>
        <NavLink to="/pages/management/balance"> <i className="fa fa-balance-scale"></i> Check Balance</NavLink>
        </li>
       {/* <li>
       <NavLink to="/pages/management/tillrequest"> <i className="fa fa-tasks"></i> Till Request</NavLink>
        </li>*/}
         <li>
          <NavLink to="/pages/management/transferTill"> <i className="fa fa-exchange"></i>  Till Transfer</NavLink>
          </li> 
        </ul>
      </li>
        
        <li className="sub-menu">
        <a href="javascript:;">
          <i className="fa fa-credit-card"></i>  <span>Transaction</span>
          </a>
        <ul className="sub">
        <li>
        <NavLink to="/pages/cashWithdrawal">
          <i className="fa fa-money"></i>  <span>Cash Withdrawal</span>
          </NavLink>
      </li>
      <li>
      <NavLink to="/pages/depositWithdrawal">
        <i className="fa fa-bank"></i>  <span>Deposit</span>
        </NavLink>
    </li>
    <li>
    <NavLink to="/pages/reverseTransaction">
      <i className="fa fa-arrow-circle-o-left"></i>  <span>Reverse Transaction</span>
      </NavLink>
  </li>
        </ul>
      </li>


      <li className="sub-menu">
      <a href="javascript:;">
        <i className="fa fa-thumbs-up"></i>  <span>Approvals</span>
        </a>
      <ul className="sub">
      {/* <li>
      <NavLink to="/pages/Approval/openTillApproval">
        <i className="fa fa-folder-open-o"></i>  <span>Open Till</span>
        </NavLink>
    </li>*/}
  <li>
  <NavLink to="/pages/Approval/tillApproval">
    <i className="fa fa-ban"></i>  <span>Close Till</span>
    </NavLink>
    <NavLink to="/pages/Approval/balanceApproval">
    <i className="fa fa-balance-scale"></i>  <span>Till Balance</span>
    </NavLink>
</li>
<li>
  <NavLink to="/pages/Approval/transactionApproval">
    <i className="fa fa-money"></i>  <span>Transaction</span>
    </NavLink>
</li>
      </ul>
    </li>

      </ul>
    </div>
  </aside>
      </div>
    )
  }
}

export default LeftSideBar