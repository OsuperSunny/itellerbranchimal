import React from 'react';
import ReactDom from 'react-dom'
import {Route,Link,BrowserRouter as Router, Switch} from 'react-router-dom';
import { createBrowserHistory } from 'history'
import LeftSideBar from './shared/leftSideBar';
import TopBar from './shared/topBar';
import Footer from './shared/footer';
import Dashboard from './pages/dashboard';
import CashDenominations from './pages/setup/cashDenominations';
import RoleResources from './pages/setup/roleResources';
import AssignTill from './pages/setup/assignTill';
import CashWithdrawal from './pages/cashWithdrawal';
import CashWithdrawalWithCheque from './pages/cashwithdrawalCheque';
import DepositWithdrawal from './pages/depositWithdrawal';
import ReverseTransaction from './pages/reverseTransaction';
import Till from './pages/setup/till';
import TillTransfer from './pages/management/transferTill';
import TillRequest from './pages/management/tillrequest';
import CloseTill from './pages/management/closeTill';
import OpenTill from './pages/management/openTill';
import VaultIn from './pages/vaultMovement/vaultIn';
import VaultOut from './pages/vaultMovement/vaultOut';
import User from './pages/setup/user';
import Branch from './pages/setup/branch';
import TillApproval from './pages/Approval/tillApproval';
import BalanceApproval from './pages/Approval/balanceApproval';
import OpenTillApproval from './pages/Approval/openTillApproval';
import TransactionApproval from './pages/Approval/transactionApproval';
import AcceptTill from './pages/Approval/acceptTillTransfer';
import Balance from './pages/management/balance';
import VaultReport from './pages/Reports/vaultReport';
import TillTransferReport from './pages/Reports/tillTransferReport';
import TransactionReport from './pages/Reports/transactionReport';
import TellerCast from './pages/Reports/tellerCast';
import CallOver from './pages/Reports/callOver';
import AuditTrail from './pages/auditTrail';
const history = createBrowserHistory();
function App() {
  return (
    <Router history={history}>
    <div>
     <TopBar></TopBar>
     <LeftSideBar></LeftSideBar>
     <Route exact path="/" component={Dashboard}/>
     <Route  path="/pages/setup/cashDenominations" component={CashDenominations}/>
     <Route  path="/pages/setup/roleResources" component={RoleResources}/>
     <Route  path="/pages/setup/user" component={User}/>
     <Route  path="/pages/setup/branch" component={Branch}/>
     <Route  path="/pages/setup/assignTill" component={AssignTill}/>
     <Route  path="/pages/setup/till" component={Till}/>
     <Route  path="/pages/management/transferTill" component={TillTransfer}/>
     <Route  path="/pages/management/tillrequest" component={TillRequest}/>
     <Route  path="/pages/management/closeTill" component={CloseTill}/>
     <Route  path="/pages/management/openTill" component={OpenTill}/>
     <Route  path="/pages/management/balance" component={Balance}/>
     <Route  path="/pages/vaultMovement/vaultIn" component={VaultIn}/>
     <Route  path="/pages/vaultMovement/vaultOut" component={VaultOut}/>
     <Route  path="/pages/cashWithdrawal" component={CashWithdrawal}/>
     <Route  path="/pages/cashwithdrawalCheque" component={CashWithdrawalWithCheque}/>
     <Route  path="/pages/depositWithdrawal" component={DepositWithdrawal}/>
     <Route  path="/pages/reverseTransaction" component={ReverseTransaction}/>
     <Route  path="/pages/Approval/tillApproval" component={TillApproval}/>
     <Route  path="/pages/Approval/balanceApproval" component={BalanceApproval}/>
     <Route  path="/pages/Approval/openTillApproval" component={OpenTillApproval}/>
     <Route  path="/pages/Approval/transactionApproval" component={TransactionApproval}/>
     <Route  path="/pages/Approval/acceptTillTransfer" component={AcceptTill}/>
     <Route  path="/pages/Reports/vaultReport" component={VaultReport}/>
     <Route  path="/pages/Reports/tillTransferReport" component={TillTransferReport}/>
     <Route  path="/pages/Reports/transactionReport" component={TransactionReport}/>
     <Route  path="/pages/Reports/tellerCast" component={TellerCast}/>
     <Route  path="/pages/Reports/callOver" component={CallOver}/>
     <Route  path="/pages/auditTrail" component={AuditTrail}/>
    </div>
    </Router>
  );
}

export default App;
