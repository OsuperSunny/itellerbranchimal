import React, {Component} from "react";
import MockJson from '../../apiService/mockJson';
import Select from 'react-select';
import axios from 'axios';
import apiUrl from '../../apiService/config'
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import GenerateToken from '../../shared/token';
import $ from 'jquery';
var Till =[];
const userName = localStorage.getItem('Id');

function loadAmountFormat(){
  $("input[data-type='currency']").on({
    keyup: function() {
      console.log("exec")
      formatCurrency($(this));
    },
    blur: function() { 
      console.log("exec")
      formatCurrency($(this), "blur");
    }
});

function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val =  left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val =  input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ".00";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}  
}

class VaultOut extends Component {
  constructor (props){
    super(props)
    this.state = {
    ID:0,
    Amount: '0.00',
    CashDenominations:[],
    CurrencyCode:'',
    Currency:'',
    SelectedCurrency:'',
    UserId:'',
    ShowError: false,
    ErrorMessage:'',
    Remarks:'',
    TellerID:'',
    UserDetails:{},
    TransRef:'',
    VaultExecuting:false,
    ShowNairaEquivalentButton: false,
    CurrencyRateDetails:{},
    ConvertedAmount:0.00,
    DebitCurrencyCode:''
}
}

ChangeTeller = e =>{
  this.setState({[e.target.name]: e.target.value});
}

ChangeRemarks = e =>{
  this.setState({[e.target.name]: e.target.value});
}

ChangeAmount = e => {
  let amount = parseFloat(e.target.value.replace(/,/g, ''));
  this.setState({
    [e.target.name]: amount
  })
}

ClearState = e => {
  this.setState({   WithdrawalAmount:0.00,
    TillNum:'',
    GLAccount:'',
    CurrencyCode:0,
    UserId:''})
    this.setState({ShowError: false, ErrorMessage:''})
}

ChangeDebitCurrency = e =>{
  console.log(e.target.value);
  let currencyCode = e.target.value;
  // if(currencyCode !== 'NGN' && currencyCode !== '0'){
  //   this.setState({ShowNairaEquivalentButton: true})
  // }else{
  //   this.setState({ShowNairaEquivalentButton: false})
  // }
  this.setState({DebitCurrencyCode: currencyCode})

}

ChangeCreditCurrency = e =>{
  console.log(e.target.value);
  let currencyCode = e.target.value;
  // if(currencyCode !== 'NGN' && currencyCode !== '0'){
  //   this.setState({ShowNairaEquivalentButton: true})
  // }else{
  //   this.setState({ShowNairaEquivalentButton: false})
  // }
  this.setState({CurrencyCode: currencyCode})
}
ConvertDate(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();
  today = yyyy + '-' + mm + '-' + dd;
  console.log(today);
  return today
}

CheckNairaEquivalent = e =>{
  let currentComponent = this;
  let convertedAmount = this.state.ConvertedAmount;
  if(this.state.Amount === '0.00' || this.state.Amount <= 0){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Please input an amount.',VaultExecuting:false})
    return;
  }

  axios.get(apiUrl.BankService.GetCurrencyRate + this.state.CurrencyCode + '/Token/' + localStorage.getItem('access_token'))
  .then(function(response){
    console.log(response.data.CurrencyRateDetails);
    convertedAmount = parseFloat(response.data.CurrencyRateDetails.CCY_SELL_RATE) * currentComponent.state.Amount
    currentComponent.setState({CurrencyRateDetails:response.data.CurrencyRateDetails, ConvertedAmount: convertedAmount})
    window.$('#NairaEquivalentModal').modal('toggle')
  })
  
}
SendRequest = e =>{
  let currentComponent = this;
  currentComponent.setState({ShowError:false, ErrorMessage: '',VaultExecuting:true})
  if(Object.keys(this.state.UserDetails).length === 0){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Session timeout. Please Lock screen or Login again.',VaultExecuting:false})
    return;
  }
  if(this.state.TellerID === ''){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Please Teller ID must not be empty.',VaultExecuting:false})
    return;
  }
  if(this.state.CurrencyCode === ''){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Please select a currency for the amount.',VaultExecuting:false})
    return;
  }
  if(this.state.Amount === '0.00'){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Please input an amount.',VaultExecuting:false})
    return;
  }
  if(this.state.Remarks === ''){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Please Remarks must not be empty.',VaultExecuting:false})
    return;
  }
  if(this.state.Remarks.length > 34){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Please Remarks character must not be graeter than 34 character',VaultExecuting:false})
    return;
  }

  // let vaultData = { "Amount": parseFloat(this.state.Amount) ,"TellerId":this.state.TellerID,"Remark":this.state.Remarks, "Branch": this.state.UserDetails.UserTillBranch, "CurrCode": this.state.CurrencyCode, "ToTellerId":this.state.UserDetails.Teller_ID, "access_token": localStorage.getItem('access_token')}
  // console.log(vaultData);

  let vaultData = {"Beneficiary": "","TransRef":"","PhoneNo":"","TransName":this.state.UserDetails.UserName, "AccountNo": "","Amount": parseFloat(this.state.Amount),"TransType": "1","TellerId": this.state.TellerID,"CustomerAcctNos": "","TotalAmt": parseFloat(this.state.Amount),"WithdrawerName": this.state.UserDetails.UserName,"WithdrawerPhoneNo": "","Status": 64,"CashierID": localStorage.getItem('Id'),"CashierTillNos": this.state.UserDetails.Teller_ID,"CashierTillGL": this.state.UserDetails.Teller_ID,"WhenApproved": currentComponent.ConvertDate(new Date()) ,"SortCode": "","Currency": 1,"ValueDate": currentComponent.ConvertDate(new Date()) ,"SupervisoryUser": "","ChequeNo": "","DateOnCheque": currentComponent.ConvertDate(new Date()),"Remark":this.state.Remarks, "Narration": this.state.Remarks,"CreationDate": currentComponent.ConvertDate(new Date()),"MachineName": "",
  "TillTransferID":0,"IsTillTransfer":false,"NeededApproval":false, "InitiatorName": this.state.UserDetails.UserName, "IsT24": false,"Branch": this.state.UserDetails.UserTillBranch, "CurrCode": this.state.CurrencyCode, "ToTellerId":this.state.UserDetails.Teller_ID, "access_token":localStorage.getItem('access_token'),TransactionParty:"",IsVaultIn: false,"GLAccountNo": "","CBACode": "","CBA": "IMAL","DisapprovalReason":"","DisapprovedBy":"","ApprovedBy": "","WhenDisapproved":"","CurrencyAbbrev": this.state.CurrencyCode,"Address": "", TransactionDetailsModels:[]}

  axios.post(apiUrl.BankService.CreateVault,vaultData)
  .then(function(response){
    console.log(response.data);
    if(response.data.success === true){
      currentComponent.setState({TellerID:'',Remarks:'',Amount:'0.00', TransRef: response.data.TransactionRef,VaultExecuting:false})
      window.$('#TransactionSuccessModal').modal('toggle')
     NotificationManager.success('Transaction succssful', 'Success',1000)
    }else{
      currentComponent.setState({VaultExecuting:false})
      NotificationManager.error(response.data.message, 'Error',1000)
    }
  }).catch(function(error){
    currentComponent.setState({VaultExecuting:false})
    NotificationManager.error('Error Ocurred', 'Error',1000)
    console.log(error);
  })
  
}

GetDate(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();

  today = dd  + '/' + mm + '/' + yyyy;
  console.log(today);
  return today
}

  async componentDidMount(){
    let currentComponent = this;
    await GenerateToken();
    loadAmountFormat();
    await axios.get(apiUrl.Security.GetCurrency)
    .then(function(response){
      let getDenominations = response.data.data;
      console.log(getDenominations);
      currentComponent.setState({CashDenominations:getDenominations})
    })
  
   await axios.get(apiUrl.BankService.GetUserDetails + 'userId=' + localStorage.getItem("Id") +  '&access_token=' + localStorage.getItem('access_token'))
    .then(function(response){
      console.log(response.data);
      if(response.data.success){
        currentComponent.setState({UserDetails: response.data.UserDetails})
      }
      
    }).catch(function(error){
      console.log(error);
    })

  }
  render(){
    return (
      <div>
      <NotificationContainer/>
      <section id="main-content">
      <section className="wrapper">
        <div className="row">
        <div className="col-md-12">
        <h3 className="page-title">
        Vault Out <small></small>
        </h3>
        </div>
        </div>
        <div className="row">
     
        </div><br/> <br/>
        {
          this.state.ShowError === true ? <div  class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
              <b>Close</b></button>
          <span class="glyphicon glyphicon-hand-right"></span> <strong>{this.state.ErrorMessage}</strong>  </div>: ''
        }
        
        <div className="row">
				<div className="col-md-12">
					<div className="portlet box grey-cascade">
            <div className="portlet-title">
            <div className="caption">
            <i className="fa fa-user"></i> Requester
          </div>
          <div className="tools">
            <a href="javascript:;" className="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" className="config">
            </a>
            <a href="javascript:;" className="reload">
            </a>
            <a href="javascript:;" className="remove">
            </a>
          </div>
            </div>
            <div className="portlet-body" style={{height:'380px'}}>
              <div className="table-toolbar">
              <div style={{overflow:'scroll', height:'360px'}}>
								<div className="row">
									<div className="col-md-6">
                  <div className="portlet-body form">
                  <div className="form-body">
                  <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                    <input  onChange={this.ChangeTeller} value={this.state.CreditAccount}   type="text" name="TellerID" class="form-control edited"/>
                    <label for="form_control_1"><b>Credit Account</b></label>
                    <span style={{fontSize:'12px'}} className="help-block">Enter the credit account number your branch..</span>
                    <i className="fa fa-key"></i>
                  </div>
                </div>
                    <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                    <input  onChange={this.ChangeTeller} value={this.state.DebitAccount}   type="text" name="TellerID" class="form-control edited"/>
                    <label for="form_control_1"><b>Debit Account</b></label>
                    <span style={{fontSize:'12px'}} className="help-block">Enter the debit account number of your branch..</span>
                    <i className="fa fa-key"></i>
                  </div>
                </div>
              <div className="form-group form-md-line-input has-success form-md-floating-label">
              <div className="input-icon right">
                <input data-type="currency" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" style={{textAlign:'right'}} defaultValue={"0.00"} onChange={this.ChangeAmount}   type="text" name="Amount" class="form-control edited"/>
                <label for="form_control_1"><b>Amount</b></label>
                <span style={{fontSize:'12px'}} className="help-block">Enter the amount you want to vault out ..</span>
                <i className="fa fa-money"></i>
              </div>
            </div>
            <div className="form-group form-md-line-input has-success form-md-floating-label">
            <div className="input-icon right">
              <input  onChange={this.ChangeTeller} value={this.state.DebitAccount}   type="text" name="TellerID" class="form-control edited"/>
              <label for="form_control_1"><b>CIF Number</b></label>
              <span style={{fontSize:'12px'}} className="help-block">Enter your CIF..</span>
              <i className="fa fa-key"></i>
            </div>
          </div>
                  </div>
                  </div>
                  </div>
                  <div className="col-md-6">
                  <div className="portlet-body form">
                  <div className="form-body">
                  <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                  <select className="form-control edited" onClick={this.ChangeCreditCurrency}>
                  <option id="0" value={0} disabled selected={true}>---Select Currency---</option>
                  {
                    this.state.CashDenominations.map(x=>
                      <option id={x.Currency} value={x.Abbrev}>{x.Currency}</option>
                      )
                  }
                  </select>
                  <label for="form_control_1"><b>Credit Currency</b></label>
                  <span style={{fontSize:'12px'}} className="help-block">Select the currency of the amount stipulated..</span>
                </div>
                </div>
                <div className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                <select className="form-control edited" onClick={this.ChangeDebitCurrency}>
                <option id="0" value={0} disabled selected={true}>---Select Currency---</option>
                {
                  this.state.CashDenominations.map(x=>
                    <option id={x.Currency} value={x.Abbrev}>{x.Currency}</option>
                    )
                }
                </select>
                <label for="form_control_1"><b>Debit Currency</b></label>
                <span style={{fontSize:'12px'}} className="help-block">Select the currency of the amount stipulated..</span>
              </div>
              </div>
                <div className="form-group form-md-line-input has-success form-md-floating-label">
              <div className="input-icon right">
                <input  onChange={this.ChangeRemarks} value={this.state.Remarks}  type="text" name="Remarks" class="form-control edited"/>
                <label for="form_control_1"><b>Remarks</b></label>
                <span style={{fontSize:'12px'}} className="help-block">Enter any additional information about vault out transaction..</span>
                <i className="fa fa-file"></i>
              </div>
            </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
            </div>
            </div>
            </div>
            <div style={{paddingLeft:'0px', textAlign:'right'}}><button onClick={this.ClearState} type="button" className="btn btn-default" data-dismiss="modal">Clear</button><button onClick={this.SendRequest} type="button" className="btn btn-primary"><i class="fa fa-arrow-circle-right"></i> Move {this.state.VaultExecuting === true ? <i id="spinner" class="fa fa-spinner fa-spin"></i> : ''}</button></div>
        </section>
        </section>

        <div className="modal fade" id="TransactionSuccessModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content" style={{width:'520px'}}>
            <div style={{background:'rgb(22 180 27)'}} className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             <h4 className="modal-title" id="myModalLabel">Transaction Successful</h4>
            </div>
            <div className="modal-body">
            <div className="row">
            <div className="col-md-12 ">
            <div className="portlet-body form">
              <div className="form-body">
              <p><b>Transaction successful with transaction reference: {this.state.TransRef} </b></p>
              </div>
              </div>
            </div>
      
            </div>
            </div>
            <div className="modal-footer">
            <button type="button" className="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
          </div>
        </div>
      </div>


      <div className="modal fade" id="NairaEquivalentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div className="modal-dialog">
        <div className="modal-content" style={{width:'520px'}}>
          <div style={{background:'#a00'}} className="modal-header">
            <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
           <h4 className="modal-title" id="myModalLabel">Naira Equivalent</h4>
          </div>
          <div className="modal-body">
          <div className="row">
          <div className="col-md-6 ">
          <div className="portlet-body form">
            <div className="form-body">
            <div className="form-group form-md-line-input has-success form-md-floating-label">
            <div className="input-icon right">
            <input className="form-control edit" type={"text"} readOnly disabled value={this.state.CurrencyRateDetails.CCY_SELL_RATE}/>
              <label for="form_control_1"><b>Rate</b></label>
              <i className="fa fa-money"></i>
            </div>
          </div>
            <div className="form-group form-md-line-input has-success form-md-floating-label">
            <div className="input-icon right">
            <input className="form-control edite" type={"text"} readOnly disabled value={this.state.Amount > 0 ? this.state.Amount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '0.00'}/>
              <label for="form_control_1"><b>Amount</b></label>
              <i className="fa fa-money"></i>
            </div>
          </div>
            </div>
            </div>
          </div>
    
          <div className="col-md-6 ">
          <div className="portlet-body form">
            <div className="form-body">
            <div className="form-group form-md-line-input has-success form-md-floating-label">
            <div className="input-icon right">
            <input className="form-control edite" type={"text"} readOnly disabled value={this.state.CurrencyRateDetails.CCY_CODE}/>
              <label for="form_control_1"><b>Currency Code</b></label>
              <i className="fa fa-money"></i>
            </div>
          </div>
            <div className="form-group form-md-line-input has-success form-md-floating-label">
            <div className="input-icon right">
            <input className="form-control edite" type={"text"} readOnly disabled value={this.state.ConvertedAmount > 0 ? this.state.ConvertedAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '0.00'}/>
              <label for="form_control_1"><b>Converted Amount</b></label>
              <i className="fa fa-money"></i>
            </div>
          </div>
            </div>
            </div>
          </div>
          </div>
          </div>
          <div className="modal-footer">
          <button type="button" className="btn btn-primary" data-dismiss="modal">OK</button>
          </div>
        </div>
      </div>
    </div>

      </div>
    )
  }
}

export default VaultOut