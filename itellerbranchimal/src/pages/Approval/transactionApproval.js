import React, {Component} from "react";
import MockJson from '../../apiService/mockJson';
import axios from 'axios';
import DataTable from '../../shared/dataTable'
import apiUrl from '../../apiService/config'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import $ from 'jquery';
import Select from 'react-select';
import GenerateToken from '../../shared/token'

const userName = localStorage.getItem('Id')
class TransactionApproval extends Component {
  constructor (props){
    super(props)
    this.state = {
    ID:0,
    TillNos:'',
    TranId:0,
    TillDesc: '',
    TransRef:'',
    GLAccountID:0,
    MinTillAmount: '0.00',
    MaxTillAmount: '0.00',
    Till:[],
    Transaction:[],
    OriginalTransaction:[],
    ReverseTransExecuting: false,
    ReversalErrorMessage:'',
    ReasonForReversal:''
}
}
ChangeHandler=e => {
  this.setState({ReasonForReversal: e.target.value})
}
onClickApprove = e => {
  console.log(e.target.id);
  let trans = this.state.Transaction;
  trans = trans.filter(function(data){
     return data.TranId === parseInt(e.target.id);
  })
  this.setState({TransRef: trans[0].TransRef, TranId:parseInt(e.target.id)})
}
ApproveTransaction = async e =>{
  let currentComponent = this;
  this.setState({ReverseTransExecuting: true, DisableButton:true});
  let transaction = this.state.Transaction;
  transaction = transaction.filter(function(data){
    return data.TranId === currentComponent.state.TranId
  })[0]
  // let data = {
  //   "TReference": this.state.TransRef,"access_token": localStorage.getItem('access_token'),
  //   "TranId": this.state.TranId,"ApprovedBy": localStorage.getItem('Id')
  // }
  transaction.access_token = localStorage.getItem('access_token');
  transaction.ApprovedBy = localStorage.getItem('Id');
   console.log(transaction)
   await axios.post(apiUrl.Approval.ApproveTransaction, transaction)
   .then(function(response){
     console.log(response.data);
     window.$('#ApproveTransactionModal').modal('toggle');
     if(response.data.success == true){
     // NotificationManager.success('Transaction approved successfully', 'Approved', 1000)
     let transaction = response.data.data.Transaction.filter(function(data){
      return data.NeededApproval === true && data.Approved === false && data.CBA === 'IMAL' && data.Status !== 3;
  })
  if(!localStorage.getItem("Branch").includes("ALL")){
   transaction = transaction.filter(function(element){
     return element.BranchCode === localStorage.getItem("Branch")
})
}
    currentComponent.setState({Transaction: transaction, ReverseTransExecuting: false, DisableButton:false, TransRef:response.data.TransactionRef})
    window.$('#TransactionSuccessModal').modal('toggle')
     }else{
      NotificationManager.error('Transaction not approved', 'Error', 3000)
      currentComponent.setState({ReverseTransExecuting: false})
     }
    
   }).catch(function(error){
    NotificationManager.error('Server Error. Please contact the admin', 'Error', 3000)
    currentComponent.setState({ReverseTransExecuting: false, DisableButton:false})
     console.log(error);
   })
  
}

DisapproveTransaction = async e =>{
  let currentComponent = this;
  if(this.state.ReasonForReversal === ''){
    currentComponent.setState({ReversalErrorMessage:'* Please input reason for reversal'})
    return;
  }
  
  this.setState({ReverseTransExecuting: true, DisableButton:true});
  let data = {
    "TReference": this.state.TransRef,"access_token": localStorage.getItem('access_token'),
    "TranId": this.state.TranId,"DisapprovedBy": localStorage.getItem('Id'),"DisapprovalReason": this.state.ReasonForReversal
  }
   console.log(data)
   await axios.post(apiUrl.Approval.DisapproveTransaction, data)
   .then(function(response){
     console.log(response.data);
     window.$('#DisapproveTransactionModal').modal('toggle');
     if(response.data.success == true){
      NotificationManager.success('Transaction disapproved successfully', 'Reversed',3000)
      let transaction = response.data.data.Transaction.filter(function(data){
        return data.NeededApproval === true && data.Approved === false && data.CBA === 'IMAL' && data.Status !== 3;
    })
    currentComponent.setState({Transaction: transaction, ReverseTransExecuting: false, DisableButton:false})
     }else{
      NotificationManager.error('Transaction not disapproved', 'Error', 3000)
      currentComponent.setState({ReverseTransExecuting: false, DisableButton:false})
     }
    
   }).catch(function(error){
    NotificationManager.error('Server Error. Please contact the admin', 'Error', 1000)
    currentComponent.setState({ReverseTransExecuting: false})
     console.log(error);
   })
  
}

GetDate(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();

  today = dd  + '/' + mm + '/' + yyyy;
  console.log(today);
  return today
}

 async componentDidMount(){
   //TransRef
  await GenerateToken();
 let currentComponent = this;
 console.log(userName);
 await axios.get(apiUrl.BankService.GetTransaction)
 .then(function(response){
  console.log(response.data)
   let transaction = response.data.data.Transaction.filter(function(data){
       return data.NeededApproval === true && data.Approved === false && data.CBA === 'IMAL' && data.Status !== 3;
   })
   if(!localStorage.getItem("Branch").includes("ALL")){
    transaction = transaction.filter(function(element){
      return element.BranchCode === localStorage.getItem("Branch")
 })
}
     currentComponent.setState({Transaction: transaction}) 
 })
    DataTable();
}

  render(){
    return (
      <div>
      <NotificationContainer/>
      <section id="main-content">
      <section className="wrapper">
        <div className="row">
        <div className="col-md-12">
        <h3 className="page-title">
       Approve Transaction <small></small>
        </h3>
        </div>
        </div><br/><br/>
        <div className="row">
				<div className="col-md-12">
					<div className="portlet box grey-cascade">
            <div className="portlet-title">
            <div className="caption">
            <i className="fa fa-money"></i>
          </div>
          <div className="tools">
            <a href="javascript:;" className="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" className="config">
            </a>
            <a href="javascript:;" className="reload">
            </a>
            <a href="javascript:;" className="remove">
            </a>
          </div>
            </div>
            <div className="portlet-body">
							<div className="table-toolbar">
                  <table className="table table-striped table-bordered table-hover" id="sample_1">
                  <thead>
                  <tr>  
                    <th>
                       Transaction ID
                    </th>
                    <th>Transactin Type</th>
                    <th>
                    Customer Account Number
                    </th>
                    <th>
                    Currency
                    </th>
                    <th>
                  Creation Date
                    </th>
                    <th>
                     Amount
                     </th>
                     <th>
                    Action
                     </th>
                  </tr>
                  </thead>
                  <tbody>
                  {
                    this.state.Transaction.map(x=> 
                     <tr key={x.TranId}>
                     <td>{x.TranId}</td>
                     <td>{x.TransType === 1 ? 'Cash Withdrawal' : x.TransType === 2 ? "Cash Deposit" : "Cash Withdrawal with Cheque"}</td>
                     <td>{x.AccountNo}</td>
                  <td>{x.CurrncyCode}</td>
                  <td>{this.GetDate(x.CreationDate)}</td>
                  <td style={{textAlign: 'right'}}>{x.TotalAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                  <td> 
                  <a onClick={this.onClickApprove} id={x.TranId} href="#" className="btn btn-primary a-btn-slide-text" data-toggle="modal" data-target="#ApproveTransactionModal" >
                  <span id={x.TranId} className="fa fa-thumbs-up" aria-hidden="true"></span>
                  <span><strong id={x.TranId}>  Approve</strong></span>            
                </a>
                <a onClick={this.onClickApprove} id={x.TranId} href="#" className="btn btn-danger a-btn-slide-text" data-toggle="modal" data-target="#DisapproveTransactionModal" >
                <span id={x.TranId} className="fa fa-thumbs-up" aria-hidden="true"></span>
                <span><strong id={x.TranId}>  Disapprove</strong></span>            
              </a>
                  </td>
                     </tr> 
                      
                      )
                  }
                  </tbody>
                  </table>
                  </div>
                  </div>
            </div>
            </div>
            </div>
        </section>
        </section>


        <div className="modal fade" id="ApproveTransactionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content" style={{width:'520px'}}>
            <div style={{background:'rgb(202 188 18)'}} className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             <h4 className="modal-title" id="myModalLabel">Approve Transaction</h4>
            </div>
            <div className="modal-body">
            <div className="row">
            <div className="col-md-12 ">
            <div className="portlet-body form">
              <div className="form-body">
              <p style={{color:'red'}}>{this.state.ReversalErrorMessage}</p>
              <p><b>Are you sure you want to approve this transaction? </b></p>
              </div>
              </div>
            </div>
           
            </div>
            </div>
            <div className="modal-footer">
            <button disabled={this.state.DisableButton} onClick={this.ApproveTransaction} type="button" className="btn btn-primary">Approve  {this.state.ReverseTransExecuting === true ? <i id="spinner" class="fa fa-spinner fa-spin"></i> : ''}</button>
            </div>
          </div>
        </div>
      </div>

      <div className="modal fade" id="DisapproveTransactionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div className="modal-dialog">
        <div className="modal-content" style={{width:'520px'}}>
          <div style={{background:'red'}} className="modal-header">
            <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
           <h4 className="modal-title" id="myModalLabel">Disapprove Transaction</h4>
          </div>
          <div className="modal-body">
          <div className="row">
          <div className="col-md-12 ">
          <div className="portlet-body form">
            <div className="form-body">
            <p style={{color:'red'}}>{this.state.ReversalErrorMessage}</p>
            <p><b>Are you sure you want to dispprove this transaction? </b></p>
            <label>Disapproval Reason</label>
            <input className="form-control" type="text" onChange={this.ChangeHandler}/>
            </div>
            </div>
          </div>
         
          </div>
          </div>
          <div className="modal-footer">
          <button disabled={this.state.DisableButton} onClick={this.DisapproveTransaction} type="button" className="btn btn-primary">Disapprove {this.state.ReverseTransExecuting === true ? <i id="spinner" class="fa fa-spinner fa-spin"></i> : ''}</button>
          </div>
        </div>
      </div>
    </div>

    <div className="modal fade" id="TransactionSuccessModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div className="modal-dialog">
      <div className="modal-content" style={{width:'520px'}}>
        <div style={{background:'rgb(22 180 27)'}} className="modal-header">
          <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         <h4 className="modal-title" id="myModalLabel">Transaction Successful</h4>
        </div>
        <div className="modal-body">
        <div className="row">
        <div className="col-md-12 ">
        <div className="portlet-body form">
          <div className="form-body">
          <p><b>Transaction successful with transaction reference: {this.state.TransRef} </b></p>
          </div>
          </div>
        </div>
  
        </div>
        </div>
        <div className="modal-footer">
        <button data-dismiss="modal" className="btn btn-primary"  data-target="#print" >Ok</button>
      <button  type="button" className="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>


      </div>
    )
  }
}

export default TransactionApproval