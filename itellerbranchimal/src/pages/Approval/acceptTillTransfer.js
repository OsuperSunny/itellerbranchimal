import React, {Component} from "react";
import DataTable from '../../shared/dataTable'
import MockJson from '../../apiService/mockJson'
import axios from 'axios';
import apiUrl from '../../apiService/config'
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import $ from 'jquery';
import Select from 'react-select';
import GenerateToken from '../../shared/token'
import Till from "../setup/till";
const userName = localStorage.getItem('Id')
class AcceptTill extends Component {
  constructor (props){
    super(props)
    this.state = {
    ID:0,
    TillNos:'',
    TillDesc: '',
    GLAccountID:0,
    Currency:1,
    MinTillAmount: '0.00',
    MaxTillAmount: '0.00',
    Till:[],
    TillDetails:{},
    Executing: false,
    Amount:0.00,
    TransferMode:0,
    Abbrev:'',
    TransRef:'',
    TransacterName:'',
    IsAccept: true,
    DisableButton:false,
    TellerDetails:{},
    IsTellerValid:false,
    CashDenominations:[],
    cifNum:'',
    Disable:false,
    ErrorMessage:'',
    Code:0
}
}

AcceptTill = e =>{
  //let till = this.state.Till;
  let currentComponent = this;
  console.log(e.target.id);
  this.setState({ID: parseInt(e.target.id)})
  window.$('#AcceptModal').modal('toggle');
  let till = this.state.Till.filter(function(data){
        return data.Id === parseInt(e.target.id)
  })
  this.setState({TransferMode:till[0].TransferMode,Abbrev:till[0].Abbrev})
 console.log(till[0].Abbrev);
   this.setState({TillDetails: till[0], Currency: till[0].ID,TransacterName: till[0].GiverUser});
}

RejectTill = e =>{
  //let till = this.state.Till;
  let currentComponent = this;
  console.log(e.target.id);
  this.setState({ID: parseInt(e.target.id)})
  window.$('#RejectModal').modal('toggle');
  let till = this.state.Till.filter(function(data){
        return data.Id === parseInt(e.target.id)
  })
  this.setState({TransferMode:till[0].TransferMode,Abbrev:till[0].Abbrev,IsAccept:false})
 console.log(till[0].Abbrev);
   this.setState({TillDetails: till[0], Currency: till[0].ID,TransacterName: till[0].GiverUser});
}



GetDateForAPI(){
  var today = new Date();
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();
   var hours = today.getHours();
  var min = today.getMinutes();
  var sec = today.getSeconds();
  today = yyyy + '-' + mm + '-' + dd;
  console.log(today);
  return today
}

RejectTransfer = e =>{
  let transferMode = this.state.TransferMode;
  let IsVaultIn;
  let IsTillTransfer;
  let url = "";
  let currentComponent = this;
  currentComponent.setState({Executing:true, DisableButton: true})
  //let data ={}
 
  this.setState({Executing:true, DisableButton: true})
 let  data ={Id: this.state.ID, Amount:0.00,CurrencyCode:0}
//    let data = {"Approved": false, "TransRef":"","PhoneNo":"","TransName":this.state.TransacterName, "AccountNo": "","Amount": this.state.TillDetails.Amount,"TransType": "","TellerId": this.state.TillDetails.GiverTillNo,"CustomerAcctNos": "","TotalAmt": this.state.TillDetails.Amount,"WithdrawerName": userName,"WithdrawerPhoneNo": "","Status": 0,"CashierID": this.state.TillDetails.GiverTillNo,"CashierTillNos": this.state.TillDetails.GiverTillNo,"CashierTillGL": "","WhenApproved": currentComponent.GetDateForAPI() ,"SortCode": "","Currency": this.state.Currency,"ValueDate": currentComponent.GetDateForAPI(),"SupervisoryUser": "","Beneficiary": this.state.TillDetails.ReceiverUser,"ChequeNo": "","DateOnCheque": currentComponent.GetDateForAPI(),"Remark":"", "Narration": "","CreationDate": currentComponent.GetDateForAPI() ,"MachineName": "",TransactionParty:"",
// "TillTransferID":this.state.ID,"IsTillTransfer":IsTillTransfer,"NeededApproval":false, "InitiatorName": this.state.TransacterName, "IsT24": true,"Branch":this.state.TillDetails.Branch, "CurrCode": this.state.Abbrev, "ToTellerId":this.state.TillDetails.ReceiverTillNo,"DisapprovalReason":"","DisapprovedBy":"",IsVaultIn:IsVaultIn,"WhenDisapproved":currentComponent.GetDateForAPI(), "access_token":localStorage.getItem('access_token'),"CBACode": "","CBA": "T24","CBAResponse":"test","TransacterEmail": "","IsReversed": false,"ApprovedBy": "",AccountName: this.state.UserTillDetails.UserName,
// "ReversedTranId": 0,"BranchCode": this.state.TillDetails.Branch, TransactionDetailsModels:[]}
console.log(data);
  axios.post(apiUrl.BankService.RejectTill, data)
  .then(function(response){
    console.log(response.data);
    if(response.data.success){
     // currentComponent.setState({TransRef: response.data.TransactionRef})
     
   console.log(response.data);
  // if(response.data.success){
     currentComponent.UpdateGrid();
    NotificationManager.success("Rejected successfully", "Success");
    window.$('#RejectModal').modal('toggle');
   currentComponent.setState({Executing:false, DisableButton: false})
   //  currentComponent.setState({UserTillDetails: response.data.UserDetails})
  //  axios.get(apiUrl.Management.GetRequestedTill + currentComponent.state.cifNum)
  //    .then(function(response){
  //      console.log(response.data)
  //     currentComponent.setState({Till:response.data})
  //    })
 //  }

  //  NotificationManager.success("Rejected successfully", "Success");
  //  window.$('#RejectModal').modal('toggle');
  // currentComponent.setState({Executing:false, DisableButton: false})
  
 
    }else
    {
     NotificationManager.error(response.data.message, "Error")
     currentComponent.setState({Executing:false, DisableButton: false})
    }
 }).catch(function(error){
     console.log(error);
 })
}

SendRequest = e=>{
  let transferMode = this.state.TransferMode;
  let IsVaultIn;
  let IsTillTransfer;
  let url = "";
  let currentComponent = this;
  currentComponent.setState({Executing:true,Disable:true})

  //let data ={}
  this.setState({Executing:true})
   let data = {"Approved": true,"TransRef":"","PhoneNo":"","TransName":this.state.TransacterName, "AccountNo": "","Amount": this.state.TillDetails.Amount,"TransType": "","TellerId": this.state.TillDetails.GiverTillNo,"CustomerAcctNos": "","TotalAmt": this.state.TillDetails.Amount,"WithdrawerName": "","WithdrawerPhoneNo": "","Status": 0,"CashierID": localStorage.getItem('Id'),"CashierTillNos": this.state.TillDetails.GiverTillNo,"CashierTillGL": "","WhenApproved": currentComponent.GetDateForAPI() ,"SortCode": "","Currency": this.state.Currency,"ValueDate": currentComponent.GetDateForAPI(),"SupervisoryUser": "","Beneficiary": this.state.TillDetails.ReceiverUser,"ChequeNo": "","DateOnCheque": currentComponent.GetDateForAPI(),"Remark":"", "Narration": "","CreationDate": currentComponent.GetDateForAPI() ,"MachineName": "",TransactionParty:"",
"TillTransferID":this.state.ID,"IsTillTransfer":IsTillTransfer,"NeededApproval":false, "InitiatorName": this.state.TransacterName, "IsT24": true,"Branch":this.state.TillDetails.Branch, "CurrCode": this.state.Abbrev, "ToTellerId":this.state.TillDetails.ReceiverTillNo,"DisapprovalReason":"","DisapprovedBy":"",IsVaultIn:IsVaultIn,"WhenDisapproved":currentComponent.GetDateForAPI(), "access_token":localStorage.getItem('access_token'),"CBACode": "","CBA": "T24","CBAResponse":"test","TransacterEmail": "","IsReversed": false,"ApprovedBy": "",AccountName: localStorage.getItem('Id'),
"ReversedTranId": 0,"BranchCode": this.state.TillDetails.CashierID, TransactionDetailsModels:[]}

 url = apiUrl.BankService.AcceptTransferTill;
console.log(data);
  axios.post(url, data)
  .then(function(response){
    console.log(response.data);
    if(response.data.success){
      currentComponent.setState({TransRef: response.data.TransactionRef})
      currentComponent.UpdateGrid()
      window.$('#TransactionSuccessModal').modal('toggle')
      NotificationManager.success("Accepted successfully", "Success");
      window.$('#AcceptModal').modal('toggle');
      currentComponent.setState({Executing:false,Disable:false})
//       let cifNum = currentComponent.state.cifNum;
//       axios.get(apiUrl.Management.GetRequestedTill + cifNum.padStart(8,'0'))
//       .then(function(response){
//    console.log(response.data);
//    currentComponent.setState({Till:response.data})
//    window.$('#TransactionSuccessModal').modal('toggle')
//    NotificationManager.success("Accepted successfully", "Success");
//    window.$('#AcceptModal').modal('toggle');
//    currentComponent.setState({Executing:false,Disable:false})
//   //  if(response.data.success){
//   //    currentComponent.setState({UserTillDetails: response.data.UserDetails})
//   //  }
//   //  if(currentComponent.state.IsAccept){
  
//   //  }else{
//   //   NotificationManager.success("Rejected successfully", "Success");
//   //   window.$('#RejectModal').modal('toggle');
//   //  }
//  }).catch(function(error){
//    console.log(error);
//    NotificationManager.error("Server Error", "Error", 3000)
//    currentComponent.setState({Executing:false,Disable:false})
//  })
    }else
    {
    // NotificationManager.error(response.data.message, "Error",3000)
    currentComponent.setState({Executing:false,Disable:false,ErrorMessage:response.data.message})
    window.$('#AcceptModal').modal('toggle');
    window.$('#errorModal').modal('toggle')
    
    }
 }).catch(function(error){
  NotificationManager.error("Server Error", "Error",3000)
  currentComponent.setState({Executing:false,Disable:false})
     console.log(error);
 })
}


async UpdateGrid (){
  let currentComponent = this;
  console.log(this.state.Code);
  this.setState({ShowError:false, ErrorMessage:''})
    let cifNum = "";
    await axios.get(apiUrl.BankService.GetTellerDetails  + localStorage.getItem("Id") + '/CurrCode/' + this.state.Code+  '/Token/' + localStorage.getItem('access_token'))
    .then(function(response){
      console.log(response.data.details);
      let tellerDetails = response.data.details
      console.log(Array.isArray(tellerDetails));
      if(Array.isArray(tellerDetails)){
        if(tellerDetails.length > 0){
         currentComponent.setState({TellerDetails: response.data.details[0],IsTellerValid:true})
         cifNum = response.data.details[0].CIF_NO + "";
         currentComponent.setState({cifNum})
         axios.get(apiUrl.Management.GetRequestedTill + cifNum.padStart(8,'0'))
         .then(function(response){
           console.log(response.data)
           currentComponent.setState({Till:response.data})
         })
        }else{
         currentComponent.setState({IsTellerValid:false})
         currentComponent.setState({ShowError:true, ErrorMessage: 'The login user is not yet assigned CIF number. Please contact the admin or select appropraite currency at the top right'})
         return;
        }
      
      }else{
        if( Object.keys(tellerDetails).length === 0){
         currentComponent.setState({IsTellerValid:false})
         currentComponent.setState({ ShowError:true, ErrorMessage: 'The login user is not yet assigned CIF number. Please contact the admin or select appropraite currency at the top right'})
         return;
        }else{
         currentComponent.setState({TellerDetails: tellerDetails,IsTellerValid:true})
         cifNum = tellerDetails.CIF_NO + "";
         currentComponent.setState({cifNum})
         axios.get(apiUrl.Management.GetRequestedTill + cifNum.padStart(8,'0'))
         .then(function(response){
           console.log(response.data)
           currentComponent.setState({Till:response.data})
         })
        }
      }
    }).catch(function(error){
     currentComponent.setState({IsTellerValid:false})
     currentComponent.setState({ ShowError:true, ErrorMessage: 'The login user is not yet assigned CIF number. Please contact the admin',})
     return;
      console.log(error);
    })
}


ChangeCurrency = async e =>{
  let currentComponent = this;
  console.log(e.target.value);
  this.setState({ShowError:false, ErrorMessage:''})
  let currencyCode =parseInt(e.target.value);
  if(currencyCode > 0){
    let currency = this.state.CashDenominations.filter(function(data){
      return data.ID === currencyCode
    })
    console.log(currency);
    this.setState({CurrencyCode: currency[0].Abbrev, CurrencyId: currencyCode, Code: currency[0].CurrencyCode})
    let cifNum = "";
    await axios.get(apiUrl.BankService.GetTellerDetails  + localStorage.getItem("Id") + '/CurrCode/' + currency[0].CurrencyCode +  '/Token/' + localStorage.getItem('access_token'))
    .then(function(response){
      console.log(response.data.details);
      let tellerDetails = response.data.details
      console.log(Array.isArray(tellerDetails));
      if(Array.isArray(tellerDetails)){
        if(tellerDetails.length > 0){
         currentComponent.setState({TellerDetails: response.data.details[0],IsTellerValid:true})
         cifNum = response.data.details[0].CIF_NO + "";
         currentComponent.setState({cifNum})
         axios.get(apiUrl.Management.GetRequestedTill + cifNum.padStart(8,'0'))
         .then(function(response){
           console.log(response.data)
           currentComponent.setState({Till:response.data})
         })
        }else{
         currentComponent.setState({IsTellerValid:false})
         currentComponent.setState({ShowError:true, ErrorMessage: 'The login user is not yet assigned CIF number. Please contact the admin or select appropraite currency at the top right'})
         return;
        }
      
      }else{
        if( Object.keys(tellerDetails).length === 0){
         currentComponent.setState({IsTellerValid:false})
         currentComponent.setState({ ShowError:true, ErrorMessage: 'The login user is not yet assigned CIF number. Please contact the admin or select appropraite currency at the top right'})
         return;
        }else{
         currentComponent.setState({TellerDetails: tellerDetails,IsTellerValid:true})
         cifNum = tellerDetails.CIF_NO + "";
         currentComponent.setState({cifNum})
         axios.get(apiUrl.Management.GetRequestedTill + cifNum.padStart(8,'0'))
         .then(function(response){
           console.log(response.data)
           currentComponent.setState({Till:response.data})
         })
        }
      }
    }).catch(function(error){
     currentComponent.setState({IsTellerValid:false})
     currentComponent.setState({ ShowError:true, ErrorMessage: 'The login user is not yet assigned CIF number. Please contact the admin',})
     return;
      console.log(error);
    })
  }
}

 async componentDidMount(){
 let currentComponent = this;
 console.log(userName);
 GenerateToken();
 await axios.get(apiUrl.Security.GetCurrency)
 .then(function(response){
   let getDenominations = response.data.data;
   console.log(getDenominations);
   currentComponent.setState({CashDenominations:getDenominations})
 })
//  await axios.get(apiUrl.BankService.GetTellerDetails  + localStorage.getItem("Id") + '/CurrCode/' + currencyCode +  '/Token/' + localStorage.getItem('access_token'))
//  .then(function(response){
//    console.log(response.data);
//    if(response.data.success){
//      currentComponent.setState({UserTillDetails: response.data.UserDetails})

 
//    }
   
//  }).catch(function(error){
//    console.log(error);
//  })
window.$('#currencyModal').modal('toggle')
window.$('#sample_1').DataTable( {
  "language": {
    "zeroRecords": " " //Change your default empty table message
    },
  retrieve: true,
  paging: true
});

}

  render(){
    return (
      <div>
      <NotificationContainer/>
      <section id="main-content">
      <section className="wrapper">
        <div className="row">
        <div className="col-md-12">
        <h3 className="page-title">
        Accept Till Transfer <small></small>
        </h3>
        </div>
        </div><br/><br/>
        {
          this.state.ShowError === true ? <div  class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
              <b>Close</b></button>
          <span class="glyphicon glyphicon-hand-right"></span> <strong>{this.state.ErrorMessage}</strong>  </div>: ''
        }
       
        <div style={{float:'right'}}>
          
        <select className="form-control" onChange={this.ChangeCurrency}>
       <option id="0" value={0} disabled selected={true}>---Select Currency---</option>
                  {
                    this.state.CashDenominations.map(x=>
                      <option  value={x.ID}>{x.Currency}</option>
                      )
                  }

       </select>
        </div>
        <span style={{float:'right',fontSize:'15px'}}><b>Currency:   </b></span>
        <br/><br/><br/>
        <div className="row">
				<div className="col-md-12">
					<div className="portlet box grey-cascade">
            <div className="portlet-title">
            <div className="caption">
            <i className="fa fa-money"></i>
          </div>
          <div className="tools">
            <a href="javascript:;" className="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" className="config">
            </a>
            <a href="javascript:;" className="reload">
            </a>
            <a href="javascript:;" className="remove">
            </a>
          </div>
            </div>
            <div className="portlet-body">
							<div className="table-toolbar">
                  <table className="table table-striped table-bordered table-hover" id="sample_1">
                  <thead>
                  <tr>  
                  <th>
                  Reciever Till Code
                   </th>
                    <th>
                    Currency
                    </th>
                    <th>
                   Giver Till Code
                    </th>
                    <th>
                   Till Amount
                     </th>
                     <th style={{width:'300px'}}>
                    Action
                     </th>
                  </tr>
                  </thead>
                  <tbody>
                  {
                    this.state.Till.map(x=> 
                     <tr key={x.Id}>
                     <td>{x.ReceiverTillNo}</td>
                  <td>{x.Currency}</td>
                  <td>{x.GiverTillNo}</td>
                  <td style={{textAlign: 'right'}}>{x.Amount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                  <td> <a  id={x.Id} onClick={this.AcceptTill}  href="#" className="btn btn-primary a-btn-slide-text" >
                  <span id={x.Id} className="fa fa-thumbs-up" aria-hidden="true"></span>
                  <span id={x.Id} ><strong id={x.Id}>  Accept</strong></span>  </a><a  id={x.Id} onClick={this.RejectTill}  href="#" className="btn btn-primary a-btn-slide-text" >
                  <span id={x.Id} className="fa fa-thumbs-down" aria-hidden="true"></span>
                  <span id={x.Id} ><strong id={x.Id}>  Reject</strong></span>  </a></td>
                     </tr> 
                      
                      )
                  }
                  </tbody>
                  </table>
                  </div>
                  </div>
            </div>
            </div>
            </div>
        </section>
        </section>
        <div aria-hidden="true" aria-labelledby="myModalLabel" id="AcceptModal" role="dialog" tabindex="-1"  class="modal fade">
        <div class="modal-dialog" style={{width:'350px'}}>
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4  style={{color:'white'}} className="modal-title" id="myModalLabel">Transfer Till</h4>
            </div>
            <div class="modal-body">
              <p>Are you sure you want to accept transfer.</p>
            </div>
            <div class="modal-footer">
              <button  data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
              <button disabled={this.state.Disable} onClick={this.SendRequest} class="btn btn-theme" type="button"><i className="fa fa-thumbs-up"></i> Accept {this.state.Executing === true ? <i id="spinner" class="fa fa-spinner fa-spin"></i> : ''}</button>
            </div>
          </div>
        </div>
        </div>

        <div aria-hidden="true" aria-labelledby="myModalLabel" id="RejectModal" role="dialog" tabindex="-1"  class="modal fade">
        <div class="modal-dialog" style={{width:'350px'}}>
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4  style={{color:'white'}} className="modal-title" id="myModalLabel">Transfer Till</h4>
            </div>
            <div class="modal-body">
              <p>Are you sure you want to reject transfer.</p>
            </div>
            <div class="modal-footer">
              <button  data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
              <button disabled={this.state.DisableButton} onClick={this.RejectTransfer} class="btn btn-theme" type="button"><i className="fa fa-thumbs-down"></i> Reject {this.state.Executing === true ? <i id="spinner" class="fa fa-spinner fa-spin"></i> : ''}</button>
            </div>
          </div>
        </div>
        </div>


        <div className="modal fade" id="TransactionSuccessModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content" style={{width:'520px'}}>
            <div style={{background:'rgb(22 180 27)'}} className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             <h4 className="modal-title" id="myModalLabel">Transaction Successful</h4>
            </div>
            <div className="modal-body">
            <div className="row">
            <div className="col-md-12 ">
            <div className="portlet-body form">
              <div className="form-body">
              <p><b>Transaction successful with transaction reference: {this.state.TransRef} </b></p>
              </div>
              </div>
            </div>
      
            </div>
            </div>
            <div className="modal-footer">
            <button type="button" className="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
          </div>
        </div>
      </div>

      <div aria-hidden="true" aria-labelledby="myModalLabel" id="currencyModal" role="dialog" tabindex="-1"  class="modal fade">
<div class="modal-dialog" style={{width:'400px'}}>
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4  style={{color:'white'}} className="modal-title" id="myModalLabel">Error</h4>
    </div>
    <div class="modal-body">
      <p><b>Please Select your CIF Currency </b></p>
       <select className="form-control" onChange={this.ChangeCurrency}>
       <option id="0" value={0} disabled selected={true}>---Select Currency---</option>
                  {
                    this.state.CashDenominations.map(x=>
                      <option  value={x.ID}>{x.Currency}</option>
                      )
                  }

       </select>
    </div>
    <div class="modal-footer">
      <button  data-dismiss="modal" class="btn btn-default" type="button">OK</button>
    </div>
  </div>
</div>
</div>
  

<div aria-hidden="true" aria-labelledby="myModalLabel" id="errorModal" role="dialog" tabindex="-1"  class="modal fade">
<div class="modal-dialog" style={{width:'400px'}}>
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4  style={{color:'white'}} className="modal-title" id="myModalLabel">Error</h4>
    </div>
    <div class="modal-body">
      <p><i className="fa fa-exclamation-triangle"></i>   {this.state.ErrorMessage}</p>
    </div>
    <div class="modal-footer">
      <button  data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
    </div>
  </div>
</div>
</div>

      </div>
    )
  }
}

export default AcceptTill