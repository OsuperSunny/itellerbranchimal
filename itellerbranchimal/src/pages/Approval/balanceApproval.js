import React, {Component} from "react";
import DataTable from '../../shared/dataTable'
import MockJson from '../../apiService/mockJson'
import $ from 'jquery';
import Select from 'react-select';
import axios from 'axios';
import apiUrl from '../../apiService/config'
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";



class BalanceApproval extends Component {
  constructor (props){
    super(props)
    this.state = {
    ID:0,
    TillApproval:[],
   startDate: new Date(),
   ApproveTillExecuting: false
}
}

GetDate(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();

  today = dd  + '/' + mm + '/' + yyyy;
  console.log(today);
  return today
}

onClickApprove = e =>{
  let ID = parseInt(e.target.id);
  this.setState({ID: ID}); 
}

onClickDisapprove = e =>{
   let ID = parseInt(e.target.id);
  this.setState({ID: ID}); 
}

ApproveTill = async e =>{
  e.preventDefault();
  this.setState({ApproveTillExecuting: true});
  let currentComponent = this;
  let data = {Id: this.state.ID}
  await axios.post(apiUrl.Approval.ApproveTillBalance, data)
  .then(function(response){
    if(response.data.success){
      let approval =  response.data.TillApprovalDetails;
      approval = approval.filter(function(data){
        return data.IsClosed == null && data.Approve === null;
      })
      console.log(approval)
      window.$('#ApproveModal').modal('hide')
      currentComponent.setState({TillApproval: approval,ApproveTillExecuting:false})
      NotificationManager.success('Approved', "Till balance approved successfully", 2000);
    }else{
      NotificationManager.error('Error', response.data.message, 2000);
      currentComponent.setState({ApproveTillExecuting:false});
    }
 
  }).catch(function(error){
    console.log(error)
    NotificationManager.error('Error', "Server Error. Please contact the admin")
  })
}

DisapproveTill = e =>{
  e.stopPropagation();
  this.setState({ApproveTillExecuting: true});
  let currentComponent = this;
  let data = {Id: this.state.ID}
  axios.post(apiUrl.Approval.DisapproveTillBalance, data)
  .then(function(response){
    if(response.data.success){
      let approval =  response.data.TillApprovalDetails;
      approval = approval.filter(function(data){
        return data.IsClosed == null && data.Approve === null;
      })
      console.log(approval)
      NotificationManager.success('Disapproved', "Till balance disapproved successfully", 2000)
      window.$('#DisapproveModal').modal('hide')
      currentComponent.setState({TillApproval: approval,ApproveTillExecuting:false})
    }else{
      NotificationManager.error('Error', response.data.message, 2000)
      currentComponent.setState({ApproveTillExecuting:false})
    }
  }).catch(function(error){
    console.log(error)
    NotificationManager.error('Error', "Server Error. Please contact the admin",2000)
  })
}

GetDate(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();

  today = dd  + '/' + mm + '/' + yyyy;
  console.log(today);
  return today
}

 async componentDidMount(){
   let currentComponent = this;
 
 await axios.get(apiUrl.Approval.GetTillApproval)
 .then(function(response){
   let approval =  response.data.TillApprovalDetails;
   console.log(approval)
    approval = approval.filter(function(data){
      return data.IsClosed == null;
    })
   currentComponent.setState({TillApproval: approval})
 })
    DataTable();
}

  render(){
    return (
      <div>
      <NotificationContainer/>
      <section id="main-content">
      <section className="wrapper">
        <div className="row">
        <div className="col-md-12">
        <h3 className="page-title">
        Close Till Approval <small></small>
        </h3>
        </div>
        </div><br/><br/>
        <div className="row">
				<div className="col-md-12">
					<div className="portlet box grey-cascade">
            <div className="portlet-title">
            <div className="caption">
            <i className="fa fa-money"></i>
          </div>
          <div className="tools">
            <a href="javascript:;" className="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" className="config">
            </a>
            <a href="javascript:;" className="reload">
            </a>
            <a href="javascript:;" className="remove">
            </a>
          </div>
            </div>
            <div className="portlet-body">
							<div className="table-toolbar">
								<div className="row">
								
                  </div><br/>
                  <table className="table table-striped table-bordered table-hover" id="sample_1">
                  <thead>
                  <tr>   
                    <th style={{width:'100px'}}>
                       Teller ID
                    </th>
                    <th style={{width:'100px'}}>
                    User 
                    </th>
                    <th style={{width:'200px'}}>
                    Event
                    </th>
                    <th style={{width:'100px'}}>
                    Creation Date
                     </th>
                     <th style={{width:'150px'}}>Cash at Hand</th>
                     <th>Overage/Shortage</th>
                     <th>
                    Action
                     </th>
                  </tr>
                  </thead>
                  <tbody>
                 {
                   this.state.TillApproval.map(x=> 
                       <tr key={x.Id}>
                          <td>{x.TellerId}</td>
                          <td>{x.User}</td>
                          <td>{x.Event}</td>
                          <td>{this.GetDate(x.DateCreated)}</td>
                          <td style={{textAlign:'right'}}>{x.ClosingBalance > 0 ?x.ClosingBalance.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","): '0.00'}</td>
                          <td style={{textAlign:'right', color: x.ShortageOverageAmount > 0 ? 'blue' :  x.ShortageOverageAmount < 0 ? 'red' : 'green' }}>{x.ShortageOverageAmount === 0 ? '0.00' : x.ShortageOverageAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") }</td>
                          <td>  <a onClick={this.onClickApprove} id={x.Id} href="#" className="btn btn-primary a-btn-slide-text" data-toggle="modal" data-target="#ApproveModal" >
                          <span id={x.Id} className="fa fa-thumbs-up" aria-hidden="true"></span>
                          <span><strong id={x.Id}>  Approve</strong></span>            
                        </a>
                          <a onClick={this.onClickDisapprove}  id={x.Id} href="#" className="btn btn-danger a-btn-slide-text" data-toggle="modal" data-target="#DisapproveModal" >
                          <span id={x.Id} className="fa fa-thumbs-down" aria-hidden="true"></span>
                          <span><strong id={x.Id}>  Disapprove</strong></span>  </a></td>
                       </tr>
                    )
                 }
                  </tbody>
                  </table>
                  </div>
                  </div>
            </div>
            </div>
            </div>
        </section>
        </section>

        <div className="modal fade" id="ApproveModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content" style={{width:'520px'}}>
            <div style={{background:'#a00'}} className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             <h4 className="modal-title" id="myModalLabel">Approve Till Balance</h4>
            </div>
            <div className="modal-body">
            <div className="row">
            <div className="col-md-12 ">
            <div className="portlet-body form">
              <div className="form-body">
              <p><b>Are you sure you want to approve till balance? </b></p>
              </div>
              </div>
            </div>
      
            </div>
            </div>
            <div className="modal-footer">
            <button onClick={this.ApproveTill} type="button" className="btn btn-primary" data-dismiss="modal">Approve {this.state.ApproveTillExecuting === true ? <i id="spinner" class="fa fa-spinner fa-spin"></i> : ''}</button>
            </div>
          </div>
        </div>
      </div>

      <div className="modal fade" id="DisapproveModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div className="modal-dialog">
        <div className="modal-content" style={{width:'520px'}}>
          <div style={{background:'red'}} className="modal-header">
            <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
           <h4 className="modal-title" id="myModalLabel">Disapprove Till Balance</h4>
          </div>
          <div className="modal-body">
          <div className="row">
          <div className="col-md-12 ">
          <div className="portlet-body form">
            <div className="form-body">
            <p><b>Are you sure you want to disapprove this till balance? </b></p>
            </div>
            </div>
          </div>
    
          </div>
          </div>
          <div className="modal-footer">
          <button onClick={this.DisapproveTill} type="button" className="btn btn-danger" data-dismiss="modal">Disapprove</button>
          </div>
        </div>
      </div>
    </div>

      </div>
    )
  }
}

export default BalanceApproval