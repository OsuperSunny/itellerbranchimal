import React, {Component} from "react";
import MockJson from '../apiService/mockJson';
import axios from 'axios';
import DataTable from '../shared/dataTable'
import apiUrl from '../apiService/config'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import $ from 'jquery';
import Select from 'react-select';
import GenerateToken from '../shared/token'

const userName = localStorage.getItem('Id')
class ReverseTransaction extends Component {
  constructor (props){
    super(props)
    this.state = {
    ID:0,
    TillNos:'',
    TranId:0,
    TillDesc: '',
    TransRef:'',
    GLAccountID:0,
    MinTillAmount: '0.00',
    MaxTillAmount: '0.00',
    Till:[],
    Transaction:[],
    OriginalTransaction:[],
    ReverseTransExecuting: false,
    ReversalErrorMessage:'',
    ReasonForReversal:''
}
}
ChangeHandler=e => {
  this.setState({ReasonForReversal: e.target.value})
}
onClickReverseButton = e => {
  console.log(e.target.id);
  let trans = this.state.Transaction;
  trans = trans.filter(function(data){
     return data.TranId === parseInt(e.target.id);
  })
  this.setState({TransRef: trans[0].TransRef, TranId:parseInt(e.target.id)})
}
ReverseTransaction = async e =>{
  let currentComponent = this;
  // if(this.state.ReasonForReversal === ''){
  //   currentComponent.setState({ReversalErrorMessage:'* Please input reason for reversal'})
  //   return;
  // }
  
  this.setState({ReverseTransExecuting: true});
   let data = {TransactionBranch: localStorage.getItem('Branch'), TReference: this.state.TransRef, access_token: localStorage.getItem('access_token'), TranId: this.state.TranId}
   console.log(data)
   await axios.post(apiUrl.BankService.ReverseTransaction, data)
   .then(function(response){
     console.log(response.data);
     window.$('#ReverseTransactionModal').modal('toggle');
     if(response.data.success == true){
      NotificationManager.success('Transaction reversed successfully', 'Reversed')
      let transaction = response.data.data.Transaction.filter(function(data){
        return data.IsReversed !== true && currentComponent.GetDate(data.CreationDate)  === currentComponent.GetDate(new Date());
    })
    currentComponent.setState({Transaction: transaction, ReverseTransExecuting: false})
     }else{
      NotificationManager.error('Transaction not reversed', 'Error')
      currentComponent.setState({ReverseTransExecuting: false})
     }
    
   }).catch(function(error){
     console.log(error);
   })
  
}

GetDate(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();

  today = dd  + '/' + mm + '/' + yyyy;
  console.log(today);
  return today
}

 async componentDidMount(){
   //TransRef
  await GenerateToken();
 let currentComponent = this;
 console.log(userName);
 await axios.get(apiUrl.BankService.GetTransaction)
 .then(function(response){
   console.log(response.data)
   let transaction = response.data.data.Transaction.filter(function(data){
       return data.IsReversed !== true && currentComponent.GetDate(data.CreationDate)  === currentComponent.GetDate(new Date());
   })
     currentComponent.setState({Transaction: transaction})
 })
    DataTable();
}

  render(){
    return (
      <div>
      <NotificationContainer/>
      <section id="main-content">
      <section className="wrapper">
        <div className="row">
        <div className="col-md-12">
        <h3 className="page-title">
        Reverse Transaction <small></small>
        </h3>
        </div>
        </div><br/><br/>
        <div className="row">
				<div className="col-md-12">
					<div className="portlet box grey-cascade">
            <div className="portlet-title">
            <div className="caption">
            <i className="fa fa-money"></i>
          </div>
          <div className="tools">
            <a href="javascript:;" className="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" className="config">
            </a>
            <a href="javascript:;" className="reload">
            </a>
            <a href="javascript:;" className="remove">
            </a>
          </div>
            </div>
            <div className="portlet-body">
							<div className="table-toolbar">
                  <table className="table table-striped table-bordered table-hover" id="sample_1">
                  <thead>
                  <tr>  
                    <th>
                       Transaction Reference
                    </th>
                    <th>
                    Customer Account Number
                    </th>
                    <th>
                    Currency
                    </th>
                    <th>
                  Teller ID
                    </th>
                    <th>
                     Amount
                     </th>
                     <th>
                    Action
                     </th>
                  </tr>
                  </thead>
                  <tbody>
                  {
                    this.state.Transaction.map(x=> 
                     <tr key={x.TranId}>
                     <td>{x.TransRef}</td>
                     <td>{x.CustomerAcctNos}</td>
                  <td>{x.CurrncyCode}</td>
                  <td>{x.CashierTillGL}</td>
                  <td style={{textAlign: 'right'}}>{x.TotalAmt.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                  <td> <a data-toggle="modal" data-target="#ReverseTransactionModal" id={x.TranId} onClick={this.onClickReverseButton}  href="#" className="btn btn-primary a-btn-slide-text"  data>
                  <span id={x.TranId} className="fa fa-arrow-circle-left" aria-hidden="true"></span>
                  <span id={x.TranId} ><strong id={x.TranId}>  Reverse</strong></span>  </a></td>
                     </tr> 
                      
                      )
                  }
                  </tbody>
                  </table>
                  </div>
                  </div>
            </div>
            </div>
            </div>
        </section>
        </section>


        <div className="modal fade" id="ReverseTransactionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content" style={{width:'520px'}}>
            <div style={{background:'rgb(202 188 18)'}} className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             <h4 className="modal-title" id="myModalLabel">Reverse Transaction</h4>
            </div>
            <div className="modal-body">
            <div className="row">
            <div className="col-md-12 ">
            <div className="portlet-body form">
              <div className="form-body">
              <p style={{color:'red'}}>{this.state.ReversalErrorMessage}</p>
              <p><b>Are you sure you want to reverse this transaction? </b></p>
              </div>
              </div>
            </div>
           
            </div>
            </div>
            <div className="modal-footer">
            <button onClick={this.ReverseTransaction} type="button" className="btn btn-primary">Reverse  {this.state.ReverseTransExecuting === true ? <i id="spinner" class="fa fa-spinner fa-spin"></i> : ''}</button>
            </div>
          </div>
        </div>
      </div>

      </div>
    )
  }
}

export default ReverseTransaction