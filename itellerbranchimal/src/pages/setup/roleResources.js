import React, {Component} from "react";
import MockJson from '../../apiService/mockJson';
import Select from 'react-select';
import axios from 'axios';
import apiUrl from '../../apiService/config'
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import Datatable from "../../shared/dataTable";
var Till =[];
const userName = localStorage.getItem('Id')
class RoleResources extends Component {
  constructor (props){
    super(props)
    this.state = {
    ID:0,
   Roles:[],
   Modules:[],
   Resources:[],
   ModuleResources:[],
   SelectedResources:[],
   IsEditRole:false,
   ModuleID:0,
   SaveResourcesExecuting: false
}
}

changeHandler = e=>{
  let ID = parseInt(e.target.id);
  let roles = this.state.Roles;
  let objIndex = roles.findIndex((obj => obj.RoleId === ID));
  roles[objIndex].RoleDesc = e.target.value;
  this.setState({Roles: roles, ID: ID})
}

ChangeRole = e =>{
  let roleId = parseInt(e.target.value);
  console.log(roleId);
  this.setState({ID: roleId})
  if(this.state.ModuleID > 0){
    let resources = this.state.Resources;
    let roleResources = this.state.RoleResources;
    let moduleId = this.state.ModuleID;
    // get resources for modules
    let moduleResources = resources.filter(function(data){
             return data.ModuleId === moduleId
     })
    roleResources = roleResources.filter(function(data){
              return data.RoleId === roleId;
    })
    console.log(roleResources)
    moduleResources.forEach(function(element){
      if(element.Checked !== true){
        element.Checked = false;
      }
      let executed = false;
      roleResources.forEach(function(item){
        item.Checked = false;
        if(item.ResourceId === element.ResourceId && executed === false){
          executed = true;
          element.Checked = true;
        }
      })
    })
    console.log(moduleResources)
    this.setState({ModuleResources: moduleResources, ModuleID: moduleId});
  }
}

ModuleChange = e => {
  let roleId = this.state.ID;
  if(roleId === 0){
    NotificationManager.warning('', 'Please select a role',1000);
    this.setState({ModuleID: parseInt(e.target.value)})
  }else{
    let resources = this.state.Resources;
    let roleResources = this.state.RoleResources;
    let moduleId = parseInt(e.target.value);
    // get resources for modules
    let moduleResources = resources.filter(function(data){
             return data.ModuleId === moduleId
     })
    roleResources = roleResources.filter(function(data){
              return data.RoleId === roleId;
    })
    console.log(roleResources)
    moduleResources.forEach(function(element){
      if(element.Checked !== true){
        element.Checked = false;
      }
      let executed = false;
      roleResources.forEach(function(item){
        item.Checked = false;
        if(item.ResourceId === element.ResourceId && executed === false){
          executed = true;
          element.Checked = true;
        }
      })
    })
    console.log(moduleResources)
    this.setState({ModuleResources: moduleResources, ModuleID: moduleId});
  }
 
}

// onclickResourceCheckbox = e =>{
//   let currentComponent = this;
//   console.log(parseInt(e.target.id))
//   let selectedResources = this.state.SelectedResources;
//   if(e.target.checked){
//     let resourceId = parseInt(e.target.id)
//     selectedResources.push(resourceId)
//     currentComponent.setState({SelectedResources: selectedResources})
//   }else{
//     selectedResources = selectedResources.filter(function(data){
//       return data !== parseInt(e.target.id)
//     })
//     currentComponent.setState({SelectedResources: selectedResources})
//   }
//   console.log(selectedResources);
// }

onclickResourceCheckbox = e =>{
  let currentComponent = this;
  let resourceId = parseInt(e.target.id);
  console.log(resourceId);
 let moduleResources = this.state.ModuleResources
 let objIndex = moduleResources.findIndex((obj => obj.ResourceId === resourceId));
 moduleResources[objIndex].Checked = true;
this.setState({ModuleResources: moduleResources});
}

onClickEditRole = e=>{
  this.setState({IsEditRole:true})
  let ID = parseInt(e.target.id);
  let roles = this.state.Roles;
  let objIndex = roles.findIndex((obj => obj.RoleId === ID));
  roles[objIndex].ShowSaveButton = true;
  roles[objIndex].Disabled = false;
  this.setState({Roles: roles})
}
onClickDeleteRole = e =>{
  window.$('#DeleteRoleModal').modal('toggle');
  this.setState({ID: parseInt(e.target.id)})
}
onClickAddNewRoleButton = e => {
  this.setState({IsEditRole:false})
  let newRole = {RoleId: 0, RoleDesc:'', Disabled:false, IsNew: true, ShowSaveButton: true}
  let roles = this.state.Roles;
  roles.push(newRole);
 this.setState({Roles: roles})
}
SaveRole = e=> {
  let currentComponent = this;
  let ID = parseInt(e.target.id);
  let role = this.state.Roles;
  role = role.filter(function(data){
    return data.RoleId === ID;
  });
  if(this.state.IsEditRole)
  {
     let data = {RoleId: ID, RoleDesc:role[0].RoleDesc}
     console.log(data)
     axios.post(apiUrl.Setup.UpdateRole, data)
     .then(function(response){
      console.log(response.data)
       if( response.data.success){
         NotificationManager.success('Updated', "Role Updated successfully")
        let roles =  response.data.data.Roles;
        console.log(roles)
      roles.forEach(function(element){
        element.Disabled = true;
        element.IsNew = false;
        element.ShowSaveButton = false;
      })
      currentComponent.setState({Roles: roles});
       }else{
          NotificationManager.error('Error', 'Role not updated')
       }
      }).catch(function(error){
        console.log(error)
      })
   
  }
  else
  {
    let data = { RoleDesc:role[0].RoleDesc}
    console.log(data)
    axios.post(apiUrl.Setup.CreateRole, data)
    .then(function(response){
     console.log(response.data)
      if( response.data.success){
        NotificationManager.success('Updated', "Role Created successfully")
       let roles =  response.data.data.Roles;
     roles.forEach(function(element){
       element.Disabled = true;
       element.IsNew = false;
       element.ShowSaveButton = false;
     })
     currentComponent.setState({Roles: roles,});
      }else{
         NotificationManager.error('Error', 'Role not updated')
      }
     }).catch(function(error){
       console.log(error)
     })
  }
}

DeleteRole = e =>{
  let currentComponent = this;
  let ID = this.state.ID;
  let data = { RoleId:ID}
  console.log(data)
  axios.post(apiUrl.Setup.DeleteRole, data)
  .then(function(response){
   console.log(response.data)
    if( response.data.success){
      NotificationManager.success('Deleted', "Role deleted successfully")
     let roles =  response.data.data.Roles;
     console.log(roles)
   roles.forEach(function(element){
     element.Disabled = true;
     element.IsNew = false;
     element.ShowSaveButton = false;
   })
   currentComponent.setState({Roles: roles});
    }else{
       NotificationManager.error('Error', 'Role not deleted')
    }
   }).catch(function(error){
     console.log(error)
   })
}

SaveResources = e =>{
  let currentComponent = this;
  this.setState({SaveResourcesExecuting: true});
console.log(this.state.ModuleResources)
let moduleResources = this.state.ModuleResources
let checkedResources = [];
moduleResources.forEach(function(element){
  if(element.Checked === true){
    checkedResources.push(element.ResourceId);
  }
})
console.log(checkedResources);
if(this.state.ID === 0){
  NotificationManager.error('', 'Please select a role');
  this.setState({SaveResourcesExecuting: false});
}else if(checkedResources.length === 0){
  NotificationManager.error('', 'No resources is checked');
  this.setState({SaveResourcesExecuting: false});
}else{
 // NotificationManager.success('', 'Success');
 let data = {"RoleId": this.state.ID,"ResourceId": checkedResources}
 axios.post(apiUrl.Setup.UpdateResources, data)
 .then(function(response){
   if(response.data.success){
    let roles =  response.data.data.Roles;
    console.log(response.data.data)
  roles.forEach(function(element){
    element.Disabled = true;
    element.IsNew = false;
    element.ShowSaveButton = false;
  })
  currentComponent.setState({Roles: roles,Modules:response.data.data.Modules,SaveResourcesExecuting: false,Resources:response.data.data.Resources, RoleResources:response.data.data.RoleResources});
    NotificationManager.success('Success', 'Saved successfully', 1000);
   }else{
    NotificationManager.error('Error', response.data.message, 1000);
    currentComponent.setState({SaveResourcesExecuting: false});
   }
 
 }).catch(function(error){
  console.log(error)
  currentComponent.setState({SaveResourcesExecuting: false});
  NotificationManager.error('Error', 'Server Error', 1000);
})
}
}
  async componentDidMount(){
    let currentComponent = this;
    await axios.get(apiUrl.Setup.RoleResources)
    .then(function(response){
      let roles =  response.data.data.Roles;
      console.log(response.data.data)
    roles.forEach(function(element){
      element.Disabled = true;
      element.IsNew = false;
      element.ShowSaveButton = false;
    })
    currentComponent.setState({Roles: roles,Modules:response.data.data.Modules,Resources:response.data.data.Resources, RoleResources:response.data.data.RoleResources});
   Datatable();
      console.log(response.data)
    }).catch(function(error){
      console.log(error)
    })
  //   let roles =  MockJson.Role;
  //   roles.forEach(function(element){
  //     element.Disabled = true;
  //     element.IsNew = false;
  //     element.ShowSaveButton = false;
  //   })
  //  await currentComponent.setState({Roles: roles});
  //  Datatable();
  }
  render(){
    return (
      <div>
      <NotificationContainer/>
      <section id="main-content">
      <section className="wrapper">
        <div className="row">
        <div className="col-md-12">
        <h3 className="page-title">
        Role Resources <small>update role and assign role to resources</small>
        </h3>
        </div>
        </div><br/>
        
        <div className="row">
				<div className="col-md-5">
					<div className="portlet box grey-cascade">
            <div className="portlet-title">
            <div className="caption">
            <i className="fa fa-users"></i> Role
          </div>
          <div className="tools">
            <a href="javascript:;" className="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" className="config">
            </a>
            <a href="javascript:;" className="reload">
            </a>
            <a href="javascript:;" className="remove">
            </a>
          </div>
            </div>
            <div className="portlet-body" style={{height:'450px'}}>
              <div className="table-toolbar">
                <div className="row">
                <div className="col-md-12">
                <div className="btn-group" style={{width:'100%'}}>
                  <button onClick={this.onClickAddNewRoleButton}   id="sample_editable_1_new" className="btn green">
                  Add New <i className="fa fa-plus"></i>
                  </button>
                </div>
              </div><br/>
									<div className="col-md-11">
                  <table className="table table-striped table-bordered table-hover" id="sample_1">
                  <thead>
                  <tr> 
                    <th>
                       Role Name
                    </th>
                     <th style={{width:'80px'}}>
                    Action
                     </th>
                  </tr>
                  </thead>
                  <tbody>
                  {
                    this.state.Roles.map(y=> 
                      <tr key={y.RoleId}>
                      <td><input id={y.RoleId} onChange={this.changeHandler} disabled={y.Disabled} defaultValue={y.RoleDesc} type="text" className="form-control" name="RoleDesc"/></td>
                      {
                         y.ShowSaveButton === false ? <td><a id={y.RoleId} onClick={this.onClickEditRole} dataToggle="tooltip" dataPlacement="bottom" 
                        title="Edit" style={{color:'blue', paddingLeft:'10px'}}>
                        <i id={y.RoleId} className="glyphicon glyphicon-edit" aria-hidden="true" style={{fontSize:'20px', paddingTop:'10px'}}></i></a> 
                        <a id={y.RoleId} onClick={this.onClickDeleteRole} dataToggle="modal" dataPlacement="bottom" data-target="#DeleteRoleModal"
                        title="Delete" style={{color:'#a71b1b', paddingLeft:'10px'}}>
                        <i id={y.RoleId} className="fa fa-trash" aria-hidden="true" style={{fontSize:'20px', paddingTop:'10px'}}></i></a>  </td>	:    
                        <td>
                        <a style={{backgroundColor:"#495c8d"}} onClick={this.SaveRole}  id={y.RoleId} href="#" className="btn btn-primary a-btn-slide-text">
                        <span id={y.RoleId} className="glyphicon glyphicon-plus" aria-hidden="true"></span>
                        <span id={y.RoleId}><strong id={y.RoleId}>  Save</strong></span>  </a></td>
                      }
                   
                      </tr>
                      )
                  }
                  </tbody>
                  </table>
                  </div>
                  </div>
                  </div>
                  </div>
            </div>
            </div>
            <div className="col-md-7">
            <div className="portlet box grey-cascade">
              <div className="portlet-title">
              <div className="caption">
              <i className="fa fa-th"></i> Resources
            </div>
            <div className="tools">
              <a href="javascript:;" className="collapse">
              </a>
              <a href="#portlet-config" data-toggle="modal" className="config">
              </a>
              <a href="javascript:;" className="reload">
              </a>
              <a href="javascript:;" className="remove">
              </a>
            </div>
              </div>
              <div className="portlet-body">
                <div className="table-toolbar">
                <div style={{height:'415px'}}>
                  <div className="row">
                  <div className="col-md-12">
                  <div className="portlet-body form">
                  <div className="form-body">
                  <label><b>Role</b></label>
              <select onChange={this.ChangeRole} className="form-control">
              <option value={0}>-----Select Role-----</option>
              {
                this.state.Roles.map(x=> 
                    <option value={x.RoleId}>{x.RoleDesc}</option>
                  )
              }
              </select>
             <br/>   
             <label><b>Modules</b></label>
             <select onClick={this.ModuleChange} className="form-control">
             <option value={0}>-----Select Module-----</option>
             {
               this.state.Modules.map(x=> 
                   <option value={x.ModuleId}>{x.ModuleName}</option>
                 )
             }
             </select>
            <br/> 
            <label><b>Resources</b></label>
            <div style={{overflow:'scroll',height: '120px'}}  className="form-group form-md-line-input has-success form-md-floating-label">
            {
              this.state.ModuleResources.map(x=> 
                <div   className="checkbox-list col-sm-4">
                <label>
                <input id={x.ResourceId} checked={x.Checked} onChange={this.onclickResourceCheckbox}  type="checkbox"/>{x.ResourceName}</label>
              </div>
                
                )
            }
          </div><br/>  
            <div style={{padding:'0px', textAlign:'right'}}><button onClick={this.SaveResources} type="button" className="btn btn-primary"><i class="fa fa-plus"></i> Save Changes {this.state.SaveResourcesExecuting === true ? <i id="spinner" class="fa fa-spinner fa-spin"></i> : ''}</button></div>
                  </div>
                  </div>
                  </div>
                    </div>
                    </div>
                    </div>
                    </div>
              </div>
              </div>
            </div>
         
        </section>
        </section>


        <div className="modal fade" id="DeleteRoleModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content" style={{width:'520px'}}>
            <div style={{background:'red'}} className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             <h4 className="modal-title" id="myModalLabel">Delete Role</h4>
            </div>
            <div className="modal-body">
            <div className="row">
            <div className="col-md-12 ">
            <div className="portlet-body form">
              <div className="form-body">
              <p><b>Are you sure you want to delete role? </b></p>
              </div>
              </div>
            </div>
           
            </div>
            </div>
            <div className="modal-footer">
            <button onClick={this.DeleteRole} type="button" className="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
          </div>
        </div>
      </div>

      </div>
    )
  }
}

export default RoleResources