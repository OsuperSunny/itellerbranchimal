import React, {Component} from "react";
import DataTable from '../../shared/dataTable'
import MockJson from '../../apiService/mockJson'
import axios from 'axios';
import apiUrl from '../../apiService/config'
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import $ from 'jquery';
import Select from 'react-select';

class Branch extends Component {
  constructor (props){
    super(props)
    this.state = {
    ID:0,
    Branch:[],
    GLAccountID:0,
    Status: false,
    AutoCompleteGLAccount:[],
    GLAccount:[],
    SeletedGLAccount:'',
    ShowDeleteButton:false,
    customStyles:[]
}
}

changeHandler = e =>{
  console.log(e.target.value);
  if(e.target.name === 'MinTillAmount' || e.target.name === 'MaxTillAmount')
  {
    let amount = parseFloat(e.target.value.replace(/,/g, ''));
    this.setState({
      [e.target.name]: amount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    })
  }else{
    this.setState({
      [e.target.name]: e.target.value
    })
  }
 
}

onChangeStatus = e =>{
  console.log(e.target.checked)
  this.setState({Status:e.target.checked});
}

handleGLAccountChange = (SeletedGLAccount) => {
    let glAccount = this.state.GLAccount.filter(function(data){
                return data.ID === SeletedGLAccount.value
  })
  console.log(SeletedGLAccount);
  if(glAccount.length > 0){
    this.setState({ GLAccountID: SeletedGLAccount.value, Currency: glAccount[0].Currency});
  }
}
onClickAddNew =e=>{
  window.$('#tillForm').find("input[type=text], textarea").val("");
}
oClickDeleteButton = e =>{
  let seletedTill = this.state.Till.filter(function(data){
    return data.Selected === true;
  });
  this.setState({SelectedTill: seletedTill});
}

onClickAllCheckbox = e =>{
  let till = this.state.Till;
  let showbutton = false;
  if(e.target.checked){
    till.forEach(function(element){
      element.Selected = true;
      showbutton = true;
    })
  }else {
    till.forEach(function(element){
      element.Selected = false;
      showbutton = false;
    })
  }
  this.setState({Till:till,ShowDeleteButton: showbutton})
}

onClickCheckBox= async e =>{
  let tillNum = e.target.id;
  let till = this.state.Till;
  if(e.target.checked)
  {
    let objIndex = this.state.Till.findIndex((obj => obj.TillNos == tillNum));
    till[objIndex].Selected = true;
  await this.setState({Till: till, ShowDeleteButton: true});
  }
  else
  {
    let objIndex = this.state.Till.findIndex((obj => obj.TillNos == tillNum));
    till[objIndex].Selected = false;
   await this.setState({Till: till});
  }
  let IsAnyTillSelected = this.state.Till.filter(function(data){
    return data.Selected === true;
  });
  if(IsAnyTillSelected.length <= 0){
    this.setState({ShowDeleteButton: false});
  }
  
}

onClickEditButton = async e =>{
  let ID = parseInt(e.target.id);
  
}

DeleteSelectedTill = e =>{
  let till = this.state.Till.filter(function(data){
    return data.Selected === false;
  });
  this.setState({Till: till,ShowDeleteButton:false});
  window.$("#deleteTill").modal('toggle');
}

CreateUserHandler = e =>{
  let currentComponent = this;
  let user = this.state
  let data ={UserId: user.UserId,UserName:user.FirstName + " " + user.LastName,Email:user.Email, Active: user.Status,BankCode:"", CreationDate:"",DateLastModified:"",Expired:"",ExpiryDate:"",LastLogingTime:"",Locked:"",LowerLimit:"",UpperLimit:"",Password:"",Supervisory:"",UserLevel:"",ValidDays:""}
   console.log(data);
   axios.post(apiUrl.Setup.CreateUser, data)
   .then(function(response){
     if(response.data.success){
      console.log(response.data);
      currentComponent.setState({Users: response.data.data.UserDetail})
     NotificationManager.success(response.data.message, "Updated");
     window.$('#newTill').modal('toggle');
     }else
     {
      NotificationManager.error(response.data.message, "Error")
     }
   
  }).catch(function(error){
      console.log(error);
  })
}

UpdateUserHandler = e =>{
  let currentComponent = this;
  let user = this.state
  let data ={UserId: user.UserId,UserName:user.FirstName + " " + user.LastName,Email:user.Email, Active: user.Status,BankCode:"", CreationDate:"",DateLastModified:"",Expired:"",ExpiryDate:"",LastLogingTime:"",Locked:"",LowerLimit:"",UpperLimit:"",Password:"",Supervisory:"",UserLevel:"",ValidDays:""}
   console.log(data);
   axios.post(apiUrl.Setup.UpdateUser, data)
   .then(function(response){
    if(response.data.success){
      console.log(response.data);
      currentComponent.setState({Users: response.data.data.UserDetail})
     NotificationManager.success(response.data.message, "Updated");
     window.$('#editUser').modal('toggle');
     }else
     {
      NotificationManager.error(response.data.message, "Error")
     }
   
  }).catch(function(error){
      console.log(error);
  })
}

SelectCustomStyle(){
  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      borderBottom: '1px dotted pink',
      color: state.isSelected ? 'black' : 'black',
      padding: 10,
    }),
    // control: () => ({
    //   // none of react-select's styles are passed to <Control />
    //   width: 220,
    // }),
    // singleValue: (provided, state) => {
    //   const opacity = state.isDisabled ? 0.5 : 1;
    //   const transition = 'opacity 300ms';
  
    //   return { ...provided, opacity, transition };
    // }
  }
  this.setState({customStyles})
}

 async componentDidMount(){
 this.SelectCustomStyle();
 let currentComponent = this;
await this.setState({Branch: MockJson.Branch});
    DataTable();
}

  render(){
    return (
      <div>
      <NotificationContainer/>
      <section id="main-content">
      <section className="wrapper">
        <div className="row">
        <div className="col-md-12">
        <h3 className="page-title">
        Branch <small>create, modify and delete branch</small>
        </h3>
        </div>
        </div><br/><br/>
        <div className="row">
				<div className="col-md-12">
					<div className="portlet box grey-cascade">
            <div className="portlet-title">
            <div className="caption">
            <i className="fa fa-money"></i>
          </div>
          <div className="tools">
            <a href="javascript:;" className="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" className="config">
            </a>
            <a href="javascript:;" className="reload">
            </a>
            <a href="javascript:;" className="remove">
            </a>
          </div>
            </div>
            <div className="portlet-body">
							<div className="table-toolbar">
								<div className="row">
									<div className="col-md-12">
										<div className="btn-group">
											<button onClick={this.onClickAddNew} data-toggle="modal" data-target="#newTill"  id="sample_editable_1_new" className="btn green">
											Add New <i className="fa fa-plus"></i>
											</button>
                    </div>
                  </div>
                  </div><br/>
                  <table className="table table-striped table-bordered table-hover" id="sample_1">
                  <thead>
                  <tr>  
                    <th>
                       Branch Code
                    </th>
                    <th>
                     Branch Name
                    </th>
                    <th>
                    Sort Code
                    </th>
                    <th>
                   Inter Branch Account
                    </th>
                    <th>
                    Status
                    </th>
                     <th>
                    Action
                     </th>
                  </tr>
                  </thead>
                  <tbody>
                  {
                     this.state.Branch.map(x=> 
                      <tr key={x.ID}>
                      <td>{x.BranchCode}</td>
                      <td>{x.BranchName}</td>
                      <td>{x.SortCode}</td>
                      <td>{x.InterBranchAccount}</td>
                      <td>{x.Status === true ?<span><i style={{color:'green'}} className="fa fa-play-circle"> </i> Active</span> : <span><i style={{color:'red'}} className="fa fa-stop-circle"></i> Inactive</span>}</td>
                      <td> <a onClick={this.onClickEditButton}  id={x.ID} href="#" className="btn btn-primary a-btn-slide-text" data-toggle="modal" data-target="#editUser">
                      <span id={x.ID} className="glyphicon glyphicon-edit" aria-hidden="true"></span>
                      <span id={x.ID}><strong id={x.ID}>  Edit</strong></span>  </a></td>
                      </tr>
                      )
                  }
                  </tbody>
                  </table>
                  </div>
                  </div>
            </div>
            </div>
            </div>
        </section>
        </section>

        <div className="modal fade" id="newTill" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div className="modal-dialog">
        <div className="modal-content" style={{width:'700px'}}>
          <div className="modal-header">
            <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 className="modal-title" id="myModalLabel">Add New Branch</h4>
          </div>
          <div className="modal-body">
          <div className="row">
          <form id="tillForm">
          <div className="col-md-6 ">
          <div className="portlet-body form">
            <div className="form-body">
            <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                    <input name="BranchCode" onChange={this.changeHandler} type="text" class="form-control edited"/>
                    <label for="form_control_1"><b>Branch Code</b></label>
                    <span style={{fontSize:'12px'}} className="help-block">Enter branch code..</span>
                    <i className="fa fa-key"></i>
                  </div>
                </div>
                <div className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                  <input name="Email" onChange={this.changeHandler} type="text" class="form-control edited"/>
                  <label for="form_control_1"><b>Branch Name</b></label>
                  <span style={{fontSize:'12px'}} className="help-block">Enter user email...</span>
                  <i className="fa fa-bank"></i>
                </div>
              </div>
              <div className="form-group form-md-line-input has-success form-md-floating-label">
              <div className="input-icon right">
                <input name="Email" onChange={this.changeHandler} type="text" class="form-control edited"/>
                <label for="form_control_1"><b>Branch Address</b></label>
                <span style={{fontSize:'12px'}} className="help-block">Enter user email...</span>
                <i className="fa fa-bank"></i>
              </div>
            </div>
            <div style={{marginTop:'-20px'}} className="form-group form-md-line-input has-success form-md-floating-label">
            <div className="input-icon right">
            <label for="form_control_1"><b style={{color:'#3c763d'}}>Active</b></label>
            <div class="onoffswitch">
                <input onChange={this.onChangeStatus} type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" defaultChecked={false}/>
                <label class="onoffswitch-label" for="myonoffswitch">
                    <span class="onoffswitch-inner"></span>
                    <span class="onoffswitch-switch"></span>
                </label>
            </div>
         
          </div>
          </div>
            </div>
            </div>
          </div>
          </form>
          <div className="col-md-6 ">
          <div className="portlet-body form">
            <div className="form-body">
            <div className="form-group form-md-line-input has-success form-md-floating-label">
            <div className="input-icon right">
              <input name="FirstName" onChange={this.changeHandler}  type="text" className="form-control edited"/>
              <label for="form_control_1"><b>Sort Code</b></label>
              <span style={{fontSize:'12px'}} className="help-block">Enter branch sort code...</span>
              <i className="fa fa-key"></i>
            </div>
          </div>
            <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                    <input  name="LastName" onChange={this.changeHandler}  type="text" className="form-control edited"/>
                    <label for="form_control_1"><b>Inter Branch Account</b></label>
                    <span style={{fontSize:'12px'}} className="help-block">Enter user last name...</span>
                    <i className="fa fa-user"></i>
                  </div>
                </div>
                <div className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                  <input  name="LastName" onChange={this.changeHandler}  type="text" className="form-control edited"/>
                  <label for="form_control_1"><b>Suspense Account</b></label>
                  <span style={{fontSize:'12px'}} className="help-block">Enter user last name...</span>
                  <i className="fa fa-key"></i>
                </div>
              </div>
            </div>
            </div>
          </div>
          </div>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
            <button onClick={this.CreateUserHandler} type="button" className="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>


    <div className="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div className="modal-dialog">
    <div className="modal-content" style={{width:'700px'}}>
      <div className="modal-header">
        <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 className="modal-title" id="myModalLabel">Update User</h4>
      </div>
      <div className="modal-body">
      <div className="row">
      <form id="tillForm">
      <div className="col-md-6 ">
      <div className="portlet-body form">
        <div className="form-body">
        <div className="form-group form-md-line-input has-success form-md-floating-label">
              <div className="input-icon right">
                <input name="BranchCode" onChange={this.changeHandler} type="text" class="form-control edited"/>
                <label for="form_control_1"><b>Branch Code</b></label>
                <span style={{fontSize:'12px'}} className="help-block">Enter branch code..</span>
                <i className="fa fa-key"></i>
              </div>
            </div>
            <div className="form-group form-md-line-input has-success form-md-floating-label">
            <div className="input-icon right">
              <input name="Email" onChange={this.changeHandler} type="text" class="form-control edited"/>
              <label for="form_control_1"><b>Branch Name</b></label>
              <span style={{fontSize:'12px'}} className="help-block">Enter user email...</span>
              <i className="fa fa-bank"></i>
            </div>
          </div>
          <div className="form-group form-md-line-input has-success form-md-floating-label">
          <div className="input-icon right">
            <input name="Email" onChange={this.changeHandler} type="text" class="form-control edited"/>
            <label for="form_control_1"><b>Branch Address</b></label>
            <span style={{fontSize:'12px'}} className="help-block">Enter user email...</span>
            <i className="fa fa-bank"></i>
          </div>
        </div>
        <div style={{marginTop:'-20px'}} className="form-group form-md-line-input has-success form-md-floating-label">
        <div className="input-icon right">
        <label for="form_control_1"><b style={{color:'#3c763d'}}>Active</b></label>
        <div class="onoffswitch">
            <input onChange={this.onChangeStatus} type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" defaultChecked={false}/>
            <label class="onoffswitch-label" for="myonoffswitch">
                <span class="onoffswitch-inner"></span>
                <span class="onoffswitch-switch"></span>
            </label>
        </div>
     
      </div>
      </div>
        </div>
        </div>
      </div>
      </form>
      <div className="col-md-6 ">
      <div className="portlet-body form">
        <div className="form-body">
        <div className="form-group form-md-line-input has-success form-md-floating-label">
        <div className="input-icon right">
          <input name="FirstName" onChange={this.changeHandler}  type="text" className="form-control edited"/>
          <label for="form_control_1"><b>Sort Code</b></label>
          <span style={{fontSize:'12px'}} className="help-block">Enter branch sort code...</span>
          <i className="fa fa-key"></i>
        </div>
      </div>
        <div className="form-group form-md-line-input has-success form-md-floating-label">
              <div className="input-icon right">
                <input  name="LastName" onChange={this.changeHandler}  type="text" className="form-control edited"/>
                <label for="form_control_1"><b>Inter Branch Account</b></label>
                <span style={{fontSize:'12px'}} className="help-block">Enter user last name...</span>
                <i className="fa fa-user"></i>
              </div>
            </div>
            <div className="form-group form-md-line-input has-success form-md-floating-label">
            <div className="input-icon right">
              <input  name="LastName" onChange={this.changeHandler}  type="text" className="form-control edited"/>
              <label for="form_control_1"><b>Suspense Account</b></label>
              <span style={{fontSize:'12px'}} className="help-block">Enter user last name...</span>
              <i className="fa fa-key"></i>
            </div>
          </div>
        </div>
        </div>
      </div>
      </div>
      </div>
      <div className="modal-footer">
        <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
        <button onClick={this.UpdateUserHandler} type="button" className="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

      </div>
    )
  }
}

export default Branch