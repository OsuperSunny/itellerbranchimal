import React, {Component} from "react";
import DataTable from '../../shared/dataTable'
import MockJson from '../../apiService/mockJson'
import axios from 'axios';
import apiUrl from '../../apiService/config'
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';

class CashDenominations extends Component {
  constructor (props){
    super(props)
    this.state = {
    ID:0,
    CurrencyName: '',
    RelationshipUnit: 0,
    SubunitName:'',
    Denominations:[],
    NewDenominations:[],
    CashDenominations:[],
    Abbrev:'',
    SelectedCashDenominations:{},
    ShowError: false,
    ErrorMessage: '',
}
}

changeHandler = e => {
  console.log(e.target.value)
  this.setState({
    [e.target.name]: e.target.value
  })
}

ChangeCurrencyName = e => {
  console.log(e.target.value)
  let ID = parseInt(e.target.name);
  console.log(ID)
  let denomination = this.state.NewDenominations;
  let objIndex = denomination.findIndex((obj => obj.ID == ID))
  denomination[objIndex].Name = e.target.value;
  this.setState({NewDenominations:denomination});
}

ChangeCurrencyNameOnEdit = e =>{
  console.log(e.target.value)
  let ID = parseInt(e.target.name);
  console.log(ID)
  let denomination = this.state.Denominations;
  let objIndex = denomination.findIndex((obj => obj.ID == ID))
  denomination[objIndex].Name = e.target.value;
  this.setState({Denominations:denomination});
}

ChangeValue = e => {
  let value = parseFloat(e.target.value)
  let ID = parseInt(e.target.name);
  let denomination = this.state.NewDenominations;
  let objIndex = denomination.findIndex((obj => obj.ID == ID))
  denomination[objIndex].Value = value;
  this.setState({NewDenominations:denomination});
}

ChangeValueOnEdit = e => {
  let value = parseFloat(e.target.value)
  let ID = parseInt(e.target.name);
  let denomination = this.state.Denominations;
  let objIndex = denomination.findIndex((obj => obj.ID == ID))
  denomination[objIndex].Value = value;
  this.setState({Denominations:denomination});
}

DeleteDenomination = e =>{
let ID = parseInt(e.target.id);
console.log(ID)
let currentComponent = this;
let denominations = this.state.Denominations.filter(function(data){
  return data.ID !== ID
})
this.setState({Denominations: denominations})
}

DeleteNewDenomination = e =>{
  let ID = parseInt(e.target.id);
  console.log(ID)
  let currentComponent = this;
  let denominations = this.state.NewDenominations.filter(function(data){
    return data.ID !== ID
  })
  this.setState({NewDenominations: denominations})
  }

  AddNewDenominations = e =>{
    let currentComponent = this;
  let newDenominationTemplate = {ID: Math.floor(Math.random() * 1000),Name: "", Value: 0.00}
  let denomination = currentComponent.state.NewDenominations
  denomination.push(newDenominationTemplate);
  this.setState({NewDenominations: denomination})
  }

AddDenominationsOnEdit = e =>{
  let currentComponent = this;
  let denominations = currentComponent.state.Denominations;
  console.log(denominations)
  let newDenominationTemplate = {ID: Math.floor(Math.random() * 1000),Name: "Total Amount", Value: 0.00}
  denominations.push(newDenominationTemplate);
  this.setState({Denominations:denominations});
  }

CreateCurrency = e=> {
  let currentComponent = this;
  this.setState({ShowError: false, ErrorMessage:''});
  if(this.state.Abbrev === ''){
    currentComponent.setState({ShowError:true, ErrorMessage: 'Please enter currency abbreviation'})
    return;
  }
  if(this.state.Abbrev.length !== 3){
    currentComponent.setState({ShowError:true, ErrorMessage: 'Currency abbrevation should be a three(3) letter word'})
    return;
  }
  if(this.state.CurrencyName === ''){
    currentComponent.setState({ShowError:true, ErrorMessage: 'Please enter currency name'})
    return;
  }
 let currency = this.state.SelectedCashDenominations;
 let denomination = [];
 console.log(this.state.NewDenominations)
 this.state.NewDenominations.forEach(function(element){
   let data = {Name: element.Name, Value: element.Value}
  denomination.push(data);
 })
  let data = {SubunitName: this.state.SubunitName, RelationshipUnit:this.state.RelationshipUnit, Currency: this.state.CurrencyName,Abbrev: this.state.Abbrev, Denomination:denomination}
  console.log(data);      
  axios.post(apiUrl.Security.CreateCurrency, data) 
  .then(function(response){
    if(response.data.success){
      let getDenominations = response.data.data;
      NotificationManager.success("Currency added successfully", "Updated")
      console.log(getDenominations);
      window.$('#newCashDenominations').modal('toggle');
      currentComponent.setState({CashDenominations:getDenominations})
    }else{
      NotificationManager.error(response.data.data.message, "Error")
    }

  }).catch(function(error){
    NotificationManager.error("Error Occurred", "Error")
  })

}

UpdateCurrency = e =>{
  let currentComponent = this;
  if(this.state.Abbrev === ''){
    currentComponent.setState({ShowError:true, ErrorMessage: 'Please enter currency abbreviation'})
    return;
  }
  if(this.state.Abbrev.length !== 3){
    currentComponent.setState({ShowError:true, ErrorMessage: 'Currency abbrevation should be a three(3) letter word'})
    return;
  }
  if(this.state.CurrencyName === ''){
    currentComponent.setState({ShowError:true, ErrorMessage: 'Please enter currency name'})
    return;
  }
 let currency = this.state.SelectedCashDenominations;
 let denomination = [];
 console.log(this.state.Denominations)
 this.state.Denominations.forEach(function(element){
   let data = {Name: element.Name, Value: element.Value}
  denomination.push(data);
 })
  let data = {ID: this.state.ID,SubunitName: this.state.SubunitName, RelationshipUnit:this.state.RelationshipUnit, Currency: this.state.CurrencyName,Abbrev: this.state.Abbrev, Denomination:denomination}
  console.log(data);      
  axios.post(apiUrl.Security.UpdateCurrency, data) 
  .then(function(response){
    if(response.data.success){
      let getDenominations = response.data.data;
      NotificationManager.success("Currency updated successfully", "Updated")
      console.log(getDenominations);
      window.$('#editCashDenominations').modal('toggle');
      currentComponent.setState({CashDenominations:getDenominations})
    }else{
      NotificationManager.error(response.data.data.message, "Error")
    }

  }).catch(function(error){
    NotificationManager.error("Error Occurred", "Error")
  })

}

onClickAddNewButton = e =>{
  this.setState({ShowError: false, ErrorMessage:'',CurrencyName:'',Abbrev:'',RelationshipUnit:'',NewDenominations:[]});
}
onClickEditButton = e =>{
  let ID = parseInt(e.target.id);
  let selectedCashDenomination = this.state.CashDenominations.filter(function(data){
                  return data.ID === ID
  });
  this.setState({CurrencyName:selectedCashDenomination[0].Currency, SubunitName:selectedCashDenomination[0].SubunitName, Abbrev:selectedCashDenomination[0].Abbrev,  RelationshipUnit:selectedCashDenomination[0].RelationshipUnit,  SelectedCashDenominations: selectedCashDenomination[0],Denominations: selectedCashDenomination[0].Denomination, ID: ID})
}

 async componentDidMount(){
   let currentComponent = this;
  await axios.get(apiUrl.Security.GetCurrency)
    .then(function(response){
      let getDenominations = response.data.data;
      console.log(getDenominations);
      currentComponent.setState({CashDenominations:getDenominations})
    })
  
  // let getDenominations = await MockJson.GetCashDenominations
  // this.setState({CashDenominations:getDenominations})
  // console.log(getDenominations);
    DataTable();
}

  render(){
    return (
      <div>
      <NotificationContainer/>
      <section id="main-content">
      <section className="wrapper">
        <div className="row">
        <div className="col-md-12">
        <h3 className="page-title">
        Currency Setup <small>create, edit and delete currency</small>
        </h3>
        </div>
        </div><br/><br/>
        <div className="row">
				<div className="col-md-12">
					<div className="portlet box grey-cascade">
            <div className="portlet-title">
            <div className="caption">
            <i className="fa fa-money"></i>
          </div>
          <div className="tools">
            <a href="javascript:;" className="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" className="config">
            </a>
            <a href="javascript:;" className="reload">
            </a>
            <a href="javascript:;" className="remove">
            </a>
          </div>
            </div>
            <div className="portlet-body">
							<div className="table-toolbar">
								<div className="row">
									<div className="col-md-6">
										<div className="btn-group">
											<button onClick={this.onClickAddNewButton} data-toggle="modal" data-target="#newCashDenominations"  id="sample_editable_1_new" className="btn green">
											Add New <i className="fa fa-plus"></i>
											</button>
										</div>
                  </div>
                  </div><br/>
                  <table className="table table-striped table-bordered table-hover" id="sample_1">
                  <thead>
                  <tr>
                    <th>
                       Currency 
                    </th>
                    <th>
                      Subunit Name
                    </th>
                    <th>
                      Relationship Unit
                    </th>
                    <th>
                     Action
                    </th>
                  </tr>
                  </thead>
                  <tbody>
                  {
                    this.state.CashDenominations.map(x=> 
                      <tr key={x.ID}>
                      <td>{x.Currency}</td>
                      <td>{x.SubunitName}</td>
                      <td>{x.RelationshipUnit}</td>
                      <td>
                      <a onClick={this.onClickEditButton} id={x.ID} href="#" className="btn btn-primary a-btn-slide-text" data-toggle="modal" data-target="#viewCashDenominations" >
                      <span id={x.ID} className="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                      <span><strong id={x.ID}>  View</strong></span>            
                    </a>
                      <a onClick={this.onClickEditButton}  id={x.ID} href="#" className="btn btn-primary a-btn-slide-text" data-toggle="modal" data-target="#editCashDenominations" >
                      <span id={x.ID} className="glyphicon glyphicon-edit" aria-hidden="true"></span>
                      <span><strong id={x.ID}>  Edit</strong></span>  </a>
                     </td>
                      </tr>
                      )
                  }
                  </tbody>
                  </table>
                  </div>
                  </div>
            </div>
            </div>
            </div>
        </section>
        </section>

        <div className="modal fade" id="newCashDenominations" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 className="modal-title" id="myModalLabel">Add New Cash Denominations</h4>
            </div>
            <div className="modal-body">
            <div className="row">
            {
              this.state.ShowError === true ? <div  class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                  <b>Close</b></button>
              <span class="glyphicon glyphicon-hand-right"></span> <strong>{this.state.ErrorMessage}</strong>  </div>: ''
            }
            <div className="col-md-6 ">
            <div className="portlet-body form">
              <div className="form-body">
            	<div className="form-group form-md-line-input has-success form-md-floating-label">
										<div className="input-icon right">
											<input onChange={this.changeHandler} name="CurrencyName" type="text" class="form-control edited"/>
											<label for="form_control_1"><b>Currency</b></label>
											<span style={{fontSize:'12px'}} className="help-block">Enter currency name for cash denominations...</span>
											<i className="fa fa-money"></i>
										</div>
									</div>
                  <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                    <input name="SubunitName" onChange={this.changeHandler} type="text" class="form-control edited"/>
                    <label for="form_control_1"><b>Subunit Name</b></label>
                    <span style={{fontSize:'12px'}} className="help-block">Enter subunit of currency for cash denominations eg Kobo,Penny...</span>
                    <i className="fa fa-money"></i>
                  </div>
                </div>
              </div>
              </div>
            </div>
            <div className="col-md-6 ">
            <div className="portlet-body form">
              <div className="form-body">
            	<div className="form-group form-md-line-input has-success form-md-floating-label">
										<div className="input-icon right">
											<input name="RelationshipUnit" onChange={this.changeHandler} type="number" className="form-control edited"/>
											<label for="form_control_1"><b>Relationship Unit</b></label>
											<span style={{fontSize:'12px'}} className="help-block">Enter the value of the unit for cash denominations...</span>
											<i className="fa fa-balance-scale"></i>
										</div>
                  </div>
                  <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                    <input placeholder="eg NGN,USD..." name="Abbrev" onChange={this.changeHandler} type="text" className="form-control edited"/>
                    <label for="form_control_1"><b>Currency Abbrevation</b></label>
                    <span style={{fontSize:'12px'}} className="help-block">Enter the abbrevation of currency eg NGN,USD...</span>
                    <i className="fa fa-balance-scale"></i>
                  </div>
                </div>
              </div>
              </div>
            </div>
            <div className="col-md-12">
						<div className="portlet box blue">
						<div className="portlet-title">
							<div className="caption">
								<i className="fa fa-bars"></i>Add Denominations
							</div>
            </div>
            <div className="portlet-body">
						<div className="btn-group">
											<a onClick={this.AddNewDenominations}  id="sample_editable_1_new" className="btn green">
											Add New <i className="fa fa-plus"></i>
											</a>
                    </div><br/><br/>
                    <div style={{overflow:'scroll', height:'200px'}}>
							<table className="table table-striped table-bordered table-hover">
							<thead>
							<tr>
								<th>
								Denominations
								</th>
                 <th>
								Value
								</th>
								<th>
								Action
								</th>
							</tr>
              </thead>
              <tbody>
              {
                this.state.NewDenominations.map(y=> 
                  <tr key={y.ID}>
                  <td><input name={y.ID} onChange={this.ChangeCurrencyName} autoComplete="off"  defaultValue={y.Name}
                  type="text" className="form-control" /></td>
                  <td><input name={y.ID} onChange={this.ChangeValue}  defaultValue={y.Value.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}  style={{textAlign:'right'}} autoComplete="off" 
                   type="text" className="form-control" />
                   </td>
                   <td>
                    <a id={y.ID} onClick={this.DeleteNewDenomination} dataToggle="tooltip" dataPlacement="bottom" 
                    title="Delete" style={{color:'#a71b1b', paddingLeft:'10px'}}>
                    <i id={y.ID} className="fa fa-trash" aria-hidden="true" style={{fontSize:'20px', paddingTop:'10px'}}></i></a>
                  </td>	
                  </tr>
                  )
              }
              </tbody>
              </table>
              </div>
                    </div>
            </div>
            </div>
            </div>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
              <button onClick={this.CreateCurrency} type="button" className="btn btn-primary">Save changes</button>
            </div>
          </div>
        </div>
      </div>

      <div className="modal fade" id="editCashDenominations" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 className="modal-title" id="myModalLabel">Update Cash Denominations</h4>
          </div>
          <div className="modal-body">
          <div className="row">
          {
            this.state.ShowError === true ? <div  class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                <b>Close</b></button>
            <span class="glyphicon glyphicon-hand-right"></span> <strong>{this.state.ErrorMessage}</strong>  </div>: ''
          }
          <div className="col-md-6 ">
          <div className="portlet-body form">
            <div className="form-body">
            <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                    <input onChange={this.changeHandler} name="CurrencyName" defaultValue={this.state.SelectedCashDenominations.Currency} type="text" className="form-control edited"/>
                    <label for="form_control_1"><b>Currency</b></label>
                    <span style={{fontSize:'12px'}} className="help-block">Enter currency name for cash denominations...</span>
                    <i className="fa fa-money"></i>
                  </div>
                </div>
                <div className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                  <input name="SubunitName" onChange={this.changeHandler} defaultValue={this.state.SelectedCashDenominations.SubunitName}  type="text" className="form-control edited"/>
                  <label for="form_control_1"><b>Subunit Name</b></label>
                  <span style={{fontSize:'12px'}} className="help-block">Enter subunit of currency for cash denominations...</span>
                  <i className="fa fa-money"></i>
                </div>
              </div>
            </div>
            </div>
          </div>
          <div className="col-md-6 ">
          <div className="portlet-body form">
            <div className="form-body">
            <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                    <input onChange={this.changeHandler} name="RelationshipUnit" defaultValue={this.state.SelectedCashDenominations.RelationshipUnit}  type="number" className="form-control edited"/>
                    <label for="form_control_1"><b>Relationship Unit</b></label>
                    <span style={{fontSize:'12px'}} className="help-block">Enter the value of the unit for cash denominations...</span>
                    <i className="fa fa-balance-scale"></i>
                  </div>
                </div>
                <div className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                  <input onChange={this.changeHandler} name="Abbrev" defaultValue={this.state.SelectedCashDenominations.Abbrev}  type="text" className="form-control edited"/>
                  <label for="form_control_1"><b>Currency Abbrevation</b></label>
                  <span style={{fontSize:'12px'}} className="help-block">Enter the abbrevation of currency eg NGN,USD...</span>
                  <i className="fa fa-balance-scale"></i>
                </div>
              </div>
            </div>
            </div>
          </div>
          <div className="col-md-12">
          <div className="portlet box blue">
          <div className="portlet-title">
            <div className="caption">
              <i className="fa fa-bars"></i>Add Denominations
            </div>
          </div>
          <div className="portlet-body">
          <div className="btn-group">
                    <a onClick={this.AddDenominationsOnEdit}  id="sample_editable_1_new" className="btn green">
                    Add New <i className="fa fa-plus"></i>
                    </a>
                  </div><br/><br/>
                  <div style={{overflow:'scroll', height:'200px'}}>
            <table className="table table-striped table-bordered table-hover">
            <thead>
            <tr>
              <th>
              Denominations
              </th>
               <th style={{textAlign:'center'}}>
              Value
              </th>
              <th>
              Action
              </th>
            </tr>
            </thead>
            <tbody>
            {
              this.state.Denominations.map(y=> 
                <tr key={y.ID}>
                <td><input  onChange={this.ChangeCurrencyNameOnEdit}  autoComplete="off"  defaultValue={y.Name}
                type="text" className="form-control" name={y.ID} /></td>
                <td><input onChange={this.ChangeValueOnEdit} defaultValue={y.Value.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}  style={{textAlign:'right'}} autoComplete="off" 
                 type="text" className="form-control" name={y.ID} />
                 </td>
                 <td>
                  <a id={y.ID} onClick={this.DeleteDenomination} dataToggle="tooltip" dataPlacement="bottom" 
                  title="Delete" style={{color:'#a71b1b', paddingLeft:'10px'}}>
                  <i id={y.ID} className="fa fa-trash" aria-hidden="true" style={{fontSize:'20px', paddingTop:'10px'}}></i></a>
                </td>	
                </tr>
                )
            }
            </tbody>
            </table>
            </div>
                  </div>
          </div>
          </div>
          </div>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
            <button onClick={this.UpdateCurrency} type="button" className="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>

    <div className="modal fade" id="viewCashDenominations" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div className="modal-dialog">
      <div className="modal-content">
        <div className="modal-header">
          <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 className="modal-title" id="myModalLabel"> Cash Denominations Details</h4>
        </div>
        <div className="modal-body">
        <div className="row">
        <div className="col-md-6 ">
        <div className="portlet-body form">
          <div className="form-body">
          <div className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                  <input value={this.state.SelectedCashDenominations.Currency} disabled={true} readOnly={true} type="text" class="form-control edited"/>
                  <label for="form_control_1"><b>Currency</b></label>
                  <i className="fa fa-money"></i>
                </div>
              </div>
              <div className="form-group form-md-line-input has-success form-md-floating-label">
              <div className="input-icon right">
                <input value={this.state.SelectedCashDenominations.SubunitName} disabled={true} readOnly={true} type="text" class="form-control edited"/>
                <label for="form_control_1"><b>Subunit Name</b></label>
              </div>
            </div>
          </div>
          </div>
        </div>
        <div className="col-md-6 ">
        <div className="portlet-body form">
          <div className="form-body">
          <div className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                  <input value={this.state.SelectedCashDenominations.RelationshipUnit} disabled={true} readOnly={true} type="text" className="form-control edited"/>
                  <label for="form_control_1"><b>Relationship Unit</b></label>
                  <i className="fa fa-balance-scale"></i>
                </div>
              </div>
              <div className="form-group form-md-line-input has-success form-md-floating-label">
              <div className="input-icon right">
                <input value={this.state.SelectedCashDenominations.Abbrev} disabled={true} readOnly={true} type="text" className="form-control edited"/>
                <label for="form_control_1"><b>Currency Abbreviation</b></label>
                <i className="fa fa-balance-scale"></i>
              </div>
            </div>
          </div>
          </div>
        </div>
        <div className="col-md-12">
        <div className="portlet box blue">
        <div className="portlet-title">
          <div className="caption">
            <i className="fa fa-bars"></i>Denominations
          </div>
        </div>
        <div className="portlet-body">
                <div style={{overflow:'scroll', height:'200px'}}>
          <table className="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>
            Denominations
            </th>
             <th style={{textAlign:'center'}}>
            Value
            </th>
          </tr>
          </thead>
          <tbody>
          {
            this.state.Denominations.sort((a, b) => parseFloat(b.Value) - parseFloat(a.Value)).map(y=> 
              <tr key={y.ID}>
              <td><input disabled={true} readOnly={true} autoComplete="off"  defaultValue={y.Name}
              type="text" className="form-control" name="Balance" /></td>
              <td><input disabled={true} readOnly={true} value={y.Value.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}  style={{textAlign:'right'}} autoComplete="off" 
               type="text" className="form-control" name="Balance" />
               </td>	
              </tr>
              )
          }
          </tbody>
          </table>
          </div>
                </div>
        </div>
        </div>
        </div>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

      </div>
    )
  }
}

export default CashDenominations