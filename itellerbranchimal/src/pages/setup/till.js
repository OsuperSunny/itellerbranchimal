import React, {Component} from "react";
import DataTable from '../../shared/dataTable'
import MockJson from '../../apiService/mockJson'
import axios from 'axios';
import apiUrl from '../../apiService/config'
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import $ from 'jquery';
import Select from 'react-select';

class Till extends Component {
  constructor (props){
    super(props)
    this.state = {
    ID:0,
    TillNos:'',
    TillDesc: '',
    GLAccountID:0,
    Status: true,
    MinTillAmount: '0.00',
    MaxTillAmount: '0.00',
    Till:[],
    TillObj:{},
    Status:false,
    SelectedTill:[],
    Currency:'',
    AutoCompleteGLAccount:[],
    GLAccount:[],
    SeletedGLAccount:'',
    ShowDeleteButton:false,
    customStyles:[]
}
}

changeHandler = e =>{
  console.log(e.target.value);
  if(e.target.name === 'MinTillAmount' || e.target.name === 'MaxTillAmount')
  {
    let amount = parseFloat(e.target.value.replace(/,/g, ''));
    this.setState({
      [e.target.name]: amount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    })
  }else{
    this.setState({
      [e.target.name]: e.target.value
    })
  }
 
}

onChangeStatus = e =>{
  console.log(e.target.checked)
  this.setState({Status:e.target.checked});
}

handleGLAccountChange = (SeletedGLAccount) => {
    let glAccount = this.state.GLAccount.filter(function(data){
                return data.ID === SeletedGLAccount.value
  })
  console.log(SeletedGLAccount);
  if(glAccount.length > 0){
    this.setState({ GLAccountID: SeletedGLAccount.value, Currency: glAccount[0].Currency});
  }
}
onClickAddNew =e=>{
  window.$('#tillForm').find("input[type=text], textarea").val("");
}
oClickDeleteButton = e =>{
  let seletedTill = this.state.Till.filter(function(data){
    return data.Selected === true;
  });
  this.setState({SelectedTill: seletedTill});
}

onClickAllCheckbox = e =>{
  let till = this.state.Till;
  let showbutton = false;
  if(e.target.checked){
    till.forEach(function(element){
      element.Selected = true;
      showbutton = true;
    })
  }else {
    till.forEach(function(element){
      element.Selected = false;
      showbutton = false;
    })
  }
  this.setState({Till:till,ShowDeleteButton: showbutton})
}

onClickCheckBox= async e =>{
  let tillNum = e.target.id;
  let till = this.state.Till;
  if(e.target.checked)
  {
    let objIndex = this.state.Till.findIndex((obj => obj.TillNos == tillNum));
    till[objIndex].Selected = true;
  await this.setState({Till: till, ShowDeleteButton: true});
  }
  else
  {
    let objIndex = this.state.Till.findIndex((obj => obj.TillNos == tillNum));
    till[objIndex].Selected = false;
   await this.setState({Till: till});
  }
  let IsAnyTillSelected = this.state.Till.filter(function(data){
    return data.Selected === true;
  });
  if(IsAnyTillSelected.length <= 0){
    this.setState({ShowDeleteButton: false});
  }
  
}

onClickEditButton = async e =>{
  let ID = parseInt(e.target.id);
  let tillObj = this.state.Till.filter(function(data){
          return  data.ID === ID
  });
  console.log(tillObj[0].MaxTillAmount);
  let till = tillObj[0];
  console.log(till.Status);

 await this.setState({TillObj: tillObj[0], GLAccountID: till.GLAccountID, ID:ID,TillNos:till.TillNos,TillDesc:till.TillDesc, Status: till.Status,
  MaxTillAmount: till.MaxTillAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),MinTillAmount: till.MinTillAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")});
}

DeleteSelectedTill = e =>{
  let till = this.state.Till.filter(function(data){
    return data.Selected === false;
  });
  this.setState({Till: till,ShowDeleteButton:false});
  window.$("#deleteTill").modal('toggle');
}

CreateTillHandler = e =>{
  let currentComponent = this;
  let till = this.state
  let maxAmount = parseFloat(till.MaxTillAmount.replace(/,/g, ''));
  let minAmount = parseFloat(till.MinTillAmount.replace(/,/g, ''));
  let data ={TillNos: till.TillNos,TillDesc:till.TillDesc,IsVault:true,SortCode:"051",Status:till.Status,MinTillAmount:minAmount,MaxTillAmount:maxAmount, GLAccountID: till.GLAccountID}
   console.log(data);
   axios.post(apiUrl.Setup.CreateTill, data)
   .then(function(response){
     if(response.data.success){
      let getTill =  response.data.data.TillSetup;
      getTill.forEach(function(element){
        element.Selected = false;
      })
     currentComponent.setState({Till:getTill})
     NotificationManager.success(response.data.message, "Updated");
     window.$('#newTill').modal('toggle');
     }else
     {
      NotificationManager.error(response.data.message, "Error")
     }
   
  }).catch(function(error){
      console.log(error);
  })
}

UpdateTillHandler = e =>{
  let currentComponent = this;
  let till = this.state
  let maxAmount = parseFloat(till.MaxTillAmount.replace(/,/g, ''));
  let minAmount = parseFloat(till.MinTillAmount.replace(/,/g, ''));
  let data ={ID: till.ID, TillNos: till.TillNos,TillDesc:till.TillDesc,IsVault:true,Status:till.Status,SortCode:"051", MinTillAmount:minAmount,MaxTillAmount:maxAmount, GLAccountID: till.GLAccountID}
   console.log(data);
   axios.post(apiUrl.Setup.UpdateTill, data)
   .then(function(response){
     if(response.data.success){
      let getTill =  response.data.data.TillSetup;
      getTill.forEach(function(element){
        element.Selected = false;
      })
     currentComponent.setState({Till:getTill})
     NotificationManager.success(response.data.message, "Updated");
     window.$('#editTill').modal('toggle');
     }else
     {
      NotificationManager.error(response.data.message, "Error")
     }
   
  }).catch(function(error){
      console.log(error);
  })
}

SelectCustomStyle(){
  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      borderBottom: '1px dotted pink',
      color: state.isSelected ? 'black' : 'black',
      padding: 10,
    }),
    // control: () => ({
    //   // none of react-select's styles are passed to <Control />
    //   width: 220,
    // }),
    // singleValue: (provided, state) => {
    //   const opacity = state.isDisabled ? 0.5 : 1;
    //   const transition = 'opacity 300ms';
  
    //   return { ...provided, opacity, transition };
    // }
  }
  this.setState({customStyles})
}

 async componentDidMount(){
 this.SelectCustomStyle();
 let currentComponent = this;
 await axios.get(apiUrl.Setup.GetTill)
 .then(function(response){
   let getTill =  response.data.data.TillSetup;
    getTill.forEach(function(element){
      element.Selected = false;
    })
    let gLAccount = response.data.data.GLAccount;
    let autoCompleteGLAccount = [];
    gLAccount.forEach(function(element){
      let data = { value: element.ID, label: element.GLAcctNo + " (" + element.GLAcctName + ")"  };
      autoCompleteGLAccount.push(data);
    })
   console.log(getTill);
   currentComponent.setState({Till:getTill,AutoCompleteGLAccount: autoCompleteGLAccount,GLAccount:gLAccount})
 })
    DataTable();
}

  render(){
    return (
      <div>
      <NotificationContainer/>
      <section id="main-content">
      <section className="wrapper">
        <div className="row">
        <div className="col-md-12">
        <h3 className="page-title">
        Till <small>create, modify and delete Till</small>
        </h3>
        </div>
        </div><br/><br/>
        <div className="row">
				<div className="col-md-12">
					<div className="portlet box grey-cascade">
            <div className="portlet-title">
            <div className="caption">
            <i className="fa fa-money"></i>
          </div>
          <div className="tools">
            <a href="javascript:;" className="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" className="config">
            </a>
            <a href="javascript:;" className="reload">
            </a>
            <a href="javascript:;" className="remove">
            </a>
          </div>
            </div>
            <div className="portlet-body">
							<div className="table-toolbar">
								<div className="row">
									<div className="col-md-12">
										<div className="btn-group">
											<button onClick={this.onClickAddNew} data-toggle="modal" data-target="#newTill"  id="sample_editable_1_new" className="btn green">
											Add New <i className="fa fa-plus"></i>
											</button>
                    </div>
                    {
                      this.state.ShowDeleteButton === true ?  <div style={{paddingLeft:'1010px'}} className="btn-group">
                      <button onClick={this.oClickDeleteButton} data-toggle="modal" data-target="#deleteTill"  id="sample_editable_1_new" className="btn red">
                      Delete Selected <i className="fa fa-remove"></i>
                      </button>
                    </div> : ""
                    }
                  </div>
                  </div><br/>
                  <table className="table table-striped table-bordered table-hover" id="sample_1">
                  <thead>
                  <tr>
                   <th style={{width:'10px'}}>
							<input type="checkbox" onClick={this.onClickAllCheckbox} /> Check All
								</th>   
                    <th>
                       Till Code
                    </th>
                    <th>
                     GL Account
                    </th>
                    <th>
                    Currency
                    </th>
                    <th>
                   Minimum Till Amount
                    </th>
                    <th>
                    Maximum Till Amount
                     </th>
                     <th>
                    Action
                     </th>
                  </tr>
                  </thead>
                  <tbody>
                  {
                    this.state.Till.map(x=> 
                      <tr className="odd gradeX" style={x.Selected === true ? 
                        {backgroundColor:'#C0C0C0'} : {backgroundColor:'white'}} key={x.TillNos}>
                      <td><input onClick={this.onClickCheckBox} id={x.TillNos} type="checkbox" /></td>
                      <td>{x.TillNos}</td>
                      <td>{x.GLAcctNo}  ({x.GLAcctName})</td>
                      <td>{x.Currency}</td>
                      <td style={{textAlign:'right'}}>{x.MinTillAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                      <td style={{textAlign:'right'}}>{x.MaxTillAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                      <td> <a onClick={this.onClickEditButton}  id={x.ID} href="#" className="btn btn-primary a-btn-slide-text" data-toggle="modal" data-target="#editTill">
                      <span id={x.ID} className="glyphicon glyphicon-edit" aria-hidden="true"></span>
                      <span id={x.ID}><strong id={x.ID}>  Edit</strong></span>  </a></td>
                      </tr> 
                      )
                  }
                  </tbody>
                  </table>
                  </div>
                  </div>
            </div>
            </div>
            </div>
        </section>
        </section>

        <div className="modal fade" id="newTill" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content" style={{width:'700px'}}>
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 className="modal-title" id="myModalLabel">Add New Till</h4>
            </div>
            <div className="modal-body">
            <div className="row">
            <form id="tillForm">
            <div className="col-md-6 ">
            <div className="portlet-body form">
              <div className="form-body">
            	<div className="form-group form-md-line-input has-success form-md-floating-label">
										<div className="input-icon right">
											<input name="TillNos" onChange={this.changeHandler} type="text" class="form-control edited"/>
											<label for="form_control_1"><b>Till Code</b></label>
											<span style={{fontSize:'12px'}} className="help-block">Enter Till Number...</span>
											<i className="fa fa-key"></i>
										</div>
									</div>
                  <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                    <input name="TillDesc" onChange={this.changeHandler} type="text" class="form-control edited"/>
                    <label for="form_control_1"><b>Till Description</b></label>
                    <span style={{fontSize:'12px'}} className="help-block">Enter Till description...</span>
                    <i className="fa fa-file-text-o"></i>
                  </div>
                </div>
                <div style={{marginTop:'-20px'}} className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                <label for="form_control_1"><b style={{color:'#3c763d'}}>GL Account</b></label>
                <Select
                defaultOptions
                styles={this.state.customStyles}
                onChange={this.handleGLAccountChange}
                options={this.state.AutoCompleteGLAccount}
                theme={theme => ({
                  ...theme,
                  borderRadius: 0,
                  colors: {
                    ...theme.colors,
                    primary25: '#428bca',
                    primary: 'white',
                  },
                })}
              />
              </div>
              </div>
              <div style={{marginTop:'-20px'}} className="form-group form-md-line-input has-success form-md-floating-label">
              <div className="input-icon right">
              <label for="form_control_1"><b style={{color:'#3c763d'}}>Till Status</b></label>
              <div class="onoffswitch">
                  <input onChange={this.onChangeStatus} type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" defaultChecked={false}/>
                  <label class="onoffswitch-label" for="myonoffswitch">
                      <span class="onoffswitch-inner"></span>
                      <span class="onoffswitch-switch"></span>
                  </label>
              </div>
           
            </div>
            </div>
              </div>
              </div>
            </div>
            </form>
            <div className="col-md-6 ">
            <div className="portlet-body form">
              <div className="form-body">
              <div className="form-group form-md-line-input has-success form-md-floating-label">
              <div className="input-icon right">
                <input value={this.state.MinTillAmount} name="MinTillAmount" onChange={this.changeHandler}  style={{textAlign:'right'}} type="text" className="form-control edited"/>
                <label for="form_control_1"><b>Min Till Amount</b></label>
                <span style={{fontSize:'12px'}} className="help-block">Enter minimum Till amount...</span>
                <i className="fa fa-money"></i>
              </div>
            </div>
            	<div className="form-group form-md-line-input has-success form-md-floating-label">
										<div className="input-icon right">
											<input value={this.state.MaxTillAmount} name="MaxTillAmount" onChange={this.changeHandler} style={{textAlign:'right'}} type="text" className="form-control edited"/>
											<label for="form_control_1"><b>Max Till Amount</b></label>
											<span style={{fontSize:'12px'}} className="help-block">Enter maximum Till amount...</span>
											<i className="fa fa-money"></i>
										</div>
                  </div>
                <div className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                  <input value={this.state.Currency} readOnly={true} disabled={true} type="text" class="form-control edited"/>
                  <label for="form_control_1"><b>Currency</b></label>
                  <i className="fa fa-money"></i>
                </div>
              </div>
              </div>
              </div>
            </div>
            </div>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
              <button onClick={this.CreateTillHandler} type="button" className="btn btn-primary">Save changes</button>
            </div>
          </div>
        </div>
      </div>

      <div className="modal fade" id="editTill" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div className="modal-dialog">
      <div className="modal-content">
        <div className="modal-header">
          <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 className="modal-title" id="myModalLabel">Update Till</h4>
        </div>
        <div className="modal-body">
        <div className="row">
        <div className="col-md-6 ">
        <div className="portlet-body form">
          <div className="form-body">
          <div className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                  <input onChange={this.changeHandler} name="TillNos" defaultValue={this.state.TillNos} type="text" class="form-control edited"/>
                  <label for="form_control_1"><b>Till Code</b></label>
                  <span style={{fontSize:'12px'}} className="help-block">Enter Till Number...</span>
                  <i className="fa fa-key"></i>
                </div>
              </div>
              <div className="form-group form-md-line-input has-success form-md-floating-label">
              <div className="input-icon right">
                <input onChange={this.changeHandler} name="TillDesc" defaultValue={this.state.TillDesc} type="text" class="form-control edited"/>
                <label for="form_control_1"><b>Till Description</b></label>
                <span style={{fontSize:'12px'}} className="help-block">Enter Till description...</span>
                <i className="fa fa-file-text-o"></i>
              </div>
            </div>
            <div style={{marginTop:'-20px'}} className="form-group form-md-line-input has-success form-md-floating-label">
            <div className="input-icon right">
            <label for="form_control_1"><b style={{color:'#3c763d'}}>GL Account</b></label>
            <Select
            placeholder={this.state.TillObj.GLAcctNo + " (" + this.state.TillObj.GLAcctName +")"}
            defaultOptions
            styles={this.state.customStyles}
            onChange={this.handleGLAccountChange}
            options={this.state.AutoCompleteGLAccount}
            theme={theme => ({
              ...theme,
              borderRadius: 0,
              colors: {
                ...theme.colors,
                primary25: '#428bca',
                primary: 'white',
              },
            })}
          />
          </div>
          </div>
          <div style={{marginTop:'-20px'}} className="form-group form-md-line-input has-success form-md-floating-label">
          <div className="input-icon right">
          <label for="form_control_1"><b style={{color:'#3c763d'}}>Till Status</b></label>
          <div class="onoffswitch2">
              <input onChange={this.onChangeStatus} type="checkbox" name="onoffswitch2" class="onoffswitch2-checkbox" id="myonoffswitch2" checked={this.state.Status}/>
              <label class="onoffswitch2-label" for="myonoffswitch2">
                  <span class="onoffswitch2-inner"></span>
                  <span class="onoffswitch2-switch"></span>
              </label>
          </div>
       
        </div>
        </div>
          </div>
          </div>
        </div>
        <div className="col-md-6 ">
        <div className="portlet-body form">
          <div className="form-body">
          <div className="form-group form-md-line-input has-success form-md-floating-label">
          <div className="input-icon right">
            <input onChange={this.changeHandler} name="MinTillAmount" style={{textAlign:'right'}} value={this.state.MinTillAmount} type="text" className="form-control edited"/>
            <label for="form_control_1"><b>Min Till Amount</b></label>
            <span style={{fontSize:'12px'}} className="help-block">Enter minimum Till amount...</span>
            <i className="fa fa-money"></i>
          </div>
        </div>
          <div className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                  <input onChange={this.changeHandler} name="MaxTillAmount" style={{textAlign:'right'}} value={this.state.MaxTillAmount} type="text" className="form-control edited"/>
                  <label for="form_control_1"><b>Max Till Amount</b></label>
                  <span style={{fontSize:'12px'}} className="help-block">Enter maximum Till amount...</span>
                  <i className="fa fa-money"></i>
                </div>
              </div>
              <div className="form-group form-md-line-input has-success form-md-floating-label">
              <div className="input-icon right">
                <input disabled readOnly defaultValue={this.state.TillObj.Currency} type="text" class="form-control edited"/>
                <label for="form_control_1"><b>Currency</b></label>
                <span style={{fontSize:'12px'}} className="help-block">Enter Till description...</span>
                <i className="fa fa-file-text-o"></i>
              </div>
            </div>
          </div>
          </div>
        </div>
        </div>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
          <button onClick={this.UpdateTillHandler} type="button" className="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
    </div>


    <div className="modal fade" id="deleteTill" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div className="modal-dialog">
    <div className="modal-content">
      <div className="modal-header">
        <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 className="modal-title" id="myModalLabel">Delete Till</h4>
      </div>
      <div className="modal-body">
      <div className="row">
      <div className="col-md-12">
      <div className="portlet-body form">
        <div className="form-body">
       <label style={{paddingLeft:"14px"}}><b>Are you sure you want to delete the below selected Till numbers</b></label><br/>
       {
         this.state.SelectedTill.map(x=> 
          <div className="col-md-1">
          <span>{x.TillNumber}</span>
          </div>
          )
       }
        </div>
        </div>
      </div>
      </div>
      </div>
      <div className="modal-footer">
        <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
        <button onClick={this.DeleteSelectedTill} type="button" className="btn btn-danger">Delete</button>
      </div>
    </div>
  </div>
  </div>

      </div>
    )
  }
}

export default Till