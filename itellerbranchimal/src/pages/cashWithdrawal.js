import React, {Component} from "react";
import MockJson from '../apiService/mockJson';
import axios from 'axios';
import apiUrl from '../apiService/config'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import GenerateToken from '../shared/token'
import $ from 'jquery';

const userName = localStorage.getItem('Id')
const ExampleCustomInput = ({ value, onClick }) => (
  <input className="form-control label-success" onClick={onClick} value={value} style={{width:'150px'}}/>
);
function loadAmountFormat(){
  $("input[data-type='currency']").on({
    keyup: function() {
      console.log("exec")
      formatCurrency($(this));
    },
    blur: function() { 
      console.log("exec")
      formatCurrency($(this), "blur");
    }
});

function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val =  left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val =  input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ".00";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}  
}

class CashWithdrawal extends Component {
  constructor (props){
    super(props)
    this.state = {
    ID:0,
    WithdrawalAmount:0.00,
    AmountTobePaid:'0.00',
    CashWithDrawal:{},
    UserTill:{},
    WithdrawalDetails:{},
    UserGLAccountNumber:'',
    FromTillTransfer: false,
    Denominations:[],
    AccountNumber:'',
    TillTransferID:0,
    startDate: new Date(),
    valueDate:new Date(),
    ShowError: false,
    ErrorMessage: '',
    MandateImage:'',
    Mandate:[],
    AcceptMandate:false,
    MandateSignature:'',
    AcceptMemo:false,
    ChequeNumber:'',
    Beneficiary:'',
    Remarks:'',
    APIdata:{},
    TransRef:'',
    ChequeStatus:'',
    IsT24:'',
    IsVault: false,
    UserDetails:{},
    TransactionParty:'',
    AccountFieldMessage:'Enter customer account number to populate details',
    AccountFieldColor:'',
    SaveTransExecuting: false,
    ShowNairaEquivalentButton: false,
    CurrencyRateDetails:{},
    ConvertedAmount:0.00,
    CIFTeller:'',
    BranchCode:'',
    CurrencyID:0,
    DisableButton:false,
    otp:'',
    TellerDetails:{},
    IsTellerValid:false,
    DisableBeneFiciary: false,
    IsLoading: true
}
}
ChangeOTP = e =>{
  this.setState({otp:e.target.value})
}


ChangeTransactionParty = e =>{
  if(e.target.value !== '0'){
    this.setState({TransactionParty: e.target.value})
    if(e.target.value === 'E'){
      this.setState({DisableBeneFiciary: false})
    }else if(e.target.value === 'M'){// self
      this.setState({DisableBeneFiciary: true})
    }
  }
}
ClearState = e => {
  this.setState({ CashWithDrawal:{}})
}

setStartDate(date){
  console.log(date);
 this.setState({ startDate: date})
}


setValueDate(date){
  console.log(date);
 this.setState({ valueDate: date})
}

ChangeRemarks = e =>{
  this.setState({Remarks: e.target.value})
}

ChangeHandler = e =>{
  this.setState({[e.target.name]: e.target.value})
}

ChangeWithdrawalAmount = e=>{
  let currentComponent = this;
  console.log(e.key)
  let amount = parseFloat(e.target.value.replace(/,/g, ''));
  console.log(amount);
  // if(e.key === 'Tab'){
  //   currentComponent.setState({AmountTobePaid: amount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")})
  // }
    if(amount >= 0){
      currentComponent.setState({AmountTobePaid: amount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")})
    }
}

ChangeCoreBanking = e =>{
  console.log(e.target.value)
  if(parseInt(e.target.value) > 0)
  {
    let isT24 = parseInt(e.target.value) === 1 ? true : false;
    console.log(isT24)
    this.setState({IsT24:isT24});
  }
}

onChangeOfBeneficiary = e =>{
  this.setState({Beneficiary: e.target.value});
}

onChangeOfAccountNumber = async e =>{
  let accountNumber = e.target.value;
  let currentComponent = this;
  currentComponent.setState({AccountFieldColor:'', AccountFieldMessage:'',ShowNairaEquivalentButton: false});
  if(accountNumber.length >= 10)
  {
    await GenerateToken();
    await axios.get(apiUrl.BankService.CustomerDetails + accountNumber + '/Token/' + localStorage.getItem('access_token'))
    .then(function(response){
      console.log(accountNumber)
      let cashWithDrawal = response.data;
      if(cashWithDrawal.Abbrev != null){
         axios.get(apiUrl.Security.GetCurrency)
        .then(function(response){
          console.log(response.data.data);
          console.log(cashWithDrawal);
          let getDenominations = response.data.data;
          if(cashWithDrawal.Abbrev !== "NGN"){
            currentComponent.setState({ShowNairaEquivalentButton: true});
        }
          getDenominations = getDenominations.filter(function(data){
            return data.Abbrev === cashWithDrawal.Abbrev
          })
          currentComponent.ChangeCurrency(getDenominations[0].CurrencyCode);
          console.log(getDenominations);
          getDenominations[0].Denomination.forEach(function(element){
             element.Amount = 0.00;
             element.Count = 0
          })
          currentComponent.setState({AccountFieldColor:'#3c763d', AccountFieldMessage:'This account is valid',Beneficiary:cashWithDrawal.AccountName});
          currentComponent.setState({Denominations:getDenominations[0].Denomination, CurrencyID:getDenominations[0].ID})
        })
        currentComponent.setState({AccountFieldColor:'#3c763d', AccountFieldMessage:'This account is valid'});
        currentComponent.setState({CashWithDrawal: cashWithDrawal,WithdrawalAmount:0.00,Beneficiary:cashWithDrawal.AccountName});
      }else{
        currentComponent.setState({AccountFieldColor:'red', AccountFieldMessage:'* Account is not valid'});
        console.log('invalid')
        currentComponent.setState({CashWithDrawal: {},WithdrawalAmount:0.00});
      }
    }).catch(function(error){
      console.log(error)
      currentComponent.setState({CashWithDrawal: {},AccountFieldColor:'red', AccountFieldMessage:'* Account is not valid'});
    })
  }else
  {
    currentComponent.setState({CashWithDrawal: {}, WithdrawalAmount:0.00, AccountFieldColor:'red', AccountFieldMessage:'* Account is not valid'});
  }
}

ChangeCurrency (currencyCode){
  let currentComponent = this;
    axios.get(apiUrl.BankService.GetTellerDetails  + localStorage.getItem("Id") + '/CurrCode/' + currencyCode +  '/Token/' + localStorage.getItem('access_token'))
   .then(function(response){
     console.log(response.data);
     let tellerDetails = response.data.details
     if(Array.isArray(tellerDetails)){
      if(tellerDetails.length > 0){
        currentComponent.setState({TellerDetails: response.data.details[0],IsTellerValid:true})
      }else{
       currentComponent.setState({IsTellerValid:false})
      }
     }else{
       if(Object.keys(tellerDetails).length === 0 ){
        currentComponent.setState({IsTellerValid:false})
       }else{
        currentComponent.setState({TellerDetails: response.data.details,IsTellerValid:true})
       }
     }
   
   }).catch(function(error){
    currentComponent.setState({IsTellerValid:false})
     console.log(error);
   })
  }

onChangeOfChequeNumber = async  e =>{
  let chequeNumber = e.target.value;
  let currentComponent = this;
  currentComponent.setState({ChequeNumber: '', ChequeStatus:'',
    startDate: new Date()});
  if(chequeNumber.length === 8)
  {
    await axios.get(apiUrl.BankService.ChequeDetails + chequeNumber)
    .then(function(response){
      console.log(response.data)
      console.log(currentComponent.GetDate(response.data.details.ChequeDate))
      currentComponent.setState({ChequeNumber: chequeNumber, ChequeStatus:response.data.details.Status,
        startDate: new Date(response.data.details.ChequeDate)});
    });
  }
  currentComponent.setState({ChequeNumber: e.target.value});
}

onChangeCount = e =>{
  let ID = parseInt(e.target.id);
  console.log(ID)
  let countValue = parseFloat(e.target.value);
  countValue = Number.isNaN(countValue) === true ? 0 : countValue;
  console.log(countValue);
  let cashWithDrawal = this.state.Denominations;
  let objIndex = cashWithDrawal.findIndex((obj => obj.ID == ID));
  console.log(countValue * cashWithDrawal[objIndex].Amount)
  let cashWithdrawalValue = cashWithDrawal[objIndex].Value === 0 ? 1 : cashWithDrawal[objIndex].Value;
  cashWithDrawal[objIndex].Amount = countValue * cashWithdrawalValue;
  cashWithDrawal[objIndex].Count = countValue;
  this.ComputeTotalAmount(cashWithDrawal);
  this.setState({Denominations: cashWithDrawal});
}

ComputeTotalAmount(denominations){
  let withdrawalAmount = 0.00
  denominations.forEach(function(element){
    withdrawalAmount = withdrawalAmount + element.Amount;
  });
  this.setState({WithdrawalAmount: withdrawalAmount});
}

CheckNairaEquivalent = e =>{
  let currentComponent = this;
  let convertedAmount = this.state.ConvertedAmount;
  let amountToBePaid = parseFloat(this.state.AmountTobePaid.replace(/,/g, ''));
  if(this.state.AmountTobePaid === '0.00' || amountToBePaid <= 0){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Please input an amount.',VaultExecuting:false})
    return;
  }

  axios.get(apiUrl.BankService.GetCurrencyRate + this.state.CurrencyCode + '/Token/' + localStorage.getItem('access_token'))
  .then(function(response){
    console.log(response.data.CurrencyRateDetails);
    convertedAmount = parseFloat(response.data.CurrencyRateDetails.CCY_SELL_RATE) * amountToBePaid
    currentComponent.setState({CurrencyRateDetails:response.data.CurrencyRateDetails, ConvertedAmount: convertedAmount})
    window.$('#NairaEquivalentModal').modal('toggle')
  })
  
}

ProceedTransaction = e =>{
console.log(this.state.APIdata);
let currentComponent = this;
this.state.APIdata.NeededApproval = true;
axios.post(apiUrl.BankService.CreateWithdrawalApproval,this.state.APIdata)
.then(function(response){
  console.log(response.data);
  if(response.data.success === true){
    NotificationManager.success('Transaction saved and awaiting approval', 'Success', 1000)
    window.$('#TransactionApproval').modal('toggle');
    currentComponent.setState({ CashWithDrawal:{},Remarks:'',AmountTobePaid:'0.00'})
  }else{
    NotificationManager.error(response.data.message, 'Error', 1000)
  }
}).catch(function(error){
  NotificationManager.error('Error Ocurred', 'Error')
  console.log(error);
})

}

SaveTransaction = async e =>{
  let currentComponent = this;
  this.setState({ErrorMessage:'', ShowError:false, SaveTransExecuting: true});
  let remarks = this.state.Remarks;
  let branchCode = this.state.TellerDetails.BRANCH_CODE + "";
  branchCode = branchCode.padStart(4,'0') 
  let tellerCIF = this.state.TellerDetails.CIF_NO + "";
  tellerCIF = tellerCIF.padStart(8,'0') 
  let CIFTeller = this.state.CIFTeller + "";
  console.log(remarks);
  if(this.state.IsTellerValid === false){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({DisableButton:false,ShowError:true, ErrorMessage: 'Operating Teller does not have CIF number for this currency. Please contact the admin',VaultExecuting:false})
    return;
  }
  // if(CIFTeller.length < 10){
  //   window.$("html, body").animate({ scrollTop: 0 }, "slow");
  //   currentComponent.setState({ShowError:true, ErrorMessage: 'Account number cannot be less than 10 digit.',SaveTransExecuting:false})
  //   return;
  // }
  if(this.state.UserDetails.DebitAmount === null){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Sorry no transaction limit set for this operation',SaveTransExecuting:false})
    return;
  }
  if(this.state.UserDetails.TillStatus === null){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Sorry till is not open for this operation. Kindly contact administrator',SaveTransExecuting:false})
    return;
  }

  if(this.state.UserDetails.TillStatus !== 'OPEN'){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Sorry the till assigned to you is  not open',SaveTransExecuting:false})
    return;
  }


  if(this.state.TransactionParty === ''){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Please select transaction party',SaveTransExecuting:false})
    return;
   }

  let amountToBePaid = parseFloat(this.state.AmountTobePaid.replace(/,/g, ''));

let accountDetails = this.state.CashWithDrawal;
console.log(accountDetails);
let transactionDetailsModels = [];
this.state.Denominations.forEach(function(element){
  if(element.Amount > 0){
    let data = {Counter: element.Count, Amount: element.Amount}
    transactionDetailsModels.push(data);
  }
})
let tranType = 0;
if(this.state.ChequeNumber != ''){
  tranType = 1;
}
  //validate account status
  if(this.state.CashWithDrawal.AccountStatus != "A"){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'The account supplied is not active. Kindly review and try again.',SaveTransExecuting:false})
    return;
  }

 // validation 2: Count must be a non negative no
 if(amountToBePaid <= 0){

  window.$("html, body").animate({ scrollTop: 0 }, "slow");
  currentComponent.setState({ShowError:true, ErrorMessage: 'This is not a valid amount. Kindly review and try again.',SaveTransExecuting:false})
  return;
}
   this.state.Denominations.forEach(function(element){
      if(element.Count < 0){
        window.$("html, body").animate({ scrollTop: 0 }, "slow");
        currentComponent.setState({ShowError:true, ErrorMessage: 'Count must be a non negative number.',SaveTransExecuting:false})
        return;
      }
   })
  //Validation 3: Trans Amount must be valid to perform this transaction
  if(this.state.WithdrawalAmount != amountToBePaid){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'The withdrawal amount does not match the amount payable.',SaveTransExecuting:false})
    return;
  }
  //Validation 4: Trans Amount must not be greater than the  Available balance and available balance must be sufficient
  console.log(this.state.CashWithDrawal.Availablebalance);
  if(this.state.CashWithDrawal.Availablebalance < amountToBePaid){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Sorry, Your account balance is insufficient for this transaction. Kindly top up and proceed.',SaveTransExecuting:false})
    return;
  }

 //Validation 5: Transaction of the till must be within an allowable limit
 
  let transLimit = 0.00;
  if(this.state.UserDetails.DebitAmount !== null){
     transLimit = parseFloat(this.state.UserDetails.DebitAmount)
  }

    //validate memo and mandate
    if(!this.state.AcceptMandate){
      window.$("html, body").animate({ scrollTop: 0 }, "slow");
      currentComponent.setState({ShowError:true, ErrorMessage: 'This account does not have a valid mandate. Please kindly review and proceed.',SaveTransExecuting:false})
      return;
    }
    let data = {"TransRef":"","PhoneNo":"","TransName":this.state.UserDetails.UserName, "AccountNo": accountDetails.AccountNo,"Amount": amountToBePaid,
    "TransType": "","TellerId": this.state.UserDetails.Teller_ID,"CustomerAcctNos": accountDetails.AccountNo,
    "TotalAmt": amountToBePaid,"WithdrawerName": this.state.UserDetails.UserName,"WithdrawerPhoneNo": "","Status": 64,
    "CashierID": localStorage.getItem('Id'),"CashierTillNos": this.state.UserDetails.Teller_ID,
    "CashierTillGL": this.state.UserDetails.Teller_ID,"WhenApproved": currentComponent.ConvertDate(new Date()) ,
    "SortCode": "","Currency": this.state.CurrencyID,"ValueDate": currentComponent.ConvertDate(this.state.valueDate) ,
    "SupervisoryUser": "","Beneficiary": this.state.Beneficiary,"ChequeNo": this.state.ChequeNumber,
    "DateOnCheque": currentComponent.ConvertDate(this.state.startDate),"Remark": "Cash paid to " + this.state.CashWithDrawal.AccountName, "Narration": remarks,
    "CreationDate": currentComponent.ConvertDate(new Date()),"MachineName": "",
    "TillTransferID":0,"IsTillTransfer":false,"NeededApproval":false, "InitiatorName": this.state.UserDetails.UserName,
     "IsT24": false,"Branch": this.state.UserDetails.UserTillBranch, "CurrCode": this.state.CashWithDrawal.Abbrev, "ToTellerId":"",
     "GLAccountNo": "","CBACode": "","CBA": "IMAL", TransactionParty:this.state.TransactionParty, "DisapprovalReason":"",
     "DisapprovedBy":"","WhenDisapproved":"", "access_token":localStorage.getItem('access_token'),"CBACode": "",
     "CBA": "IMAL", "CurrencyAbbrev": this.state.CashWithDrawal.Abbrev,"CBAResponse":"test",
    "ApprovedBy": "",SMCIFNumber:tellerCIF, BranchCode: branchCode,"TransacterEmail": "",
    "IsReversed": false,"AccountName":this.state.CashWithDrawal.AccountName,
    "ReversedTranId": 0,TransactionDetailsModels:transactionDetailsModels}
    currentComponent.setState({APIdata: data});
    console.log(amountToBePaid);
    if(amountToBePaid > 250000){
      currentComponent.setState({SaveTransExecuting:false});
      window.$('#TransactionApproval').modal('toggle');
      return;
    }
    console.log(transLimit);
    if(amountToBePaid > transLimit){
      currentComponent.setState({SaveTransExecuting:false});
      window.$('#TransactionApproval').modal('toggle');
      return;
    }else{
      console.log(data);
      currentComponent.setState({ SaveTransExecuting: false})
      window.$('#tokenModal').modal('show');
      return;
    //   axios.post(apiUrl.BankService.CreateCashWithdrawal,data)
    //   .then(function(response){
    //     console.log(response.data);
    //     if(response.data.success === true){
    //       currentComponent.setState({ CashWithDrawal:{},Remarks:'',AmountTobePaid:'0.00', TransRef: response.data.TransactionRef,SaveTransExecuting: false})
    //       window.$('#TransactionSuccessModal').modal('toggle')
    //      // NotificationManager.success('Transaction succssful', 'Success')
    //     }else{
    //       currentComponent.setState({ SaveTransExecuting: false})
    //       NotificationManager.error(response.data.message, 'Error',2000)
    //     }
    //   }).catch(function(error){
    //     currentComponent.setState({ SaveTransExecuting: false})
    //     NotificationManager.error('Error Ocurred', 'Error',2000)
    //     console.log(error);
    //   })
     }
 
}


ValidateOTP = e =>{
  let currentComponent = this;
  currentComponent.setState({OTPExecuting: true, DisableButton: true})
      let data = currentComponent.state.APIdata;
      data.ApprovedBy = localStorage.getItem('Id');
      console.log(data);
      axios.post(apiUrl.BankService.CreateCashWithdrawal,data)
      .then(function(response){
        console.log(response.data);
        if(response.data.success === true){
          currentComponent.setState({OTPExecuting: false, DisableButton: false})
          currentComponent.setState({ CashWithDrawal:{},Remarks:'',AmountTobePaid:'0.00',WithdrawalAmount:0.00,Beneficiary:'',Remarks:'',   TransRef: response.data.TransactionRef,SaveTransExecuting: false})
          window.$('#tokenModal').modal('hide')
          window.$('#TransactionSuccessModal').modal('toggle')
         // NotificationManager.success('Transaction succssful', 'Success')
        }else{
          currentComponent.setState({ SaveTransExecuting: false})
          currentComponent.setState({OTPExecuting: false, DisableButton: false,ErrorMessage:response.data.message})
          window.$('#tokenModal').modal('hide')
          window.$('#errorModal').modal('toggle');
        }
      }).catch(function(error){
        currentComponent.setState({OTPExecuting: false, DisableButton: false})
        currentComponent.setState({ SaveTransExecuting: false, ErrorMessage:'Server Error'})
        window.$('#tokenModal').modal('hide')
        window.$('#errorModal').modal('toggle');
        console.log(error);
      })
 
}

ClickViewMandate = async e => {
  let currentComponent = this;
  currentComponent.setState({ShowError:false, ErrorMessage: '',IsLoading:true})
  if(Object.keys(this.state.CashWithDrawal).length === 0){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Please input a valid account number'})
    window.$('#mandate').modal('hide')
    return;
  }
  let data = {Number: this.state.CashWithDrawal.customerId, access_token: localStorage.getItem('access_token')}
  await axios.post(apiUrl.BankService.GetMandate, data )
  .then(function(response){
    currentComponent.setState({IsLoading:false})
         console.log(response.data);
         if(response.data.success){
          currentComponent.setState({Mandate: response.data.Mandate})
         }else{
          currentComponent.setState({IsLoading:false})
          window.$("html, body").animate({ scrollTop: 0 }, "slow");
          currentComponent.setState({ShowError:true, ErrorMessage: response.data.message})
          window.$('#mandate').modal('hide')
         }
  }).catch(function(error){
    currentComponent.setState({IsLoading:false})
    console.log(error)
    NotificationManager.error('Error', 'Server Error',2000)
  })
}



GetDate(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();

  today = dd  + '/' + mm + '/' + yyyy;
  console.log(today);
  return today
}

ConvertDate(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();
  today = yyyy + '-' + mm + '-' + dd;
  console.log(today);
  return today
}

GetDateForAPI(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();
   var hours = today.getHours();
  var min = today.getMinutes();
  var sec = today.getSeconds();
  today = yyyy + '-' + mm + '-' + dd + 'T' + hours + ':' + min + ':' + sec + '+01:00';
  console.log(today);
  return today
}

RejectMandate = e =>{
  NotificationManager.success('Mandate is rejected successfully', 'success')
  window.$('#mandate').modal('toggle')
  this.setState({AcceptMandate: false})
}

AcceptMandate = e =>{
  NotificationManager.success('Mandate is accepted successfully', 'success', 2000)
  window.$('#mandate').modal('toggle')
  this.setState({AcceptMandate: true})
}

AcceptMemo = e =>{
   this.setState({AcceptMemo: true});
   window.$('#memo').modal('toggle')
}

RejectMemo = e =>{
  this.setState({AcceptMemo: false});
  window.$('#memo').modal('toggle')
}

  async componentDidMount(){
    loadAmountFormat();
   // window.$('#TransactionSuccessModal').modal('toggle');
   await GenerateToken();
    let currentComponent = this;
    await axios.get(apiUrl.BankService.GetUserDetails + 'userId=' + localStorage.getItem("Id") +  '&access_token=' + localStorage.getItem('access_token'))
    .then(function(response){
      console.log(response.data);
      if(response.data.success){
        currentComponent.setState({UserDetails: response.data.UserDetails})
      }
      
    }).catch(function(error){
      console.log(error);
    })

  }
  render(){
    return (
       <div>
       <NotificationContainer/>
      <section id="main-content">
      <section className="wrapper">
        <div className="row">
        <div className="col-md-12">
        <h3 className="page-title">
        Cash Withdrawal <small></small>
        </h3>
        </div>
        </div>
        <div className="row">
        <div className="col-md-6"  style={{width:'50%'}}> <b style={{fontSize:'15px'}}>Transaction Party: </b><select onClick={this.ChangeTransactionParty}>
        <option value="0" selected disabled>---Select account type---</option>
        <option value="M">SELF</option>
        <option value="E">THIRD-PARTY</option>
        </select><br/><br/>
        </div>
        <div style={{paddingLeft:'0px', textAlign:'right'}} className="col-md-6">
        <button data-toggle="modal" data-target="#mandate" onClick={this.ClickViewMandate} type="button" className="btn btn-primary"><i className="fa fa-eye"></i> View Mandate</button>
        </div>
        </div><br/> <br/>
        {
          this.state.ShowError === true ? <div  class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
              <b>Close</b></button>
          <span class="glyphicon glyphicon-hand-right"></span> <strong>{this.state.ErrorMessage}</strong>  </div>: ''
        }
        <div className="row">
				<div className="col-md-12">
					<div className="portlet box grey-cascade">
            <div className="portlet-title">
            <div className="caption">
            <i className="fa fa-info"></i> Account Details
          </div>
          <div className="tools">
            <a href="javascript:;" className="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" className="config">
            </a>
            <a href="javascript:;" className="reload">
            </a>
            <a href="javascript:;" className="remove">
            </a>
          </div>
            </div>
            <div className="portlet-body" style={{height:'325px'}}>
              <div className="table-toolbar">
              <div style={{overflow:'scroll', height:'300px'}}>
								<div className="row">
									<div className="col-md-6">
                  <div className="portlet-body form">
                  <div className="form-body">
                  <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                    <input defaultValue={this.state.AccountNumber} onChange={this.onChangeOfAccountNumber} type="number" className="form-control edited"/>
                    <label for="form_control_1"><b>Customer A/C</b></label>
                    <span style={{fontSize:'12px', color:this.state.AccountFieldColor}} className="help-block">{this.state.AccountFieldMessage}</span>
                    <i className="fa fa-key"></i>
                  </div>
                </div>
            <div className="form-group form-md-line-input has-success form-md-floating-label">
            <div className="input-icon right">
            <input value={this.state.CashWithDrawal.AccountStatus === "A" ?  "ACTIVE" : "INACTIVE"} type="text" className="form-control label-success"  readOnly={true} disabled={true}/>
              <label for="form_control_1"><b>Account Status</b></label>
            </div>
          </div>
          <div className="form-group form-md-line-input has-success form-md-floating-label">
          <div className="input-icon right">
            <input defaultValue={this.state.CashWithDrawal.AccountType} type="text" className="form-control edited" readOnly={true} disabled={true}/>
            <label for="form_control_1"><b>Product Type</b></label>
            <i className="fa fa-bank"></i>
          </div>
        </div>
          <div className="form-group form-md-line-input has-success form-md-floating-label">
          <div className="input-icon right">
            <input defaultValue={this.state.CashWithDrawal.BranchCode} type="text" className="form-control edited" readOnly={true} disabled={true}/>
            <label for="form_control_1"><b>Branch</b></label>
            <i className="fa fa-bank"></i>
          </div>
        </div>
                  </div>
                  </div>
                  </div>

                  <div className="col-md-6">
                  <div className="portlet-body form">
                  <div className="form-body">
                  <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                    <input defaultValue={this.state.CashWithDrawal.AccountName} type="text" className="form-control edited" readOnly={true} disabled={true}/>
                    <label for="form_control_1"><b>Customer Name</b></label>
                    <i className="fa fa-user"></i>
                  </div>
                </div>
            <div className="form-group form-md-line-input has-success form-md-floating-label">
            <div className="input-icon right">
              <input defaultValue={this.state.CashWithDrawal.Currency} type="text" className="form-control edited" readOnly={true} disabled={true}/>
              <label for="form_control_1"><b>Currency</b></label>
              <i className="fa fa-money"></i>
            </div>
          </div>

                <div className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                  <input style={{textAlign:'right'}} defaultValue={this.state.CashWithDrawal.Availablebalance > 0 || this.state.CashWithDrawal.Availablebalance < 0  ? this.state.CashWithDrawal.Availablebalance.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : ''} type="text" className="form-control edited" readOnly={true} disabled={true}/>
                  <label for="form_control_1"><b>Effective Balance</b></label>
                  <i className="fa fa-money"></i>
                </div>
              </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
            </div>
            </div>
            <div className="col-md-6">
            <div className="portlet box grey-cascade">
              <div className="portlet-title">
              <div className="caption">
              <i className="fa fa-info"></i> Withdrawal Details
            </div>
            <div className="tools">
              <a href="javascript:;" className="collapse">
              </a>
              <a href="#portlet-config" data-toggle="modal" className="config">
              </a>
              <a href="javascript:;" className="reload">
              </a>
              <a href="javascript:;" className="remove">
              </a>
            </div>
              </div>
              <div className="portlet-body">
                <div className="table-toolbar">
                <div style={{overflow:'scroll', height:'290px'}}>
                {
                  this.state.ShowNairaEquivalentButton === true ? <a data-toggle="tooltip"dataPlacement="bottom"  title="Check naira equivalent" style={{float:'right'}} onClick={this.CheckNairaEquivalent} type="button" className="btn btn-primary"><i class="fa fa-money"></i> Naira Equivalent {this.state.CheckNairaEquivalentExecuting === true ? <i id="spinner" class="fa fa-spinner fa-spin"></i> : ''}</a> : ''
                }<br/>
                  <div className="row">
                    <div className="col-md-6">
                    <div className="portlet-body form">
                    <div className="form-body">
                    <div className="form-group form-md-line-input has-success form-md-floating-label">
                    <div className="input-icon right">
                      <input disabled readOnly value={this.state.TellerDetails.BRANCH_CODE} type="number" name="BranchCode" className="form-control edited"/>
                      <label for="form_control_1"><b>Branch Code</b></label>
                      <span style={{fontSize:'12px'}} className="help-block">Enter your branch code</span>
                      <i className="fa fa-key"></i>
                    </div>
                  </div>
                  <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                    <input value={this.state.Beneficiary} disabled={this.state.DisableBeneFiciary} readOnly={this.state.DisableBeneFiciary} onChange={this.onChangeOfBeneficiary} type="text" className="form-control edited"/>
                    <label for="form_control_1"><b>Beneficiary</b></label>
                    <span style={{fontSize:'12px'}} className="help-block">Enter the beneficiary name of the cash withdrawal</span>
                    <i className="fa fa-gift"></i>
                  </div>
                </div>
                <div className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                  <textarea disabled readOnly value={Object.keys(this.state.CashWithDrawal).length === 0  ? "" :this.state.ChequeNumber + "- Cash paid to " + this.state.CashWithDrawal.AccountName} type="text" className="form-control edited"/>
                  <label for="form_control_1"><b>Remarks</b></label>
                  <i className="fa fa-file-text-o"></i>
                </div>
              </div>
           
                </div>
                </div>
                    </div>

                    <div className="col-md-6">
                    <div className="portlet-body form">
                    <div className="form-body">
                    <div className="form-group form-md-line-input has-success form-md-floating-label">
                    <div className="input-icon right">
                      <input readOnly disabled  onChange={this.ChangeHandler} value={this.state.TellerDetails.CIF_NO} type="number" name="CIFTeller" className="form-control edited"/>
                      <label for="form_control_1"><b>CIF Number</b></label>
                      <span style={{fontSize:'12px'}} className="help-block">Enter your account number</span>
                      <i className="fa fa-key"></i>
                    </div>
                  </div>
              <div className="form-group form-md-line-input has-success form-md-floating-label">
              <div className="input-icon right">
                <input data-type="currency" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" onChange={this.ChangeWithdrawalAmount} defaultValue={this.state.AmountTobePaid} style={{textAlign:'right'}}  type="text" className="form-control edited" />
                <label for="form_control_1"><b>Withdrawal Amount</b></label>
                <span style={{fontSize:'12px'}} className="help-block">Enter the withdrawal Amount</span>
                <i className="fa fa-money"></i>
              </div>
            </div>
            <div style={{paddingTop:'1px'}} className="form-group form-md-line-input has-success form-md-floating-label">
              <div className="input-icon right">
              <label for="form_control_1"><b style={{color:'#3c763d'}}>Value Date</b></label><br/>
              <DatePicker
              dateFormat="dd/MM/yyyy"
              selected={this.state.valueDate}
              onChange={date => this.setValueDate(date)}
              customInput={<ExampleCustomInput/>}
            />
              </div>
            </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
              </div>
              </div>
              <div className="col-md-6">
              <div className="portlet box grey-cascade">
                <div className="portlet-title">
                <div className="caption">
                <i className="fa fa-file"></i> Cash Analysis
              </div>
              <div className="tools">
                <a href="javascript:;" className="collapse">
                </a>
                <a href="#portlet-config" data-toggle="modal" className="config">
                </a>
                <a href="javascript:;" className="reload">
                </a>
                <a href="javascript:;" className="remove">
                </a>
              </div>
                </div>
                <div className="portlet-body">
                  <div className="table-toolbar">
                  <div style={{overflow:'scroll', height:'290px'}}>
                  <div className="row">
                      <div className="col-md-12">
                      <div className="portlet-body form">
                      <div className="form-body">
                      <table className="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th style={{width:'240px'}}>
            Denominations
            </th>
             <th style={{width:'120px'}}>
            Count
            </th>
            <th>
            Amount
            </th>
          </tr>
          </thead>
          <tbody>
          {
            Object.keys(this.state.CashWithDrawal).length === 0  ? '' : this.state.Denominations.sort((a, b) => parseFloat(b.Value) - parseFloat(a.Value)).map(y=>
              <tr key={y.ID}>
                  <td><input defaultValue={y.Name} readOnly={true} disabled={true}
                  type="text" className="form-control" name="Balance" /></td>
                  <td><input id={y.ID} onChange={this.onChangeCount} autoComplete="off"
                   type="number" className="form-control" name="Balance" /></td>
                   <td><input readOnly disabled value={y.Amount > 0 ? y.Amount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","): '0.00'}  style={{textAlign:'right'}} autoComplete="off"
              type="text" className="form-control" name="Balance" />
              </td>
              </tr>
              )
          }
          </tbody>
          </table>
          <div style={{textAlign:'right'}}>   <span style={{paddingLeft:'0px', fontSize:'15px'}}><b>Withdrawal Amount: </b><input style={{textAlign:'right',width:'200px'}} disabled={true} readOnly={true} value={this.state.WithdrawalAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}/></span></div>

                      </div>
                      </div>
                      </div>
                      </div>
                      </div>
                      </div>
                      </div>
                </div>
                </div><br/>

            </div>
            <div style={{paddingLeft:'0px', textAlign:'right'}}><button onClick={this.ClearState} type="button" className="btn btn-default" data-dismiss="modal">Close</button><button onClick={this.SaveTransaction} type="button" className="btn btn-primary"><i class="fa fa-print"></i> Save  {this.state.SaveTransExecuting === true ? <i id="spinner" class="fa fa-spinner fa-spin"></i> : ''}</button></div>
        </section>
        </section>


        <div className="modal fade" id="mandate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content" style={{width:'700px'}}>
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 className="modal-title" id="myModalLabel">Mandate</h4>
            </div>
            <div className="modal-body">
            <div className="row">
            <div className="col-md-12">
            <div className="portlet-body form">
              <div className="form-body">
              <div style={{overflow:'scroll'}} className="form-group form-md-line-input has-success form-md-floating-label">
                {
                  this.state.IsLoading ?
                  <div> <i className="fa fa-spinner fa-spin"></i> <b>Loading.......</b> </div> :
                  <table className="table table-striped table-bordered table-hover">
                  <thead>
                  <th>Customer Name</th>
                  <th>Mandate Picture</th>
                  <th>Status</th>
                  </thead>
                  <tbody>
                  {
                   
                      <tr>
                      <td>{ this.state.Mandate.signatureDescription}</td>
                      <td> <img style={{width:'250px',height:'200px'}} src= {"data:image/*;base64," +  this.state.Mandate.signatureFile} /></td>
                      <td>{ this.state.Mandate.status === "Approved" ? <i style={{color:'green', fontWeight:'bold'}} className="fa fa-check-circle"> Approved</i>  : <i style={{color:'red', fontWeight:'bold'}} className="fa fa-thumbs-down"> Disapproved</i>} </td>
                      </tr>
                  }
                  </tbody>
                  </table>
                }
             
            </div>

              </div>
              </div>
            </div>
            </div>
            </div>
            <div className="modal-footer">
            <button onClick={this.RejectMandate} type="button" className="btn btn-danger"><i className="fa fa-thumbs-down"></i> Reject</button>
              <button onClick={this.AcceptMandate} type="button" className="btn btn-primary"><i className="fa fa-thumbs-up"></i> Accept</button>
            </div>
          </div>
        </div>
      </div>


    <div className="modal fade" id="TransactionApproval" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div className="modal-dialog">
      <div className="modal-content" style={{width:'500px'}}>
        <div style={{background:'#b48913'}} className="modal-header">
          <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         <h4 className="modal-title" id="myModalLabel">Warning</h4>
        </div>
        <div className="modal-body">
        <div className="row">
        <div className="col-md-12 ">
        <div className="portlet-body form">
          <div className="form-body">
          <p><b>The amount requested has exceeded the maximum limit. Please click proceed to send for approval or cancel to abort transaction.</b></p>
          </div>
          </div>
        </div>

        </div>
        </div>
        <div className="modal-footer">
        <button type="button" className="btn btn-default" data-dismiss="modal">Cancel</button>
          <button onClick={this.ProceedTransaction} type="button" className="btn btn-primary"><i className="fa fa-thumbs-up"></i> Proceed</button>
        </div>
      </div>
    </div>
  </div>


  <div className="modal fade" id="TransactionSuccessModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div className="modal-dialog">
    <div className="modal-content" style={{width:'520px'}}>
      <div style={{background:'rgb(22 180 27)'}} className="modal-header">
        <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
       <h4 className="modal-title" id="myModalLabel">Transaction Successful</h4>
      </div>
      <div className="modal-body">
      <div className="row">
      <div className="col-md-12 ">
      <div className="portlet-body form">
        <div className="form-body">
        <p><b>Transaction successful with transaction reference: {this.state.TransRef} </b></p>
        </div>
        </div>
      </div>

      </div>
      </div>
      <div className="modal-footer">
      <button type="button" className="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

<div className="modal fade" id="NairaEquivalentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div className="modal-dialog">
  <div className="modal-content" style={{width:'520px'}}>
    <div style={{background:'#a00'}} className="modal-header">
      <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
     <h4 className="modal-title" id="myModalLabel">Naira Equivalent</h4>
    </div>
    <div className="modal-body">
    <div className="row">
    <div className="col-md-6 ">
    <div className="portlet-body form">
      <div className="form-body">
      <div className="form-group form-md-line-input has-success form-md-floating-label">
      <div className="input-icon right">
      <input className="form-control edit" type={"text"} readOnly disabled value={this.state.CurrencyRateDetails.CCY_SELL_RATE}/>
        <label for="form_control_1"><b>Rate</b></label>
        <i className="fa fa-money"></i>
      </div>
    </div>
      <div className="form-group form-md-line-input has-success form-md-floating-label">
      <div className="input-icon right">
      <input className="form-control edite" type={"text"} readOnly disabled value={this.state.AmountTobePaid }/>
        <label for="form_control_1"><b>Amount</b></label>
        <i className="fa fa-money"></i>
      </div>
    </div>
      </div>
      </div>
    </div>

    <div className="col-md-6 ">
    <div className="portlet-body form">
      <div className="form-body">
      <div className="form-group form-md-line-input has-success form-md-floating-label">
      <div className="input-icon right">
      <input className="form-control edite" type={"text"} readOnly disabled value={this.state.CurrencyRateDetails.CCY_CODE}/>
        <label for="form_control_1"><b>Currency Code</b></label>
        <i className="fa fa-money"></i>
      </div>
    </div>
      <div className="form-group form-md-line-input has-success form-md-floating-label">
      <div className="input-icon right">
      <input className="form-control edite" type={"text"} readOnly disabled value={this.state.ConvertedAmount > 0 ? this.state.ConvertedAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '0.00'}/>
        <label for="form_control_1"><b>Converted Amount</b></label>
        <i className="fa fa-money"></i>
      </div>
    </div>
      </div>
      </div>
    </div>
    </div>
    </div>
    <div className="modal-footer">
    <button type="button" className="btn btn-primary" data-dismiss="modal">OK</button>
    </div>
  </div>
</div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" id="tokenModal" role="dialog" tabindex="-1"  class="modal fade">
<div class="modal-dialog" style={{width:'350px'}}>
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4  style={{color:'white'}} className="modal-title" id="myModalLabel">Confirmation</h4>
    </div>
    <div class="modal-body">
      <p>Are you sure you want make with this transaction?.</p>
    </div>
    <div class="modal-footer">
      <button  data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
      <button disabled={this.state.DisableButton} onClick={this.ValidateOTP} class="btn btn-theme" type="button">Yes {this.state.OTPExecuting === true ? <i id="spinner" class="fa fa-spinner fa-spin"></i> : ''}</button>
    </div>
  </div>
</div>
</div>


<div aria-hidden="true" aria-labelledby="myModalLabel" id="errorModal" role="dialog" tabindex="-1"  class="modal fade">
<div class="modal-dialog" style={{width:'400px'}}>
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4  style={{color:'white'}} className="modal-title" id="myModalLabel">Error</h4>
    </div>
    <div class="modal-body">
      <p><i className="fa fa-exclamation-triangle"></i>   {this.state.ErrorMessage}</p>
    </div>
    <div class="modal-footer">
      <button  data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
    </div>
  </div>
</div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" id="errorModal" role="dialog" tabindex="-1"  class="modal fade">
<div class="modal-dialog" style={{width:'400px'}}>
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4  style={{color:'white'}} className="modal-title" id="myModalLabel">Error</h4>
    </div>
    <div class="modal-body">
      <p><i className="fa fa-exclamation-triangle"></i>   {this.state.ErrorMessage}</p>
    </div>
    <div class="modal-footer">
      <button  data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
    </div>
  </div>
</div>
</div>
      </div>
    )
  }
}

export default CashWithdrawal