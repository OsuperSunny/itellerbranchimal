import React, {Component} from "react";
import MockJson from '../apiService/mockJson';
import axios from 'axios';
import apiUrl from '../apiService/config'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import GenerateToken from '../shared/token'
import $ from 'jquery';
import Datatable from "../shared/dataTable";
import ReactExport from 'react-data-export';
import sterlingLogo from '../sterling.png'
import { jsPDF } from "jspdf";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const userName = localStorage.getItem('Id')
const ExampleCustomInput = ({ value, onClick }) => (
  <input className="form-control label-success" onClick={onClick} value={value} style={{width:'150px'}}/>
);

// var generateData = function(amount) {
//   var result = [];
//   var data = {
//     coin: "100",
//     game_group: "GameGroup",
//     game_name: "XPTO2",
//     game_version: "25",
//     machine: "20485861",
//     vlt: "0"
//   };
//   for (var i = 0; i < amount; i += 1) {
//     data.id = (i + 1).toString();
//     result.push(Object.assign({}, data));
//   }
//   return result;
// };

function createHeaders(keys) {
  var result = [];
  for (var i = 0; i < keys.length; i += 1) {
    result.push({
      id: keys[i],
      name: keys[i],
      prompt: keys[i],
      width: 65,
      align: "center",
      padding: 0
    });
  }
  return result;
}



// var doc = new jsPDF({ putOnlyUsedFonts: true, orientation: "landscape" });
// doc.table(1, 1, generateData(100), headers, { autoSize: true });



function loadAmountFormat(){
  $("input[data-type='currency']").on({
    keyup: function() {
      console.log("exec")
      formatCurrency($(this));
    },
    blur: function() { 
      console.log("exec")
      formatCurrency($(this), "blur");
    }
});

function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val =  left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val =  input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ".00";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}  
}

class AuditTrail extends Component {
  constructor (props){
    super(props)
    this.state = {
    WithdrawalAmount:0.00,
    AmountTobePaid:'0.00',
    startDate: new Date(),
    endDate: new Date(),
    AuditTrail:[],
    TellerFieldMessage: 'Filter by teller name',
    TellerFieldColor: 'green',
    OriginalAuditTrail:[],
    IsVaultIn: false,
    IsVaultOut: false,
    selectedOption:'',
    multiDataSet:[],
    vaultPDFdetails:[],
    CashDenominations:[],
    Abbrev:''
}
}

onChangeOfTellerID = e =>{
  let auditTrail = this.state.AuditTrail;
  this.setState({TellerFieldColor: '', TellerFieldMessage:''})
  auditTrail = auditTrail.filter(function(data){
          return data.UserId === e.target.value;
  })
  if(auditTrail.length <= 0){
    this.setState({TellerFieldColor: 'red', TellerFieldMessage:'No record found for teller'})
  }else{
    this.setState({AuditTrail: auditTrail,TellerFieldColor: '', TellerFieldMessage:'' });
  }
}


GenerateFileData(transaction){
  let vaultDetails = [];
  let vaultPDFdetails = [];
  let currentComponent = this;
  transaction.forEach(function(element){
    let data = [];
    let vauldata = {DateTreated: currentComponent.ConvertDateTime(element.CreationDate),TransactionReference:element.TransRef, tellerId: element.TellerId,tellerName: element.TellerName,toTellerId: element.ToTellerId, currency: element.Currency,
    transactionType: element.IsVaultIn === true ? "VaultIn" : "VaultOut",
    amount: element.TotalAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
    let dateTreated = {value: currentComponent.ConvertDateTime(element.CreationDate), style: {font: {sz: "18"}}};
    let transRef = {value: element.TransRef, style: {font: {sz: "18"}}};
    let tellerId = {value: element.TellerId, style: {font: {sz: "18"}}};
    let TellerName = {value: element.TellerName, style: {font: {sz: "18"}}};
    let branch = {value: element.Branch, style: {font: {sz: "18"}}};
    let toTellerId = {value: element.ToTellerId, style: {font: {sz: "18"}}};
    let currency = {value: element.Currency, style: {font: {sz: "18"}}};
    let transactionType = {value: element.IsVaultIn === true ? "VaultIn" : "VaultOut", style: {font: {sz: "18"}}};
    let amount = {value: element.TotalAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
     style: {font: {sz: "18"},alignment:{horizontal:'right'}}};
     data.push(dateTreated)
     data.push(transRef)
    data.push(tellerId);
    data.push(TellerName);
    data.push(branch);
    data.push(toTellerId);
    data.push(currency);
    data.push(transactionType);
    data.push(amount);
    vaultDetails.push(data);
    vaultPDFdetails.push(vauldata);
  });
  this.setState({vaultPDFdetails});
 // this.ImportToExcel(vaultDetails);
}

ClearState = e => {
  this.setState({ CashWithDrawal:{}})
}

async setStartDate(date){
  console.log(date);
  let currentComponent = this;
 this.setState({ startDate: date})
 let startDate = currentComponent.ConvertDate(date);
  let endDate = currentComponent.ConvertDate(currentComponent.state.endDate);
 await axios.get(apiUrl.Report.AuditTrail + 'From/' + startDate + '/To/' + endDate)
  .then(function(response){
  console.log(response.data)
  let AuditTrail = response.data.Audit.sort((a, b) => b.DateTime - a.DateTime)
  currentComponent.setState({AuditTrail, OriginalAuditTrail:AuditTrail})
  })
}

async setEndDate(date){
  let currentComponent = this;
  console.log(date);
  let startDate = currentComponent.ConvertDate(currentComponent.state.startDate);
  let endDate = currentComponent.ConvertDate(date);
  await axios.get(apiUrl.Report.Transaction + 'From/' + startDate + '/To/' + endDate)
  .then(function(response){
  console.log(response.data)
  let AuditTrail = response.data.Audit.sort((a, b) => b.DateTime - a.DateTime)
  currentComponent.setState({AuditTrail, OriginalAuditTrail:AuditTrail})
  })
window.$('#sample_1').DataTable( {
  "language": {
    "zeroRecords": " " //Change your default empty table message
    },
  retrieve: true,
  paging: true
});
 this.setState({ endDate: date})
}

 async ImportToExcel(VaultDetails){
//      const multiDataSet = [
//       {
//         columns: [
//             {title: "", width: {wpx: 80}},//pixels width 
//   {title: "", width: {wpx: 90}},
//   {title: "Sterling Bank", width: {wch: 45}, 
//   style: {font: {sz: "24", bold: true}, alignment:{horizontal:'center'}}},//char width 
//         ],
//         data: [
//         ]
// },
//          { ySteps: 2,
//      columns: [
//       {title: "Date Treated", style: {font: {sz: "15", bold: true}}},
//       {title: "Transaction Reference", style: {font: {sz: "15", bold: true}}},
//                  {title: "Teller ID", style: {font: {sz: "15", bold: true}}},//pixels width 
//        {title: " Teller Name", width:  {wch: 30}, 
//        style: {font: {sz: "15", bold: true}}},//char width 
//        {title: "Teller Branch",  width: {wch: 45},style: {font: {sz: "15", bold: true}}},
//        {title: " Vault Teller ID", width:  {wch: 30},style: {font: {sz: "15", bold: true}}},
//        {title: "Currency", width: {wch: 30},style: {alignment:{horizontal:'center'},font: {sz: "15", bold: true}}},
//        {title: "Transaction Type", width: {wch: 40},style: {font: {sz: "15", bold: true}}},
//        {title: "Amount", width: {wch: 40},style: {font: {sz: "15", bold: true}}}
//      ],
//      data: VaultDetails
//    },
//    {  ySteps: 2,
//      columns: [
//                  {title: "Authorized Signature", style: {font: {sz: "15", bold: true}}},//pixels width 
//        {title: "",width:  {wch: 30},
//        style: {font: {sz: "15", bold: true}}},//char width 
//        {title: "", width:  {wch: 45},style: {font: {sz: "15", bold: true}}},
//        {title: "", width:  {wch: 30},style: {font: {sz: "15", bold: true}}},
//        {title: "", width: {wch: 30},style: {alignment:{horizontal:'center'},font: {sz: "15", bold: true}}},
//        {title: "", width: {wch: 40},style: {font: {sz: "15", bold: true}}},
//      ],
//      data: [
//                  [
//                      {value: "", style: {font: {sz: "18"}}},
//          {value: "", style: {font: {sz: "18"}, alignment:{horizontal:'center'}}},
//          {value: ""}
//                  ]]
//    }
//  ];
//  // const columns =  [
//  // 	{title: "", width: {wpx: 80}},//pixels width 
//  // 	{title: "Precise Financial Systems", width: {wch: 70}, 
//  // 	style: {font: {sz: "24", bold: true}, alignment:{horizontal:'center'}}},//char width 
//  // 	{title: "", width: {wpx: 90}},
//  // ]
//      this.setState({multiDataSet:multiDataSet})
 }

Excel(){
  const multiDataSet = [
    {
        columns: ["Name", "Salary", "Sex"],
        data: [
            ["Johnson", 30000, "Male"],
            ["Monika", 355000, "Female"],
            ["Konstantina", 20000, "Female"],
            ["John", 250000, "Male"],
            ["Josef", 450500, "Male"],
        ]
    },
    {
        xSteps: 1, // Will start putting cell with 1 empty cell on left most
        ySteps: 5, //will put space of 5 rows,
        columns: ["Name", "Department"],
        data: [
            ["Johnson", "Finance"],
            ["Monika", "IT"],
            ["Konstantina", "IT Billing"],
            ["John", "HR"],
            ["Josef", "Testing"],
        ]
    }
];
this.setState({multiDataSet:multiDataSet})
}


GetDate(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();

  today = dd  + '/' + mm + '/' + yyyy;
  return today
}

ConvertDate(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();
  today = yyyy + '-' + mm + '-' + dd;
  return today
}

ConvertDateTime(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();
  var hours = today.getHours();
  var min = today.getMinutes();
  if(min === 0){
    min = "00"
  }
  let minute = "" + min;
  if(minute.length  === 1){
    min = "0" + min
  }
  today = dd + '/' + mm + '/' + yyyy + " " +  hours + ':' + min;
  return today
}

GetDateForAPI(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();
   var hours = today.getHours();
  var min = today.getMinutes();
  var sec = today.getSeconds();
  today = yyyy + '-' + mm + '-' + dd + 'T' + hours + ':' + min + ':' + sec + '+01:00';
  return today
}

GeneratePDF = e =>{
let currentComponent = this;
console.log(currentComponent.state.vaultPDFdetails);
  const headers = createHeaders([
    "DateTreated",
    "TransactionReference",
    "tellerId",
    "tellerName",
    "toTellerId",
    "currency",
    "transactionType",
    "amount",
  ]);
var doc = new jsPDF({ putOnlyUsedFonts: true, orientation: "landscape" });
doc.addImage(sterlingLogo, "PNG", 130, 10, 20, 20);
doc.text("Sterling Bank", 130,40);
doc.table(1, 60, currentComponent.state.vaultPDFdetails, headers, { autoSize: false,fontSize:8 });
doc.save("sterling-bank-vault-transaction-report.pdf");
}

  async componentDidMount(){
    loadAmountFormat();
    this.Excel();
    let currentComponent = this;
    let startDate = currentComponent.ConvertDate(currentComponent.state.startDate);
    let endDate = currentComponent.ConvertDate(currentComponent.state.endDate);
    await axios.get(apiUrl.Report.AuditTrail + 'From/' + startDate + '/To/' + endDate)
    .then(function(response){
    console.log(response.data)
    // let transaction = response.data.VaultTransaction.sort((a, b) => b.CreationDate - a.CreationDate)
    let auditTrail = response.data.Audit.sort((a, b) => b.DateTime - a.DateTime)
    currentComponent.setState({AuditTrail: auditTrail, OriginalAuditTrail: auditTrail})
    //currentComponent.GenerateFileData(auditTrail);
    })
    window.$('#sample_1').DataTable( {
      "language": {
        "zeroRecords": " " //Change your default empty table message
        },
      retrieve: true,
      paging: true
    });
  }
  render(){
    return (
       <div>
       <NotificationContainer/>
      <section id="main-content">
      <section className="wrapper">
        <div className="row">
        <div className="col-md-12">
        <h3 className="page-title">
        Audit Trail<small></small>
        </h3>
        </div>
        </div>
        {
          this.state.ShowError === true ? <div  class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
              <b>Close</b></button>
          <span class="glyphicon glyphicon-hand-right"></span> <strong>{this.state.ErrorMessage}</strong>  </div>: ''
        }
        <div className="row">
				<div className="col-md-12">
					<div className="portlet box grey-cascade">
            <div className="portlet-title">
            <div className="caption">
            <i className="fa fa-info"></i> 
          </div>
          <div className="tools">
            <a href="javascript:;" className="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" className="config">
            </a>
            <a href="javascript:;" className="reload">
            </a>
            <a href="javascript:;" className="remove">
            </a>
          </div>
            </div>
            <div className="portlet-body" style={{height:'225px'}}>
              <div className="table-toolbar">
              <div style={{overflow:'scroll', height:'200px'}}>
								<div className="row">
									<div className="col-md-6">
                  <div className="portlet-body form">
                  <div className="form-body">
                  <div style={{paddingTop:'1px'}} className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                  <label for="form_control_1"><b style={{color:'#3c763d'}}>Date From</b></label>
                  <DatePicker
                  dateFormat="dd/MM/yyyy"
                  selected={this.state.startDate}
                  onChange={date => this.setStartDate(date)}
                  customInput={<ExampleCustomInput/>}
                />
                  </div>
                </div>
                  <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                    <input  onChange={this.onChangeOfTellerID} type="text" className="form-control edited"/>
                    <label for="form_control_1"><b>Teller Name</b></label>
                    <span style={{fontSize:'12px', color:this.state.TellerFieldColor}} className="help-block">{this.state.TellerFieldMessage}</span>
                    <i className="fa fa-user"></i>
                  </div>
                </div>
                  </div>
                  </div>
                  </div>

                  <div className="col-md-6">
                  <div className="portlet-body form">
                  <div className="form-body">
                  <div style={{paddingTop:'1px', marginBottom:'30px'}} className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                  <label for="form_control_1"><b style={{color:'#3c763d'}}>Date To</b></label>
                  <DatePicker
                  dateFormat="dd/MM/yyyy"
                  selected={this.state.endDate}
                  onChange={date => this.setEndDate(date)}
                  customInput={<ExampleCustomInput/>}
                />
                  </div>
                </div>

                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
            </div>
            </div>
            <div className="col-md-12">
            <div className="portlet box grey-cascade">
              <div className="portlet-title">
              <div className="caption">
              <i className="fa fa-info"></i> Details
            </div>
            <div className="tools">
              <a href="javascript:;" className="collapse">
              </a>
              <a href="#portlet-config" data-toggle="modal" className="config">
              </a>
              <a href="javascript:;" className="reload">
              </a>
              <a href="javascript:;" className="remove">
              </a>
            </div>
              </div>
              <div className="portlet-body">
                <div className="table-toolbar">
                <div style={{overflow:'scroll', height:'290px'}}>
                  <table className="table table-striped table-bordered table-hover" id="sample_1">
                  <thead>
                  <tr>
                  <th style={{width:'200px'}}>
                      Date Created
                    </th >
                    <th style={{width:'200px'}}>
                      Teller Name
                    </th>
                    <th style={{width:'150px'}}>
                    Host Name
                    </th>
                    <th style={{width:'150px'}}>
                   Machine Name
                   </th>
                    <th>
                   Event
                    </th>
                  </tr>
                  </thead>
                  <tbody>
                  {
                    this.state.AuditTrail.map(x=> 
                      <tr>
                      <td>{this.ConvertDateTime(x.DateTime)}</td>
                      <td>{x.UserId}</td>
                      <td>{x.HostName}</td>
                      <td>{x.MachineName}</td>
                      <td>{x.Event}</td> 
                      </tr>
                      
                      )
                  }
                  </tbody>
                  </table>
                    </div>
                    </div>
                    </div>
              </div>
              </div><br/>

            </div>
           
        </section>
        </section>

      </div>
    )
  }
}

export default AuditTrail