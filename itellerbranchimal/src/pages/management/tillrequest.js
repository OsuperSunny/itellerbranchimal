import React, {Component} from "react";
import MockJson from '../../apiService/mockJson';
import Select from 'react-select';
import axios from 'axios';
import apiUrl from '../../apiService/config'
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
var Till =[];
const userName = localStorage.getItem('Id')
class TillRequest extends Component {
  constructor (props){
    super(props)
    this.state = {
    ID:0,
    WithdrawalAmount:0.00,
    CashWithDrawal:{},
    Amount: '0.00',
    TillNum:'',
    AutoCompleteUsers:[],
    AutoCompleteTill:[],
    AutoCompleteGLAccount:[],
    UserTillDetails:{},
    RequesterTill:[],
    GLAccount:'',
    Till:[],
    TillNum:'',
    CashDenominations:[],
    CurrencyCode:0,
    Currency:'',
    SelectedCurrency:'',
    UserId:'',
    ShowError: false,
    ErrorMessage:''
}
}

ChangeAmount = e => {
  let amount = parseFloat(e.target.value.replace(/,/g, ''));
  this.setState({
    [e.target.name]: amount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
  })
}

ClearState = e => {
  this.setState({   WithdrawalAmount:0.00,
    TillNum:'',
    GLAccount:'',
    CurrencyCode:0,
    UserId:''})
    this.setState({ShowError: false, ErrorMessage:''})
}

ChangeCurrency = e =>{
  let cashDenomination = this.state.CashDenominations
  console.log(e.target.value);
  if(e.target.value !== '0'){
    cashDenomination =  cashDenomination.filter(function(data){
          return data.Currency === e.target.value
    })

    let index =  e.target.selectedIndex;
    let el = e.target.childNodes[index];
    let currency = el.getAttribute('id');
    console.log(currency)
    let giverTill = this.state.GiverTill;
    let requesterTill = this.state.RequesterTill;
    requesterTill = requesterTill.filter(function(data){
     return data.Currency === currency;
 })
    giverTill = giverTill.filter(function(data){
     return data.Currency === currency;
 })
    let autoCompleteUsers = [];
    giverTill.forEach(function(element){
      let data = { value: element.UserId, label: element.UserName};
      autoCompleteUsers.push(data);
    })
   console.log(giverTill);
   this.setState({AutoCompleteUsers:autoCompleteUsers,UserTillDetails: requesterTill[0]})
    
   let currencyCode = cashDenomination[0].ID;
   this.setState({CurrencyCode: currencyCode, SelectedCurrency:currency})
  }
 
}

//  handleTillChange= (selectedUser) =>{
//   console.log(selectedUser);
//   let till = this.state.Till;
//   till = till.filter(function(data){
//       return data.ID === selectedUser.value
//   })
//   console.log(till[0])
//   let glAccount = till[0].GLAcctNo + " (" + till[0].GLAcctName + ")";
//   this.setState({GLAccount: glAccount,TillNum:till[0].TillNos,Currency:till[0].Currency})
// }

handleUserChange = (selectedUser) => {
  console.log(selectedUser);
  let giverTill = this.state.GiverTill;
  giverTill = giverTill.filter(function(data){
       return data.UserId === selectedUser.value;
  })
  console.log(giverTill[0]);
  let glAccount = giverTill[0].GLAcctNo + " (" + giverTill[0].GLAcctName + ")";
  this.setState({GLAccount: glAccount,UserId: selectedUser.value, TillNum:giverTill[0].TillNos,Currency:giverTill[0].Currency})
}

SelectCustomStyle(){
  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      borderBottom: '1px dotted pink',
      color: state.isSelected ? 'black' : 'black',
      padding: 10,
    }),
  }
  this.setState({customStyles})
}

SendRequest = e =>{
  let currentComponent = this;
  currentComponent.setState({ShowError: false, ErrorMessage:''})
  console.log(this.state)
  let amount = parseFloat(this.state.Amount.replace(/,/g, ''))
  let data = {GiverTillNo: this.state.TillNum,GiverUser: this.state.UserId,ReceiverTillNo : this.state.UserTillDetails.TillNos, ReceiverUser : this.state.UserTillDetails.UserId,CurrencyCode: this.state.CurrencyCode,Amount: amount}
  if(this.state.CurrencyCode == 0 ){
    currentComponent.setState({ShowError: true, ErrorMessage:'Please select currency'})
  }else{
    if(amount > this.state.UserTillDetails.MaxAmount ){
      currentComponent.setState({ShowError: true, ErrorMessage:'The assigned amount of your till exceeds amount requested to your till'})
      return;
    }
    if(this.state.SelectedCurrency !== this.state.Currency){
      currentComponent.setState({ShowError: true, ErrorMessage:'The selected currency does not match the currency setup for this operation'})
    }else{
      axios.post(apiUrl.Management.RequestTill, data)
      .then(function(response){
        console.log(response.data);
        if(response.data.success){
          NotificationManager.success(response.data.message, "Updated");
          currentComponent.setState({ WithdrawalAmount:0.00,
            TillNum:'',
            GLAccount:'',
            CurrencyCode:0,
            UserId:'',ShowError: false, ErrorMessage:''})
        }else
        {
         NotificationManager.error(response.data.message, "Error")
        }
     }).catch(function(error){
         console.log(error);
     })
    }
   
  }

}

GetDate(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();

  today = dd  + '/' + mm + '/' + yyyy;
  console.log(today);
  return today
}

  async componentDidMount(){
    this.SelectCustomStyle();
    let currentComponent = this;

      await axios.get(apiUrl.Setup.GetUserTill)
      .then(function(response){
      let getTill =  response.data.data;
      console.log(getTill);
      let till = getTill.filter(function(data){
          return data.UserId === userName && currentComponent.GetDate(data.DateCreated)  === currentComponent.GetDate(new Date());
      })
      let giverTill = getTill.filter(function(data){
        return data.UserId != userName && currentComponent.GetDate(data.DateCreated)  === currentComponent.GetDate(new Date());
    })
      console.log(till);
//       let getUsers = till.filter(function(data){
//         return data.UserId != userName  // from localstorage
//  })
 // let autoCompleteUsers = [];
  giverTill.forEach(function(element){
    let data = { value: element.UserId, label: element.UserName};
   // autoCompleteUsers.push(data);
  })
 console.log(giverTill);
//  currentComponent.setState({AutoCompleteUsers:autoCompleteUsers})
      console.log(till)
      if(till.length > 0){
        currentComponent.setState({ UserTillDetails: till[0],RequesterTill: till, GiverTill:giverTill})
      }
      
      })
      // await axios.get(apiUrl.Setup.GetTill)
      // .then(function(response){
      //   let getTill =  response.data.data.TillSetup;
      //    let autoCompleteTill = [];
      //    getTill.forEach(function(element){
      //      let data = { value: element.ID, label: element.TillNos + " (" + element.TillDesc + ")"  };
      //      autoCompleteTill.push(data);
      //    })
      //   console.log(getTill);
      //   currentComponent.setState({Till:getTill,AutoCompleteTill:autoCompleteTill})
      // })
      await axios.get(apiUrl.Security.GetCurrency)
      .then(function(response){
        let getDenominations = response.data.data;
        console.log(getDenominations);
        currentComponent.setState({CashDenominations:getDenominations})
      })

      // await axios.get(apiUrl.Setup.GetUsers)
      // .then(function(response){
      //   let getUsers =  response.data.data.UserDetail;
      //   getUsers = getUsers.filter(function(data){
      //          return data.UserId != userName  // from localstorage
      //   })
      //    let autoCompleteUsers = [];
      //    getUsers.forEach(function(element){
      //      let data = { value: element.UserId, label: element.UserName};
      //      autoCompleteUsers.push(data);
      //    })
      //   console.log(getUsers);
      //   currentComponent.setState({AutoCompleteUsers:autoCompleteUsers})
      // })

  }
  render(){
    return (
      <div>
      <NotificationContainer/>
      <section id="main-content">
      <section className="wrapper">
        <div className="row">
        <div className="col-md-12">
        <h3 className="page-title">
        Till Request <small>request for till</small>
        </h3>
        </div>
        </div>
        <div className="row">
        <div className="col-md-6"> <b style={{fontSize:'15px'}}>Currency: </b>
        <select onClick={this.ChangeCurrency}>
        <option id="0" value={0} disabled selected={true}>---Select Currency---</option>
        {
          this.state.RequesterTill.map(x=>
            <option id={x.Currency} value={x.Currency}>{x.Currency}</option>
            )
        }
        </select> <span style={{fontSize:'14px', paddingLeft:'10px'}}><b>Maximum Amount: </b></span><input value={this.state.UserTillDetails.MaxAmount > 0 ? this.state.UserTillDetails.MaxAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","): '0.00'} disabled  style={{textAlign:'right',fontSize:'15px', width:'200px', fontWeight:'bold'}} type="text" name="Amount" /></div>
        </div><br/> <br/>
        {
          this.state.ShowError === true ? <div  class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
              <b>Close</b></button>
          <span class="glyphicon glyphicon-hand-right"></span> <strong>{this.state.ErrorMessage}</strong>  </div>: ''
        }
        
        <div className="row">
				<div className="col-md-6">
					<div className="portlet box grey-cascade">
            <div className="portlet-title">
            <div className="caption">
            <i className="fa fa-user"></i> Requester
          </div>
          <div className="tools">
            <a href="javascript:;" className="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" className="config">
            </a>
            <a href="javascript:;" className="reload">
            </a>
            <a href="javascript:;" className="remove">
            </a>
          </div>
            </div>
            <div className="portlet-body" style={{height:'325px'}}>
              <div className="table-toolbar">
              <div style={{overflow:'scroll', height:'300px'}}>
								<div className="row">
									<div className="col-md-11">
                  <div className="portlet-body form">
                  <div className="form-body">
                  <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                    <input disabled readOnly  value ={Object.keys(this.state.UserTillDetails).length === 0 ? '' : this.state.UserTillDetails.TillNos} type="text" class="form-control edited"/>
                    <label for="form_control_1"><b>Till Code</b></label>
                    <i className="fa fa-key"></i>
                  </div>
                </div>
                <div className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                  <input disabled readOnly value={this.state.UserTillDetails.UserName} type="text" class="form-control edited"/>
                  <label for="form_control_1"><b>Name</b></label>
                  <i className="fa fa-user"></i>
                </div>
              </div>
              <div className="form-group form-md-line-input has-success form-md-floating-label">
              <div className="input-icon right">
                <input style={{textAlign:'right'}} onChange={this.ChangeAmount}  value={this.state.Amount} type="text" name="Amount" class="form-control edited"/>
                <label for="form_control_1"><b>Amount</b></label>
                <i className="fa fa-money"></i>
              </div>
            </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
            </div>
            </div>
            <div className="col-md-6">
            <div className="portlet box grey-cascade">
              <div className="portlet-title">
              <div className="caption">
              <i className="fa fa-user"></i> Giver
            </div>
            <div className="tools">
              <a href="javascript:;" className="collapse">
              </a>
              <a href="#portlet-config" data-toggle="modal" className="config">
              </a>
              <a href="javascript:;" className="reload">
              </a>
              <a href="javascript:;" className="remove">
              </a>
            </div>
              </div>
              <div className="portlet-body">
                <div className="table-toolbar">
                <div style={{overflow:'scroll', height:'290px'}}>
                  <div className="row">
                  <div className="col-md-12">
                  <div className="portlet-body form">
                  <div className="form-body">
                  <div style={{marginTop:'-20px'}} className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                  <label for="form_control_1"><b style={{color:'#3c763d'}}>User</b></label>
                  <Select
                  defaultOptions
                  styles={this.state.customStyles}
                  onChange={this.handleUserChange}
                  options={this.state.AutoCompleteUsers}
                  theme={theme => ({
                    ...theme,
                    borderRadius: 0,
                    colors: {
                      ...theme.colors,
                      primary25: '#428bca',
                      primary: 'white',
                    },
                  })}
                />
                </div>
                </div>
                
                 <div className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                  <input disabled readOnly value={this.state.TillNum} type="text" class="form-control edited"/>
                  <label for="form_control_1"><b>Till Code</b></label>
                  <i className="fa fa-user"></i>
                </div>
              </div>
              <div className="form-group form-md-line-input has-success form-md-floating-label">
              <div className="input-icon right">
                <input value={this.state.GLAccount} disabled readOnly name="TillNos" type="text" class="form-control edited"/>
                <label for="form_control_1"><b>GL Account</b></label>
                <i className="fa fa-money"></i>
              </div>
            </div>
                  </div>
                  </div>
                  </div>
                    </div>
                    </div>
                    </div>
                    </div>
              </div>
              </div>
            </div>
            <div style={{padding:'0px', textAlign:'right'}}><button onClick={this.ClearState} type="button" className="btn btn-default" data-dismiss="modal">Clear</button><button onClick={this.SendRequest} type="button" className="btn btn-primary"><i class="fa fa-plus"></i> Save Changes</button></div>
        </section>
        </section>

      </div>
    )
  }
}

export default TillRequest