import React, {Component} from "react";
import MockJson from '../../apiService/mockJson';
import Select from 'react-select';
import axios from 'axios';
import apiUrl from '../../apiService/config'
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import GenerateToken from '../../shared/token'
import $ from 'jquery'
var Till =[];
const userName = localStorage.getItem('Id')

function loadAmountFormat(){
  $("input[data-type='currency']").on({
    keyup: function() {
      console.log("exec")
      formatCurrency($(this));
    },
    blur: function() { 
      console.log("exec")
      formatCurrency($(this), "blur");
    }
});

function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val =  left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val =  input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ".00";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}  
}


class CloseTill extends Component {
  constructor (props){
    super(props)
    this.state = {
    ID:0,
    UserId:'',
    ShowError: false,
    ErrorMessage:'',
    UserDetails:{},
    UserIdErrorMessage:'',
    UserName:'',
    Teller_ID:'',
    Time_Opened:'',
    Comment:'',
    OpenTillExecuting: false,
    Balance:0.00,
    Balances:0.00,
    CashRecievedFromVault:0.00,
    CashRecievedFromCustomers:0.00,
    CashTransferedToVault:0.00,
    CashPaidToCustomers:0.00,
    TillTransferIn:0.00,
    OverageShortage:0.00,
    TillTransferOut:0.00,
    BalanceMessage:'',
    BalanceMessageColor:'',
    DisableCloseButton:true,
    BalanceMessageIcon: ''
}
}

ChangeUser =  e => {
  let currentComponent = this;
 
  currentComponent.setState({UserIdErrorMessage:'', UserDetails:{}})
  if(e.target.value.length === 0){
    console.log(e.target.value.length);
    currentComponent.setState({UserIdErrorMessage:'', UserName:'',Teller_ID:'',Time_Opened:''})
  }else{
   axios.get(apiUrl.BankService.GetUserDetails + 'userId=' + e.target.value +  '&access_token=' + localStorage.getItem('access_token'))
    .then(function(response){
      console.log(response.data);
      if(response.data.success){
        currentComponent.setState({UserDetails: response.data.UserDetails, UserName: response.data.UserDetails.UserName, Teller_ID:response.data.UserDetails.Teller_ID,Time_Opened:response.data.UserDetails.Time_Opened})
      }else{
        currentComponent.setState({UserIdErrorMessage:'* User ID is invalid', UserName:'',Teller_ID:'',Time_Opened:''})
      }
      
    }).catch(function(error){
      console.log(error);
      currentComponent.setState({UserIdErrorMessage:'* User ID is invalid', UserName:'',Teller_ID:'',Time_Opened:''})
    })
  }

}

ChangeBalance = e =>{
  console.log(e.target.value)
 let balance =  parseFloat(e.target.value.replace(/,/g, ''))
 console.log(balance)
    this.setState({Balance:balance})
}

ChangeComment = e => {
  this.setState({[e.target.name]: e.target.value})
}

ClearState = e => {
  this.setState({   WithdrawalAmount:0.00,
    UserName:'',
    Teller_ID:'',
    Time_Opened:'',
    Comment:'',})
    this.setState({ShowError: false, ErrorMessage:''})
}

ConvertDate(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();
  today = yyyy + '-' + mm + '-' + dd;
  console.log(today);
  return today
}

SendRequest = async e =>{
  let currentComponent = this;
  currentComponent.setState({ShowError:false, ErrorMessage: '',OpenTillExecuting:true})
  if(this.state.Teller_ID === ''){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Teller ID not found.', OpenTillExecuting:false})
    return;
  }
   if(this.state.Comment === ''){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Please enter comment.', OpenTillExecuting:false})
   }
  let data = {
     "Id": 0,"TransactionBranch": this.state.UserDetails.UserTillBranch,"TellerId": this.state.Teller_ID,"Status": "CLOSED","Comments": this.state.Comment,"IsHeadTeller": "NO","User": this.state.UserDetails.User,"ApprovedBy": localStorage.getItem('Id'), "ApprovedDate":  currentComponent.ConvertDate(new Date()),"DateCreated":  currentComponent.ConvertDate(new Date()), "IsClosed": false, "Event": this.state.UserDetails.UserName + " closed till","CBAResponse": "",ClosingBalance:this.state.Balance, ShortageOverageAmount: this.state.OverageShortage
  }
  console.log(data)
  await axios.post(apiUrl.BankService.CloseTill, data)
  .then(function(response){
    if(response.data.success){
      currentComponent.setState({OpenTillExecuting:false,  UserName:'',Teller_ID:'',
      Time_Opened:'',Comment:''})
      window.$('#CashBalanceModal').modal('toggle');
      NotificationManager.success('Success', 'Sent for approval',2000)
    }else{
    currentComponent.setState({OpenTillExecuting: false})
    console.log(response.data)
    NotificationManager.error('Error', 'Till already sent for approval for closure',2000)
    }
  }).catch(function(error){
    currentComponent.setState({OpenTillExecuting: false})
    console.log(error)
    NotificationManager.error('Error', 'Server Error', 2000)
  })
}

  async componentDidMount(){
    let currentComponent = this;
    await GenerateToken();
    loadAmountFormat();
    await axios.get(apiUrl.BankService.GetUserDetails + 'userId=' + localStorage.getItem('Id') +  '&access_token=' + localStorage.getItem('access_token'))
      .then(function(response){
        console.log(response.data);
        if(response.data.success){
          currentComponent.setState({UserDetails: response.data.UserDetails, UserName: response.data.UserDetails.UserName, Teller_ID:response.data.UserDetails.Teller_ID,Time_Opened:response.data.UserDetails.Time_Opened,TillStatus: response.data.UserDetails.TillStatus})
        }else{
           NotificationManager.error('Error', 'Session Expired. Please Login again', 1000)
        }
        
      }).catch(function(error){
        console.log(error);
        NotificationManager.error('Error', 'Server Error', 1000)
      })
  }
  render(){
    return (
      <div>
      <NotificationContainer/>
      <section id="main-content">
      <section className="wrapper">
        <div className="row">
        <div className="col-md-12">
        <h3 className="page-title">
        Close Till <small></small>
        </h3>
        </div>
        </div>
        <div className="row">
     
        </div><br/> <br/>
        {
          this.state.ShowError === true ? <div  class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
              <b>Close</b></button>
          <span class="glyphicon glyphicon-hand-right"></span> <strong>{this.state.ErrorMessage}</strong>  </div>: ''
        }
        
        <div className="row">
				<div className="col-md-12">
					<div className="portlet box grey-cascade">
            <div className="portlet-title">
            <div className="caption">
            <i className="fa fa-user"></i> User Till
          </div>
          <div className="tools">
            <a href="javascript:;" className="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" className="config">
            </a>
            <a href="javascript:;" className="reload">
            </a>
            <a href="javascript:;" className="remove">
            </a>
          </div>
            </div>
            <div className="portlet-body" style={{height:'325px'}}>
              <div className="table-toolbar">
              <div style={{overflow:'scroll', height:'300px'}}>
								<div className="row">
									<div className="col-md-6">
                  <div className="portlet-body form">
                  <div className="form-body">
                  <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                    <input value={localStorage.getItem('Id')} readOnly disabled  type="text" name="TillCode" class="form-control edited"/>
                    <label for="form_control_1"><b>User Id</b></label>
                    <span style={{fontSize:'12px', color:'red'}} className="help-block"></span>
                    <i className="fa fa-key"></i>
                  </div>
                </div>
                <div className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                  <input disabled readOnly value={this.state.Teller_ID} type="text" class="form-control edited"/>
                  <label for="form_control_1"><b>Teller ID</b></label>
                  <i className="fa fa-user"></i>
                </div>
              </div>
              <div className="form-group form-md-line-input has-success form-md-floating-label">
              <div className="input-icon right">
              <input disabled readOnly value={this.state.TillStatus} type="text" class="form-control edited"/>
              <label for="form_control_1"><b>Till Status</b></label>
              <i className="fa fa-toggle-on"></i>
            </div>
            </div>
                  </div>
                  </div>
                  </div>
                  <div className="col-md-6">
                  <div className="portlet-body form">
                  <div className="form-body">
                  <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                  <input disabled readOnly value={this.state.Time_Opened} type="text" class="form-control edited"/>
                  <label for="form_control_1"><b>Time Opened</b></label>
                  <i className="fa fa-clock-o"></i>
                </div>
                </div>
              <div className="form-group form-md-line-input has-success form-md-floating-label">
              <div className="input-icon right">
                <input readOnly disabled   value={this.state.UserName} type="text" name="Amount" class="form-control edited"/>
                <label for="form_control_1"><b>Name</b></label>
                <i className="fa fa-user"></i>
              </div>
            </div>
            <div className="form-group form-md-line-input has-success form-md-floating-label">
            <div className="input-icon right">
              <input name="Comment" onChange={this.ChangeComment} value={this.state.Comment} type="text" class="form-control edited"/>
              <label for="form_control_1"><b>Comment</b></label>
              <i className="fa fa-file"></i>
            </div>
          </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
            </div>
            </div>
            </div>
            <div style={{paddingLeft:'0px', textAlign:'right'}}><button onClick={this.ClearState} type="button" className="btn btn-default" data-dismiss="modal">Clear</button><button  onClick={this.SendRequest} type="button" className={"btn btn-primary"}><i class="fa fa-ban"></i> Close {this.state.OpenTillExecuting === true ? <i id="spinner" class="fa fa-spinner fa-spin"></i> : ''}</button></div>
        </section>
        </section>

      </div>
    )
  }
}

export default CloseTill