import React, {Component} from "react";
import MockJson from '../../apiService/mockJson';
import Select from 'react-select';
import axios from 'axios';
import apiUrl from '../../apiService/config'
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import GenerateToken from '../../shared/token'
// import ValidatePageAccess from '../../shared/ValidatePageAccess'
import $ from 'jquery'
var Till =[];
const userName = localStorage.getItem('Id')

function loadAmountFormat(){
  $("input[data-type='currency']").on({
    keyup: function() {
      console.log("exec")
      formatCurrency($(this));
    },
    blur: function() { 
      console.log("exec")
      formatCurrency($(this), "blur");
    }
});

function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val =  left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val =  input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ".00";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}  
}


class Balance extends Component {
  constructor (props){
    super(props)
    this.state = {
    ID:0,
    UserId:'',
    ShowError: false,
    ErrorMessage:'',
    UserDetails:{},
    UserIdErrorMessage:'',
    UserName:'',
    Teller_ID:'',
    Time_Opened:'',
    Comment:'',
    OpenTillExecuting: false,
    Balance:0.00,
    Balances:0.00,
    CashRecievedFromVault:0.00,
    CashRecievedFromCustomers:0.00,
    CashTransferedToVault:0.00,
    CashPaidToCustomers:0.00,
    TillTransferIn:0.00,
    OverageShortage:0.00,
    TillTransferOut:0.00,
    CashBroughtForward:0.00,
    ExpectedCashAtHand:0.00,
    BalanceMessage:'',
    BalanceMessageColor:'',
    BalanceMessageIcon: '',
    CashDenominations:[],
    CurrencyCode:'',
    CurrencyId:0,
    TillStatus:'',
    SOcolor:'black',
    TellerDetails:{}
}
}

ChangeUser =  e => {
  let currentComponent = this;
 
  currentComponent.setState({UserIdErrorMessage:'', UserDetails:{}})
  if(e.target.value.length === 0){
    console.log(e.target.value.length);
    currentComponent.setState({UserIdErrorMessage:'', UserName:'',Teller_ID:'',Time_Opened:''})
  }else{
   axios.get(apiUrl.BankService.GetUserDetails + 'userId=' + e.target.value +  '&access_token=' + localStorage.getItem('access_token'))
    .then(function(response){
      console.log(response.data);
      if(response.data.success){
        currentComponent.setState({UserDetails: response.data.UserDetails, UserName: response.data.UserDetails.UserName, Teller_ID:response.data.UserDetails.Teller_ID,Time_Opened:response.data.UserDetails.Time_Opened})
      }else{
        currentComponent.setState({UserIdErrorMessage:'* User ID is invalid', UserName:'',Teller_ID:'',Time_Opened:''})
      }
      
    }).catch(function(error){
      console.log(error);
      currentComponent.setState({UserIdErrorMessage:'* User ID is invalid', UserName:'',Teller_ID:'',Time_Opened:''})
    })
  }

}

ChangeBalance = e =>{
  console.log(e.target.value)
 let balance =  parseFloat(e.target.value.replace(/,/g, ''))
 console.log(balance)
    this.setState({Balance:balance})
}

ChangeComment = e => {
  this.setState({[e.target.name]: e.target.value})
}

ChangeCurrency = async e =>{
  console.log(e.target.value);
  let currentComponent = this;
  
  let currencyId = parseInt(e.target.value);
  if(currencyId > 0){
    this.setState({DisableRequest:true})
    let currency = this.state.CashDenominations.filter(function(data){
      return data.ID === currencyId
    })[0];
  
    await axios.get(apiUrl.BankService.GetTellerDetails  + localStorage.getItem("Id") + '/CurrCode/' + currency.CurrencyCode +  '/Token/' + localStorage.getItem('access_token'))
    .then(function(response){
      console.log(response.data.details);
      let tellerDetails = response.data.details
      console.log(Array.isArray(tellerDetails));
      if(Array.isArray(tellerDetails)){
        if(tellerDetails.length > 0){
         currentComponent.setState({TellerDetails: response.data.details[0],IsTellerValid:true})
        }else{
         currentComponent.setState({IsTellerValid:false})
        }
      
      }else{
        if( Object.keys(tellerDetails).length === 0){
         currentComponent.setState({IsTellerValid:false})
        }else{
         currentComponent.setState({TellerDetails: tellerDetails,IsTellerValid:true})
        }
      }
      currentComponent.setState({DisableRequest: false})
    }).catch(function(error){
     currentComponent.setState({DisableRequest: false})
     currentComponent.setState({IsTellerValid:false})
      console.log(error);
    })
  }
  //currentComponent.setState({DisableRequest: false})
  this.setState({CurrencyId: currencyId})
}

ClearState = e => {
  this.setState({   WithdrawalAmount:0.00,
    UserName:'',
    Teller_ID:'',
    Time_Opened:'',
    Comment:'',})
    this.setState({ShowError: false, ErrorMessage:''})
}

ConvertDate(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();
  today = yyyy + '-' + mm + '-' + dd;
  console.log(today);
  return today
}

ConfirmRequest = e =>{
  window.$('#ConfirmModal').modal('toggle');
}

SendRequest = async e =>{
  let currentComponent = this;
  currentComponent.setState({ShowError:false, ErrorMessage: '',OpenTillExecuting:true})
  let data = {
     "Id": 0,"TransactionBranch": this.state.UserDetails.UserTillBranch,"TellerId": this.state.Teller_ID,"Status": "OPEN","Comments": this.state.Comment,"IsHeadTeller": "NO","User": this.state.UserDetails.User,"ApprovedBy": localStorage.getItem('Id'), "ApprovedDate":  currentComponent.ConvertDate(new Date()),"DateCreated":  currentComponent.ConvertDate(new Date()), "IsClosed": false, "Event": this.state.UserDetails.UserName + " confirmed till balance","CBAResponse": "",ClosingBalance:this.state.Balance, ShortageOverageAmount: this.state.OverageShortage
  }
  console.log(data)
  await axios.post(apiUrl.Management.ConfirmBalance, data)
  .then(function(response){
    if(response.data.success){
      currentComponent.setState({OpenTillExecuting:false})
      window.$('#CashBalanceModal').modal('toggle');
      window.$('#ConfirmModal').modal('toggle');
      NotificationManager.success('Success', 'Till Balanced confirmed successfully',2000)
    }else{
    currentComponent.setState({OpenTillExecuting: false})
    console.log(response.data)
    NotificationManager.error('Error', 'Till balance already confirmed',2000)
    }
  }).catch(function(error){
    currentComponent.setState({OpenTillExecuting: false})
    console.log(error)
    NotificationManager.error('Error', 'Server Error', 2000)
  })
}


CheckBalance = async e=>{
  let currentComponent = this;

  currentComponent.setState({ShowError:false, ErrorMessage: ''})
  if(this.state.Teller_ID === ''){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Teller ID not found.', OpenTillExecuting:false})
    return;
  }
  if(this.state.CurrencyId === '' || this.state.CurrencyId <= 0){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Please select a currency.', OpenTillExecuting:false})
    return;
  }
  console.log(this.state.Balance);
  // if(this.state.Balance <= 0.00){
  //   window.$("html, body").animate({ scrollTop: 0 }, "slow");
  //   currentComponent.setState({ShowError:true, ErrorMessage: 'Please input balance.', OpenTillExecuting:false})
  //   return;
  // }
  // if(this.state.CurrencyCode === ''){
  //   window.$("html, body").animate({ scrollTop: 0 }, "slow");
  //   currentComponent.setState({ShowError:true, ErrorMessage: 'Please select a currency.', OpenTillExecuting:false})
  //   return;
  // }
  window.$('#CashBalanceModal').modal('toggle');
  await currentComponent.GetTilltransaction(this.state.Teller_ID, this.state.CurrencyId);
}
  
  async GetTilltransaction(tellerId, currencyId){
    let currentComponent = this;
    let branchCode = this.state.TellerDetails.BRANCH_CODE + "";
    branchCode = branchCode.padStart(4,'0') 
    let tellerCIF = this.state.TellerDetails.CIF_NO + "";
  tellerCIF = tellerCIF.padStart(8,'0') 
    let balance = parseFloat(currentComponent.state.Balance) 
    let data = {CurrencyId: currencyId, BranchCode: branchCode, CIFnumber:tellerCIF}
    await axios.post(apiUrl.Management.GetTillTransaction , data) 
    .then(function(response){
      console.log(response.data);
      currentComponent.setState({
        CashRecievedFromVault:response.data.CashReceivedFromVault,
        CashRecievedFromCustomers:response.data.CashRecievedFromCustomers,
        CashTransferedToVault:response.data.CashTransferedToVault,
        CashPaidToCustomers:response.data.CashPaidToCustomers,
        TillTransferIn:response.data.TillTransferIn,
        TillTransferOut:response.data.TillTransferOut,
        CashBroughtForward:response.data.CashBroughtForward,
        Balances: balance
      })
      //response.data.CashTransferedToVault +
      // let credit = response.data.CashBroughtForward + response.data.CashReceivedFromVault + response.data.CashRecievedFromCustomers + response.data.TillTransferIn + currentComponent.state.Balance ;
      // let debit = response.data.CashPaidToCustomers + response.data.TillTransferOut + response.data.CashTransferedToVault;
      let expectedCash = (response.data.CashBroughtForward + response.data.CashReceivedFromVault + response.data.CashRecievedFromCustomers + response.data.TillTransferIn) - (response.data.CashPaidToCustomers + response.data.TillTransferOut + response.data.CashTransferedToVault);
      currentComponent.setState({ExpectedCashAtHand: expectedCash});

      // let credit = response.data.CashRecievedFromVault -( response.data.CashRecievedFromCustomers + response.data.TillTransferIn);
      // let debit = currentComponent.state.Balance  - (response.data.CashPaidToCustomers + response.data.TillTransferOut);
      let overageShortage =  currentComponent.state.Balance - expectedCash;
      // (response.data.CashRecievedFromVault + response.data.CashRecievedFromCustomers + response.data.TillTransferIn) - (currentComponent.state.Balance  + response.data.CashPaidToCustomers + response.data.TillTransferOut)
      if(overageShortage === 0.00){
        currentComponent.setState({BalanceMessageIcon:'fa-check-circle-o',SOcolor:'black', BalanceMessageColor: 'green',BalanceMessage: 'Till Account is balanced'})
      }else if(overageShortage > 0.00){
        currentComponent.setState({BalanceMessageIcon:'fa-exclamation-circle',SOcolor:'blue',BalanceMessageColor: 'blue', BalanceMessage: 'Till Account is not balanced (Overage)'})
      }else if(overageShortage < 0.00){
        currentComponent.setState({BalanceMessageIcon:'fa-exclamation-circle',SOcolor:'red',BalanceMessageColor: 'red', BalanceMessage: 'Till Account is not balanced(Shortage)'})
      }
      currentComponent.setState({OverageShortage:overageShortage})
    }).catch(function(error){
      console.log(error);
      NotificationManager.error('Error', 'Server Error', 1000)
    })
  }
  // async componentWillReceiveProps(){
  //   await ValidatePageAccess("Balance");
  // }
  async componentDidMount(){
    let currentComponent = this;
   // await ValidatePageAccess("Balance");
    await GenerateToken();
    loadAmountFormat();
    await axios.get(apiUrl.Security.GetCurrency)
    .then(function(response){
      let getDenominations = response.data.data;
      console.log(getDenominations);
      currentComponent.setState({CashDenominations:getDenominations})
    })
    await axios.get(apiUrl.BankService.GetUserDetails + 'userId=' + localStorage.getItem('Id') +  '&access_token=' + localStorage.getItem('access_token'))
      .then(function(response){
        console.log(response.data);
        if(response.data.success){
          currentComponent.setState({UserDetails: response.data.UserDetails, UserName: response.data.UserDetails.UserName, Teller_ID:response.data.UserDetails.Teller_ID,Time_Opened:response.data.UserDetails.Time_Opened, TillStatus: response.data.UserDetails.TillStatus})
        }else{
           NotificationManager.error('Error', 'Session Expired. Please Login again', 1000)
        }
        
      }).catch(function(error){
        console.log(error);
        NotificationManager.error('Error', 'Server Error', 1000)
      })
  }
  render(){
    return (
      <div>
      <NotificationContainer/>
      <section id="main-content">
      <section className="wrapper">
        <div className="row">
        <div className="col-md-12">
        <h3 className="page-title">
        Check Balance <small></small>
        </h3>
        </div>
        </div>
        <div className="row">
     
        </div><br/> <br/>
        {
          this.state.ShowError === true ? <div  class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
              <b>Close</b></button>
          <span class="glyphicon glyphicon-hand-right"></span> <strong>{this.state.ErrorMessage}</strong>  </div>: ''
        }
        
        <div className="row">
				<div className="col-md-12">
					<div className="portlet box grey-cascade">
            <div className="portlet-title">
            <div className="caption">
            <i className="fa fa-user"></i> User Till
          </div>
          <div className="tools">
            <a href="javascript:;" className="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" className="config">
            </a>
            <a href="javascript:;" className="reload">
            </a>
            <a href="javascript:;" className="remove">
            </a>
          </div>
            </div>
            <div className="portlet-body" style={{height:'370px'}}>
              <div className="table-toolbar">
              <div style={{overflow:'scroll', height:'350px'}}>
								<div className="row">
									<div className="col-md-6">
                  <div className="portlet-body form">
                  <div className="form-body">
                  <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                    <input value={localStorage.getItem('Id')} readOnly disabled  type="text" name="TillCode" class="form-control edited"/>
                    <label for="form_control_1"><b>User Id</b></label>
                    <span style={{fontSize:'12px', color:'red'}} className="help-block"></span>
                    <i className="fa fa-key"></i>
                  </div>
                </div>
                <div className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                  <input readOnly disabled   value={this.state.UserName} type="text" name="Amount" class="form-control edited"/>
                  <label for="form_control_1"><b>Name</b></label>
                  <i className="fa fa-user"></i>
                </div>
              </div>
                <div className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                  <input disabled readOnly value={this.state.Teller_ID} type="text" class="form-control edited"/>
                  <label for="form_control_1"><b>Teller ID</b></label>
                  <i className="fa fa-user"></i>
                </div>
              </div>
              <div className="form-group form-md-line-input has-success form-md-floating-label">
              <div className="input-icon right">
              <input disabled readOnly value={this.state.Time_Opened} type="text" class="form-control edited"/>
              <label for="form_control_1"><b>Time Opened</b></label>
              <i className="fa fa-clock-o"></i>
            </div>
            </div>
                  </div>
                  </div>
                  </div>
                  <div className="col-md-6">
                  <div className="portlet-body form">
                  <div className="form-body">
                  <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                  <input disabled readOnly value={this.state.TillStatus} type="text" class="form-control edited"/>
                  <label for="form_control_1"><b>Till Status</b></label>
                  <i className="fa fa-toggle-on"></i>
                </div>
                </div>
                <div className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                <select className="form-control edited" onClick={this.ChangeCurrency}>
                <option id="0" value={0} disabled selected={true}>---Select Currency---</option>
                {
                  this.state.CashDenominations.map(x=>
                    <option id={x.Currency} value={x.ID}>{x.Currency}</option>
                    )
                }
                </select>
                <label for="form_control_1"><b>Currency</b></label>
                <span style={{fontSize:'12px'}} className="help-block">Select the currency of the amount stipulated..</span>
              </div>
              </div>
            <div className="form-group form-md-line-input has-success form-md-floating-label">
            <div className="input-icon right" style={{width:'75%', display:'inline-block'}}>
              <input onChange={this.ChangeBalance} data-type="currency" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" style={{textAlign:'right'}}  defaultValue={"0.00"} type="text" name="Amount" class="form-control edited"/>
              <label for="form_control_1"><b>Cash at hand</b></label>
              <span className="help-block">Please enter how much cash you have..</span>
            </div>
            <button disabled={this.state.DisableRequest}  onClick={this.CheckBalance} data-toggle="modal"  style={{height:'33px',float:'right'}} className="btn btn-primary"><i className="fa fa-eye"></i> Check Balance </button>
          </div>
              
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
            </div>
            </div>
            </div>
            <div style={{paddingLeft:'0px', textAlign:'right'}}><button onClick={this.ClearState} type="button" className="btn btn-default" data-dismiss="modal">Clear</button></div>
        </section>
        </section>


        <div className="modal fade" id="CashBalanceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content" style={{width:'700px'}}>
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 className="modal-title" id="myModalLabel">Till Balance</h4>
            </div>
            <div className="modal-body">
            <span  style={{color:this.state.BalanceMessageColor, fontSize:'14px', fontWeight:'bold'}}><i className={"fa " + this.state.BalanceMessageIcon}></i> {this.state.BalanceMessage}</span>
            <div className="row">
            <div className="portlet box grey-cascade">

        

            <div className="portlet-body" >
            <div className="table-toolbar">
            <div style={{overflow:'scroll'}}>
      
            <table className="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
            <tr>
            <th style={{width:'300px'}}>
            Description 
            </th>
            <th>
            Cash Inflow 
            </th>
            <th>
            Cash Outflow
            </th>
            </tr>
            </thead>
            <tbody>
            <tr>
            <td>
            Cash brought forward 
            </td>
            <td>
            </td>
            <td style={{textAlign:'right'}}>
            {this.state.CashBroughtForward > 0 ? this.state.CashBroughtForward.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '0.00'}
            </td>
            </tr>

            <tr>
            <td>
            Cash received from Vault
            </td>
            <td>
            </td>
            <td style={{textAlign:'right'}}>
            {this.state.CashRecievedFromVault > 0 ? this.state.CashRecievedFromVault.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '0.00'}
            </td>
            </tr>

            <tr>
            <td>
            Cash Transfered to vault
            </td>
            <td style={{textAlign:'right'}}>
            {this.state.CashTransferedToVault > 0 ? this.state.CashTransferedToVault.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '0.00'}
            </td>
            <td>
            </td>
            </tr>

            <tr>
            <td>
            Cash received from other tills
            </td>
            <td style={{textAlign:'right'}}>
            {this.state.TillTransferIn > 0 ? this.state.TillTransferIn.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '0.00'}
            </td>
            <td>
            </td>
            </tr>

            <tr>
            <td>
            Cash transfered to other tills
            </td>
            <td>
            </td>
            <td style={{textAlign:'right'}}>
            {this.state.TillTransferOut > 0 ? this.state.TillTransferOut.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '0.00'}
            </td>
            </tr>

            <tr>
            <td>
            Cash received from customers
            </td>
            <td style={{textAlign:'right'}}>
            {this.state.CashRecievedFromCustomers > 0 ? this.state.CashRecievedFromCustomers.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '0.00'}
            </td>
            <td>
            </td>
            </tr>

            <tr>
            <td>
            Cash paid to customers
            </td>
            <td>
            </td>
            <td style={{textAlign:'right'}}>
            {this.state.CashPaidToCustomers > 0 ? this.state.CashPaidToCustomers.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '0.00'}
            </td>
            </tr>

            <tr>
            <td>
            Expected cash at Hand
            </td>
            <td>
            </td>
            <td style={{textAlign:'right'}}>
            {this.state.ExpectedCashAtHand.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
            </td>
            </tr>

            <tr>
            <td>
            Cash at hand
            </td>
            <td>
            </td>
            <td style={{textAlign:'right'}}>
            {this.state.Balances.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
            </td>
            </tr>

            <tr>
            <td>
           <b>Shortage/Overage</b>
            </td>
            <td>
            </td>
            <td style={{textAlign:'right'}}>
            <b><span style={{color:this.state.SOcolor}}>{this.state.OverageShortage.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span></b> 
            </td>
            </tr>
            </tbody>
            </table>
</div>
</div></div></div>
         
            </div>
            </div>
            <div className="modal-footer">
              <button  type="button" className="btn btn-default" data-dismiss="modal">Close</button><button onClick={this.ConfirmRequest} type="button" className={"btn btn-primary"}><i class="fa fa-thumbs-o-up"></i> Confirm </button>
            </div>
          </div>
        </div>
      </div>



     <div aria-hidden="true" aria-labelledby="myModalLabel" id="ConfirmModal" role="dialog" tabindex="-1"  class="modal fade">
<div class="modal-dialog" style={{width:'350px'}}>
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4  style={{color:'white'}} className="modal-title" id="myModalLabel">Confirm Balance</h4>
    </div>
    <div class="modal-body">
      <p><b>Are you sure you want to comfirm Till Balance?</b></p>
      <p style={{color:'red'}}><b>Note:</b> Clicking confirm button will finalize balance checking for the day. </p>
    </div>
    <div class="modal-footer">
    <button  type="button" className="btn btn-default" data-dismiss="modal">Close</button><button onClick={this.SendRequest} type="button" className={"btn btn-primary"}><i class="fa fa-thumbs-o-up"></i> Confirm {this.state.OpenTillExecuting === true ? <i id="spinner" class="fa fa-spinner fa-spin"></i> : ''}</button>
    </div>
  </div>
</div>
</div>

      </div>
    )
  }
}

export default Balance