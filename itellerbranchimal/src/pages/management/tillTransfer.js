import React, {Component} from "react";
import DataTable from '../../shared/dataTable'
import MockJson from '../../apiService/mockJson'
import axios from 'axios';
import apiUrl from '../../apiService/config'
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import $ from 'jquery';
import Select from 'react-select';
import Till from "../setup/till";
const userName = localStorage.getItem('Id')
class TillTransfer extends Component {
  constructor (props){
    super(props)
    this.state = {
    ID:0,
    TillNos:'',
    TillDesc: '',
    GLAccountID:0,
    MinTillAmount: '0.00',
    MaxTillAmount: '0.00',
    Till:[],
}
}

TransferTill = e =>{
  let till = this.state.Till;
  console.log(e.target.id);
  console.log(till);
  till = till.filter(function(data){
     return data.ID === parseInt(e.target.id);
  })
  localStorage.setItem('AccountNumber', till[0].AccountNum);
  localStorage.setItem('AccountName', till[0].AccountName);
  localStorage.setItem('Amount', till[0].TillAmount);
  localStorage.setItem('UserId', till[0].UserId)
  localStorage.setItem('IsTillTransfer', 1);
  localStorage.setItem('TillTransferID', till[0].ID);
  window.location.href = '/pages/cashWithdrawal';
}

 async componentDidMount(){
 let currentComponent = this;
 console.log(userName);
 await axios.get(apiUrl.Management.GetRequestedTill + userName)
 .then(function(response){
   let getTill =  response.data;
   console.log(response.data)
   let till = [];
   response.data.forEach(element => {
     if( element.RequesterTillDetails.length > 0){
      let data = {ID: element.Id, UserId:element.RequesterUserDetails[0].UserId, User: element.RequesterUserDetails[0].UserName,Currency: element.Currency, TillNum: element.RequesterTillDetails[0].TillNos, TillAmount: element.Amount, GLAccount:element.RequesterTillDetails[0].GLAcctNo + " (" + element.RequesterTillDetails[0].GLAcctName + ")", AccountNum: element.RequesterTillDetails[0].GLAcctNo, AccountName: element.RequesterTillDetails[0].GLAcctName}
  till.push(data);   
     }
      
   });
   console.log(till);
   currentComponent.setState({Till: till})
 })
    DataTable();
}

  render(){
    return (
      <div>
      <NotificationContainer/>
      <section id="main-content">
      <section className="wrapper">
        <div className="row">
        <div className="col-md-12">
        <h3 className="page-title">
        Till transfer <small>transfer till</small>
        </h3>
        </div>
        </div><br/><br/>
        <div className="row">
				<div className="col-md-12">
					<div className="portlet box grey-cascade">
            <div className="portlet-title">
            <div className="caption">
            <i className="fa fa-money"></i>
          </div>
          <div className="tools">
            <a href="javascript:;" className="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" className="config">
            </a>
            <a href="javascript:;" className="reload">
            </a>
            <a href="javascript:;" className="remove">
            </a>
          </div>
            </div>
            <div className="portlet-body">
							<div className="table-toolbar">
                  <table className="table table-striped table-bordered table-hover" id="sample_1">
                  <thead>
                  <tr>  
                    <th>
                       Till Number 
                    </th>
                    <th>
                     GL Account
                    </th>
                    <th>
                    Currency
                    </th>
                    <th>
                   User
                    </th>
                    <th>
                   Till Amount
                     </th>
                     <th>
                    Action
                     </th>
                  </tr>
                  </thead>
                  <tbody>
                  {
                    this.state.Till.map(x=> 
                     <tr key={x.ID}>
                     <td>{x.TillNum}</td>
                  <td>{x.GLAccount}</td>
                  <td>{x.Currency}</td>
                  <td>{x.User}</td>
                  <td style={{textAlign: 'right'}}>{x.TillAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                  <td> <a id={x.ID} onClick={this.TransferTill}  href="#" className="btn btn-primary a-btn-slide-text" >
                  <span id={x.ID} className="fa fa-arrow-circle-right" aria-hidden="true"></span>
                  <span id={x.ID} ><strong id={x.ID}>  Transfer</strong></span>  </a></td>
                     </tr> 
                      
                      )
                  }
                  </tbody>
                  </table>
                  </div>
                  </div>
            </div>
            </div>
            </div>
        </section>
        </section>


      </div>
    )
  }
}

export default TillTransfer