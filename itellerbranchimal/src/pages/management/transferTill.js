import React, {Component} from "react";
import MockJson from '../../apiService/mockJson';
import Select from 'react-select';
import axios from 'axios';
import apiUrl from '../../apiService/config'
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import $ from 'jquery';
var Till =[];
const userName = localStorage.getItem('Id')

function loadAmountFormat(){
  $("input[data-type='currency']").on({
    keyup: function() {
      console.log("exec")
      formatCurrency($(this));
    },
    blur: function() { 
      console.log("exec")
      formatCurrency($(this), "blur");
    }
});

function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val =  left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val =  input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ".00";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}  
}

class TransferTill extends Component {
  constructor (props){
    super(props)
    this.state = {
    ID:0,
    Amount: '0.00',
    CashDenominations:[],
    CurrencyCode:'',
    Currency:'',
    SelectedCurrency:'',
    UserId:'',
    ShowError: false,
    ErrorMessage:'',
    Remarks:'',
    TellerID:'',
    UserDetails:{},
    TransRef:'',
    TransferTillExecuting:false,
    CIFtoTeller:'',
    CIFteller:'',
    BranchCode:'',
    CurrencyId:0,
    TellerDetails:{},
    ApiData:{},
    DisableButton:false,
    IsTellerValid:false,
    DisableRequest:false,
    UserTillDetails:{}
}
}

ChangeHandler = e =>{
  this.setState({[e.target.name]: e.target.value});
}

ChangeRemarks = e =>{
  this.setState({[e.target.name]: e.target.value});
}

ChangeAmount = e => {
  let amount = parseFloat(e.target.value.replace(/,/g, ''));
  this.setState({
    [e.target.name]: amount
  })
}

ClearState = e => {
  this.setState({  CIFteller:'', CIFtoTeller:'', BranchCode:'',
    Remarks:'',CurrencyCode:'',
    TellerID:'', Amount: '0.00',})
    this.setState({ShowError: false, ErrorMessage:''})
}

ChangeCurrency = async e =>{
  let currentComponent = this;
  console.log(e.target.value);
  let currencyCode =parseInt(e.target.value);
  currentComponent.setState({DisableRequest: true})
  if(currencyCode > 0){
    let currency = this.state.CashDenominations.filter(function(data){
      return data.ID === currencyCode
    })
    console.log(currency);
    this.setState({CurrencyCode: currency[0].Abbrev, CurrencyId: currencyCode})
    axios.get(apiUrl.BankService.GetTellerDetails  + localStorage.getItem("Id") + '/CurrCode/' + currency[0].CurrencyCode +  '/Token/' + localStorage.getItem('access_token'))
    .then(function(response){
      console.log(response.data.details);
      let tellerDetails = response.data.details
      console.log(Array.isArray(tellerDetails));
      if(Array.isArray(tellerDetails)){
        if(tellerDetails.length > 0){
         currentComponent.setState({TellerDetails: response.data.details[0],IsTellerValid:true})
        }else{
         currentComponent.setState({IsTellerValid:false})
        }
      
      }else{
        if( Object.keys(tellerDetails).length === 0){
         currentComponent.setState({IsTellerValid:false})
        }else{
         currentComponent.setState({TellerDetails: tellerDetails,IsTellerValid:true})
        }
      }
      currentComponent.setState({DisableRequest: false})
     //  if(tellerDetails.length > 0){
     //    currentComponent.setState({TellerDetails: response.data.details[0],IsTellerValid:true})
     //  }else{
     //   currentComponent.setState({IsTellerValid:false})
     //  }
    }).catch(function(error){
     currentComponent.setState({DisableRequest: false})
     currentComponent.setState({IsTellerValid:false})
      console.log(error);
    })
  }
}

async ValidateUser (Id){
  let currentComponent = this;
  let currencyCode =parseInt(Id);
  let IsTellerValid = false;
  if(currencyCode > 0){
    let currency = this.state.CashDenominations.filter(function(data){
      return data.ID === currencyCode
    })
    console.log(currency);
    this.setState({CurrencyCode: currency[0].Abbrev, CurrencyId: currencyCode})
   await axios.get(apiUrl.BankService.GetTellerDetails  + localStorage.getItem("Id") + '/CurrCode/' + currency[0].CurrencyCode +  '/Token/' + localStorage.getItem('access_token'))
   .then(function(response){
     console.log(response.data.details);
     let tellerDetails = response.data.details
     if(tellerDetails.length > 0){
       IsTellerValid = true;
       currentComponent.setState({TellerDetails: response.data.details[0],IsTellerValid:true})
     }else{
      IsTellerValid = false;
      currentComponent.setState({IsTellerValid:false})
     }
   }).catch(function(error){
     currentComponent.setState({IsTellerValid:false})
     console.log(error);
   })
   return IsTellerValid
  }else{
    return IsTellerValid
  }
}

ConvertDate(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();
  today = yyyy + '-' + mm + '-' + dd;
  console.log(today);
  return today
}

onClickConfirm =e =>{
  let currentComponent = this
  currentComponent.setState({ShowError:false, ErrorMessage: '',TransferTillExecuting:true,DisableButton:true})
  let url = "";
  if(this.state.IsVaultIn){
    url = apiUrl.BankService.CreateVaultIn
  }else{
    url = apiUrl.BankService.CreateVaultOut
  }
  console.log(this.state.ApiData)

  //axios.post(apiUrl.BankService.TransferTill,currentComponent.state.ApiData)
  axios.post(apiUrl.Management.RequestTill,currentComponent.state.ApiData)
  .then(function(response){
    console.log(response.data);
    if(response.data.success === true){
      currentComponent.setState({CIFteller:'', CIFtoTeller:'', BranchCode:'',Remarks:'',Amount:'0.00', TransferTillExecuting:false,DisableButton:false})
      window.$('#confirmationModal').modal('toggle');
      window.$('#TransactionSuccessModal').modal('toggle')
    // NotificationManager.success('Transaction succssful', 'Success',2000)
    }else{
      currentComponent.setState({TransferTillExecuting:false,DisableButton:false, ErrorMessage:response.data.message})
    }
  }).catch(function(error){
    NotificationManager.error('Error Ocurred', 'Error',3500)
    currentComponent.setState({TransferTillExecuting:false,DisableButton:false, ErrorMessage:"Server Error"})
    console.log(error);
  })
}

SendRequest = async e =>{
  let currentComponent = this;
  let CIFtoTeller = this.state.CIFtoTeller + "";
  if(this.state.CurrencyCode === ''){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({DisableButton:false,ShowError:true, ErrorMessage: 'Please select a currency for the amount.',VaultExecuting:false})
    return;
  }
  if(this.state.IsTellerValid === false){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({DisableButton:false, ShowError:true, ErrorMessage: 'The login user is not yet assigned CIF number. Please contact the admin',VaultExecuting:false})
    return;
  }
  console.log(this.state.TellerDetails)
  let branchCode = this.state.TellerDetails.BRANCH_CODE + "";
  branchCode = branchCode.padStart(4,'0') 
  let tellerCIF = this.state.TellerDetails.CIF_NO + "";
  tellerCIF = tellerCIF.padStart(8,'0') 
  currentComponent.setState({ShowError:false, ErrorMessage: ''})
  if(Object.keys(this.state.UserTillDetails).length === 0){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Session timeout. Please Lock screen or Login again.',TransferTillExecuting:false})
    return;
  }
  if(CIFtoTeller === ''){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Please input To Teller CIF number',TransferTillExecuting:false})
    return;
  }
  if(CIFtoTeller.padStart(8,'0').length > 8){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'To Teller CIF number exceeds more than 8 digit. Please input not more than 8 digit To Teller CIF number.',TransferTillExecuting:false})
    return;
  }
  if(this.state.CurrencyCode === ''){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Please select a currency for the amount.',TransferTillExecuting:false})
    return;
  }

 
  if(this.state.IsTellerValid === false){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({DisableButton:false,ShowError:true, ErrorMessage: 'Operating Teller does not have CIF number for this currency. Please contact the admin',VaultExecuting:false})
    return;
  }
  if(this.state.Amount === '0.00'){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Please input an amount.',TransferTillExecuting:false})
    return;
  }
  if(this.state.Remarks === ''){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Please Remarks must not be empty.',TransferTillExecuting:false})
    return;
  }
  if(this.state.Remarks.length > 34){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Please Remarks character must not be graeter than 34 character',TransferTillExecuting:false})
    return;
  }

  let data = {GiverTillNo:tellerCIF,GiverUser:this.state.UserTillDetails.UserName ,ReceiverTillNo :CIFtoTeller.padStart(8,'0'), ReceiverUser :CIFtoTeller.padStart(8,'0') ,CurrencyCode:  parseInt(this.state.CurrencyId),Amount: parseFloat(this.state.Amount), CashierID:branchCode, TransferMode:0, Branch: this.state.UserTillDetails.UserTillBranch,CBA:"IMAL" }

  //  let data = {"TransRef":"","PhoneNo":"","TransName":this.state.UserDetails.UserName, "AccountNo":"","Amount":  parseFloat(this.state.Amount),
  // "TransType": "1","TellerId": tellerCIF,"CustomerAcctNos": "","TotalAmt": parseFloat(this.state.Amount),
  // "WithdrawerName": this.state.UserDetails.UserName,"WithdrawerPhoneNo": "","Status": 64,"CashierID": localStorage.getItem('Id'),
  // "CashierTillNos": this.state.UserDetails.Teller_ID,"CashierTillGL": "","WhenApproved": currentComponent.ConvertDate(new Date()) ,
  // "SortCode": "","Currency": this.state.CurrencyId,"ValueDate": currentComponent.ConvertDate(new Date()) ,"SupervisoryUser": "",
  // "ChequeNo": "","DateOnCheque": currentComponent.ConvertDate(new Date()),"Remark":this.state.Remarks, 
  // "Narration": this.state.Remarks,"CreationDate":  currentComponent.ConvertDate(new Date()) ,"MachineName": "",
  // "CBAResponse":"",CBACode:"",TransactionParty:"","GLAccountNo": "","CBA": "T24","TillTransferID":0,"IsTillTransfer":true,
  // "ApprovedBy": "","NeededApproval":false, "InitiatorName": localStorage.getItem("UserName"), "IsT24": false,
  // "Branch": this.state.UserDetails.UserTillBranch, "CurrCode": this.state.CurrencyCode, "ToTellerId":CIFtoTeller.padStart(8,'0'), 
  // DisapprovalReason:"",DisapprovedBy:"",WhenDisapproved:"","CurrencyAbbrev": this.state.CurrencyCode,
  // "access_token":localStorage.getItem('access_token'),BranchCode:branchCode,"TransacterEmail": "",
  // "IsReversed": false,"AccountName":"",
  //  "ReversedTranId": 0,  TransactionDetailsModels:[]}
   this.setState({ApiData: data});
   window.$('#confirmationModal').modal('toggle');
  
}

GetDate(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();

  today = dd  + '/' + mm + '/' + yyyy;
  console.log(today);
  return today
}

  async componentDidMount(){
    let currentComponent = this;
    loadAmountFormat();
    await axios.get(apiUrl.Security.GetCurrency)
    .then(function(response){
      let getDenominations = response.data.data;
      console.log(getDenominations);
      currentComponent.setState({CashDenominations:getDenominations})
    })
  
   await axios.get(apiUrl.BankService.GetUserDetails + 'userId=' + localStorage.getItem("Id") +  '&access_token=' + localStorage.getItem('access_token'))
    .then(function(response){
      console.log(response.data);
      if(response.data.success){
        currentComponent.setState({UserTillDetails: response.data.UserDetails})
      }
      
    }).catch(function(error){
      console.log(error);
    })

  }
  render(){
    return (
      <div>
      <NotificationContainer/>
      <section id="main-content">
      <section className="wrapper">
        <div className="row">
        <div className="col-md-12">
        <h3 className="page-title">
        Transfer Till <small></small>
        </h3>
        </div>
        </div>
        <div className="row">
     
        </div><br/> <br/>
        {
          this.state.ShowError === true ? <div  class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
              <b>Close</b></button>
          <span class="glyphicon glyphicon-hand-right"></span> <strong>{this.state.ErrorMessage}</strong>  </div>: ''
        }
        
        <div className="row">
				<div className="col-md-12">
					<div className="portlet box grey-cascade">
            <div className="portlet-title">
            <div className="caption">
            <i className="fa fa-user"></i> Requester
          </div>
          <div className="tools">
            <a href="javascript:;" className="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" className="config">
            </a>
            <a href="javascript:;" className="reload">
            </a>
            <a href="javascript:;" className="remove">
            </a>
          </div>
            </div>
            <div className="portlet-body" style={{height:'325px'}}>
              <div className="table-toolbar">
              <div style={{overflow:'scroll', height:'300px'}}>
								<div className="row">
									<div className="col-md-6">
                  <div className="portlet-body form">
                  <div className="form-body">
                  <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                    <input onChange={this.ChangeHandler} value={this.state.CIFtoTeller}    type="number" name="CIFtoTeller" class="form-control edited"/>
                    <label for="form_control_1"><b>Teller CIF Number</b></label>
                    <span style={{fontSize:'12px'}} className="help-block">Enter teller CIF number you want to send money to..</span>
                    <i className="fa fa-key"></i>
                  </div>
                </div>
              <div className="form-group form-md-line-input has-success form-md-floating-label">
              <div className="input-icon right">
                <input data-type="currency" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" style={{textAlign:'right'}} onChange={this.ChangeAmount}  defaultValue={'0.00'} type="text" name="Amount" class="form-control edited"/>
                <label for="form_control_1"><b>Amount</b></label>
                <span style={{fontSize:'12px'}} className="help-block">Enter the amount you want to transfer ..</span>
                <i className="fa fa-money"></i>
              </div>
            </div>
                  </div>
                  </div>
                  </div>
                  <div className="col-md-6">
                  <div className="portlet-body form">
                  <div className="form-body">
                  <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                  <select className="form-control edited" onClick={this.ChangeCurrency}>
                  <option id="0" value={0} disabled selected={true}>---Select Currency---</option>
                  {
                    this.state.CashDenominations.map(x=>
                      <option  value={x.ID}>{x.Currency}</option>
                      )
                  }
                  </select>
                  <label for="form_control_1"><b>Currency</b></label>
                  <span style={{fontSize:'12px'}} className="help-block">Select the currency of the amount stipulated..</span>
                </div>
                </div>
                <div className="form-group form-md-line-input has-success form-md-floating-label">
              <div className="input-icon right">
                <input  onChange={this.ChangeRemarks}  type="text" name="Remarks" class="form-control edited"/>
                <label for="form_control_1"><b>Remarks</b></label>
                <span style={{fontSize:'12px'}} className="help-block">Enter any additional information about vault out transaction..</span>
                <i className="fa fa-file"></i>
              </div>
            </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
            </div>
            </div>
            </div>
            <div style={{paddingLeft:'0px', textAlign:'right'}}><button onClick={this.ClearState} type="button" className="btn btn-default" data-dismiss="modal">Clear</button><button disabled={this.state.DisableRequest} onClick={this.SendRequest} type="button" className="btn btn-primary"><i class="fa fa-send"></i> Transfer </button></div>
        </section>
        </section>

        <div className="modal fade" id="TransactionSuccessModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content" style={{width:'520px'}}>
            <div style={{background:'rgb(22 180 27)'}} className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             <h4 className="modal-title" id="myModalLabel">Transaction Successful</h4>
            </div>
            <div className="modal-body">
            <div className="row">
            <div className="col-md-12 ">
            <div className="portlet-body form">
              <div className="form-body">
              <p><b>Transaction successful awaiting approval from the teller id sent to </b></p>
              </div>
              </div>
            </div>
      
            </div>
            </div>
            <div className="modal-footer">
            <button type="button" className="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
          </div>
        </div>
      </div>

      <div aria-hidden="true" aria-labelledby="myModalLabel" id="confirmationModal" role="dialog" tabindex="-1"  class="modal fade">
<div class="modal-dialog" style={{width:'350px'}}>
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4  style={{color:'white'}} className="modal-title" id="myModalLabel">Confirmation</h4>
    </div>
    <div class="modal-body">
      <p>Are you sure you want to proced with this transaction?</p>
    </div>
    <div class="modal-footer">
      <button  data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
      <button disabled={this.state.DisableButton} onClick={this.onClickConfirm} class="btn btn-theme" type="button">Submit {this.state.TransferTillExecuting === true ? <i id="spinner" class="fa fa-spinner fa-spin"></i> : ''}</button>
    </div>
  </div>
</div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" id="errorModal" role="dialog" tabindex="-1"  class="modal fade">
<div class="modal-dialog" style={{width:'400px'}}>
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4  style={{color:'white'}} className="modal-title" id="myModalLabel">Error</h4>
    </div>
    <div class="modal-body">
      <p><i className="fa fa-exclamation-triangle"></i>   {this.state.ErrorMessage}</p>
    </div>
    <div class="modal-footer">
      <button  data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
    </div>
  </div>
</div>
</div>

      </div>
    )
  }
}

export default TransferTill