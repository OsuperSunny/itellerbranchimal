import React, {Component} from "react";
import MockJson from '../../apiService/mockJson';
import Select from 'react-select';
import axios from 'axios';
import apiUrl from '../../apiService/config'
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import GenerateToken from '../../shared/token'
var Till =[];
const userName = localStorage.getItem('Id')
class OpenTill extends Component {
  constructor (props){
    super(props)
    this.state = {
    ID:0,
    UserId:'',
    ShowError: false,
    ErrorMessage:'',
    UserDetails:{},
    UserIdErrorMessage:'',
    UserName:'',
    Teller_ID:'',
    Time_Opened:'',
    Comment:'',
    OpenTillExecuting: false
}
}

ChangeUser =  e => {
  let currentComponent = this;
 
  currentComponent.setState({UserIdErrorMessage:'', UserDetails:{}})
  if(e.target.value.length === 0){
    console.log(e.target.value.length);
    currentComponent.setState({UserIdErrorMessage:'', UserName:'',Teller_ID:'',Time_Opened:''})
  }else{
   axios.get(apiUrl.BankService.GetUserDetails + 'userId=' + e.target.value +  '&access_token=' + localStorage.getItem('access_token'))
    .then(function(response){
      console.log(response.data);
      if(response.data.success){
        currentComponent.setState({UserDetails: response.data.UserDetails, UserName: response.data.UserDetails.UserName, Teller_ID:response.data.UserDetails.Teller_ID,Time_Opened:response.data.UserDetails.Time_Opened})
      }else{
        currentComponent.setState({UserIdErrorMessage:'* User ID is invalid', UserName:'',Teller_ID:'',Time_Opened:''})
      }
      
    }).catch(function(error){
      console.log(error);
      currentComponent.setState({UserIdErrorMessage:'* User ID is invalid', UserName:'',Teller_ID:'',Time_Opened:''})
    })
  }

}

ChangeComment = e => {
  this.setState({[e.target.name]: e.target.value})
}

ClearState = e => {
  this.setState({   WithdrawalAmount:0.00,
    TillNum:'',
    GLAccount:'',
    CurrencyCode:0,
    UserId:''})
    this.setState({ShowError: false, ErrorMessage:''})
}
ConvertDate(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();
  today = yyyy + '-' + mm + '-' + dd;
  console.log(today);
  return today
}
SendRequest = async e =>{
  let currentComponent = this;
  currentComponent.setState({ShowError:false, ErrorMessage: '',OpenTillExecuting:true})
  if(this.state.Teller_ID === ''){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Teller ID not found.', OpenTillExecuting:false})
    return;
  }
  if(this.state.Comment === ''){
    window.$("html, body").animate({ scrollTop: 0 }, "slow");
    currentComponent.setState({ShowError:true, ErrorMessage: 'Please input comment.', OpenTillExecuting:false})
    return;
  }
  
  let data = {
     "Id": 0,"TransactionBranch": this.state.UserDetails.UserTillBranch,"TellerId": this.state.Teller_ID,"Status": "OPEN","Comments": this.state.Comment,"IsHeadTeller": "NO","User": this.state.UserDetails.User,"ApprovedBy": localStorage.getItem('Id'), "ApprovedDate":  currentComponent.ConvertDate(new Date()),"DateCreated":  currentComponent.ConvertDate(new Date()),"IsClosed": false, "Event": this.state.UserDetails.UserName + " opened till","CBAResponse": ""
  }
  console.log(data)
  await axios.post(apiUrl.BankService.OpenTill, data)
  .then(function(response){
    if(response.data.success){
      NotificationManager.success('Success', 'Till opened successfully')
      currentComponent.setState({OpenTillExecuting:false,  UserName:'',Teller_ID:'',
      Time_Opened:'',Comment:''})
    }else{
      NotificationManager.error('Error', 'Till not opened')
    currentComponent.setState({OpenTillExecuting: false})
    console.log(response.data)
    }
  }).catch(function(error){
    NotificationManager.error('Error', 'Server Error')
    currentComponent.setState({OpenTillExecuting: false})
    console.log(error)
  })
}

  async componentDidMount(){
    let currentComponent = this;
    await GenerateToken();
    await axios.get(apiUrl.BankService.GetUserDetails + 'userId=' + localStorage.getItem('Id') +  '&access_token=' + localStorage.getItem('access_token'))
      .then(function(response){
        console.log(response.data);
        if(response.data.success){
          currentComponent.setState({UserDetails: response.data.UserDetails, UserName: response.data.UserDetails.UserName, Teller_ID:response.data.UserDetails.Teller_ID,Time_Opened:response.data.UserDetails.Time_Opened})
        }else{
           NotificationManager.error('Error', 'Session Expired. Please Login again', 1000)
        }
        
      }).catch(function(error){
        console.log(error);
        NotificationManager.error('Error', 'Server Error', 1000)
      })
  }
  render(){
    return (
      <div>
      <NotificationContainer/>
      <section id="main-content">
      <section className="wrapper">
        <div className="row">
        <div className="col-md-12">
        <h3 className="page-title">
        Open Till <small></small>
        </h3>
        </div>
        </div>
        <div className="row">
     
        </div><br/> <br/>
        {
          this.state.ShowError === true ? <div  class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
              <b>Close</b></button>
          <span class="glyphicon glyphicon-hand-right"></span> <strong>{this.state.ErrorMessage}</strong>  </div>: ''
        }
        
        <div className="row">
				<div className="col-md-12">
					<div className="portlet box grey-cascade">
            <div className="portlet-title">
            <div className="caption">
            <i className="fa fa-user"></i> User Till
          </div>
          <div className="tools">
            <a href="javascript:;" className="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" className="config">
            </a>
            <a href="javascript:;" className="reload">
            </a>
            <a href="javascript:;" className="remove">
            </a>
          </div>
            </div>
            <div className="portlet-body" style={{height:'325px'}}>
              <div className="table-toolbar">
              <div style={{overflow:'scroll', height:'300px'}}>
								<div className="row">
									<div className="col-md-6">
                  <div className="portlet-body form">
                  <div className="form-body">
                  <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                    <input disabled readOnly type="text" name="TillCode" value={localStorage.getItem('Id')} class="form-control edited"/>
                    <label for="form_control_1"><b>User Id</b></label>
                    <span style={{fontSize:'12px', color:'red'}} className="help-block"></span>
                    <i className="fa fa-key"></i>
                  </div>
                </div>
                <div className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                  <input disabled readOnly value={this.state.Teller_ID} type="text" class="form-control edited"/>
                  <label for="form_control_1"><b>Teller ID</b></label>
                  <i className="fa fa-user"></i>
                </div>
              </div>
              <div className="form-group form-md-line-input has-success form-md-floating-label">
              <div className="input-icon right">
                <textarea name="Comment" onChange={this.ChangeComment} value={this.state.Comment} type="text" class="form-control edited"/>
                <label for="form_control_1"><b>Comment</b></label>
                <i className="fa fa-file"></i>
              </div>
            </div>
                  </div>
                  </div>
                  </div>
                  <div className="col-md-6">
                  <div className="portlet-body form">
                  <div className="form-body">
                  <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                  <input disabled readOnly value={this.state.Time_Opened} type="text" class="form-control edited"/>
                  <label for="form_control_1"><b>Time Opened</b></label>
                  <i className="fa fa-clock-o"></i>
                </div>
                </div>
              <div className="form-group form-md-line-input has-success form-md-floating-label">
              <div className="input-icon right">
                <input readOnly disabled   value={this.state.UserName} type="text" name="Amount" class="form-control edited"/>
                <label for="form_control_1"><b>Name</b></label>
                <i className="fa fa-user"></i>
              </div>
            </div>
              
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
            </div>
            </div>
            </div>
            <div style={{paddingLeft:'0px', textAlign:'right'}}><button onClick={this.ClearState} type="button" className="btn btn-default" data-dismiss="modal">Clear</button><button onClick={this.SendRequest} type="button" className="btn btn-primary"><i class="fa fa-folder-open"></i> Open {this.state.OpenTillExecuting === true ? <i id="spinner" class="fa fa-spinner fa-spin"></i> : ''}</button></div>
        </section>
        </section>

      </div>
    )
  }
}

export default OpenTill