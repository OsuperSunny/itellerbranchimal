import React, {Component} from "react";
import MockJson from '../../apiService/mockJson';
import axios from 'axios';
import apiUrl from '../../apiService/config'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import GenerateToken from '../../shared/token'
import $ from 'jquery';
import DataTable from '../../shared/dataTable'
import pdfGeneration from "../../shared/pdfGeneration";
import GenerateExcel from '../../shared/generateExcel'

const userName = localStorage.getItem('Id')
const ExampleCustomInput = ({ value, onClick }) => (
  <input className="form-control label-success" onClick={onClick} value={value} style={{width:'150px'}}/>
);
function loadAmountFormat(){
  $("input[data-type='currency']").on({
    keyup: function() {
      console.log("exec")
      formatCurrency($(this));
    },
    blur: function() { 
      console.log("exec")
      formatCurrency($(this), "blur");
    }
});


function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val =  left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val =  input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ".00";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}  
}

class TransactionReport extends Component {
  constructor (props){
    super(props)
    this.state = {
    WithdrawalAmount:0.00,
    AmountTobePaid:'0.00',
    startDate: new Date(),
    endDate: new Date(),
    Transaction: [],
    transferPDFdetails:[],
    columns:[
      { title: 'DateTreated' ,width: {wpx: 150}}, 
      { title: 'TransactionReference',width: {wpx: 130} },
      { title: 'TellerId',width: {wpx: 120} },
      { title: 'TransacterName' ,width: {wpx: 150}},
      { title: 'AccountNumber' ,width: {wpx: 120}},
      { title: 'Narration' ,width: {wpx: 150}},
      { title: 'TransTypeName' ,width: {wpx: 90}},
      { title: 'Status' ,width: {wpx: 90}},
      { title: 'ApprovedBy' ,width: {wpx: 120}},
      { title: 'DisapprovedBy' ,width: {wpx: 120}},
      { title: 'Currency' ,width: {wpx: 90}},
      { title: 'TotalAmount' ,width: {wpx: 100}}]
}
}

GeneratePDF = e => {
  let currentComponent = this;
 
  const headers = [
    { text: 'DateTreated', style: 'tableHeader' },
    { text: 'Transaction Reference', style: 'tableHeader' },
    { text: 'TellerID', style: 'tableHeader' },
    { text: 'TransacterName', style: 'tableHeader' },
    { text: 'AccountNumber', style: 'tableHeader' },
    { text: 'Narration', style: 'tableHeader' },
    { text: 'TransactionType', style: 'tableHeader' },
    { text: 'ApprovalStatus', style: 'tableHeader' },
    { text: 'ApprovalBy', style: 'tableHeader' },
    { text: 'DisapprovedBy', style: 'tableHeader' },
    { text: 'Currency', style: 'tableHeader' },
    { text: 'Amount', style: 'tableHeader' }];


  pdfGeneration(currentComponent.state.transferPDFdetails, headers, 'transactionReport');
 
}
ClearState = e => {
  this.setState({ CashWithDrawal:{}})
}

GenerateFileData(transaction){
  let currentComponent = this;
    let transferPDFdetails = [];
  transaction.forEach(function(element){
    let transactiondata = {DateTreated: currentComponent.ConvertDateTime(element.CreationDate),TransactionReference:element.TransRef, TellerId: element.TellerId,TransacterName: element.TransacterName,AccountNumber: element.AccountNumber, 
      Narration: element.Narration, TransTypeName: element.TransTypeName,ApprovalStatus: element.Status === 1 ? 'Pending' : element.Status === 2 ? "Approved" : "Disapproved",ApprovedBy: element.ApprovedBy,DisapprovedBy: element.DisapprovedBy,Currency: element.Currency,
      TotalAmount: element.TotalAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
      transferPDFdetails.push(transactiondata);
  });
  this.setState({transferPDFdetails});
}
async setStartDate(date){
  console.log(date);
  let currentComponent = this;
 this.setState({ startDate: date})
 let startDate = currentComponent.ConvertDate(date);
  let endDate = currentComponent.ConvertDate(currentComponent.state.endDate);
  await axios.get(apiUrl.Report.Transaction + 'From/' + startDate + '/To/' + endDate)
  .then(function(response){
    console.log(response.data)
    let transaction = response.data.Transaction.sort((a, b) => b.CreationDate - a.CreationDate)
    if(!localStorage.getItem("Branch").includes("ALL")){
      transaction = transaction.filter(function(element){
        return element.BranchCode === localStorage.getItem("Branch")
   })
  }
      currentComponent.setState({Transaction: transaction})
      currentComponent.GenerateFileData(transaction)
  })
}

setEndDate(date){
  console.log(date);
 this.setState({ endDate: date})
}

GetDate(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();

  today = dd  + '/' + mm + '/' + yyyy;
  console.log(today);
  return today
}

ConvertDate(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();
  today = yyyy + '-' + mm + '-' + dd;
  console.log(today);
  return today
}

ConvertDateTime(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();
  var hours = today.getHours();
  var min = today.getMinutes();
  if(min === 0){
    min = "00"
  }
  let minute = "" + min;
  if(minute.length  === 1){
    min = "0" + min
  }
  today = dd + '/' + mm + '/' + yyyy + " " +  hours + ':' + min;
  console.log(today);
  return today
}

GetDateForAPI(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();
   var hours = today.getHours();
  var min = today.getMinutes();
  var sec = today.getSeconds();
  today = yyyy + '-' + mm + '-' + dd + 'T' + hours + ':' + min + ':' + sec + '+01:00';
  console.log(today);
  return today
}

  async componentDidMount(){
    loadAmountFormat();
   await GenerateToken();
    let currentComponent = this;
    let startDate = currentComponent.ConvertDate(currentComponent.state.startDate);
  let endDate = currentComponent.ConvertDate(currentComponent.state.endDate);
    await axios.get(apiUrl.Report.Transaction + 'From/' + startDate + '/To/' + endDate)
    .then(function(response){
      console.log(response.data)
      let transaction = response.data.Transaction.filter(function(data){
        return data.CBA === "IMAL"
     })

      if(!localStorage.getItem("Branch").includes("ALL")){
        transaction = transaction.filter(function(element){
          return element.BranchCode === localStorage.getItem("Branch")
     })
     currentComponent.setState({Transaction: transaction,OriginalTransaction: transaction})
    }else{
      currentComponent.setState({Transaction: transaction, OriginalTransaction: transaction})
    }
       
        currentComponent.GenerateFileData(transaction)
    })
    window.$('#sample_1').DataTable( {
      "language": {
        "zeroRecords": " " //Change your default empty table message
        },
      retrieve: true,
      paging: true
    });
  }
  render(){
    return (
       <div>
       <NotificationContainer/>
      <section id="main-content">
      <section className="wrapper">
        <div className="row">
        <div className="col-md-12">
        <h3 className="page-title">
        Transaction Report <small></small>
        </h3>
        </div>
        </div>
        {
          this.state.ShowError === true ? <div  class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
              <b>Close</b></button>
          <span class="glyphicon glyphicon-hand-right"></span> <strong>{this.state.ErrorMessage}</strong>  </div>: ''
        }
        <div className="row">
				<div className="col-md-12">
					<div className="portlet box grey-cascade">
            <div className="portlet-title">
            <div className="caption">
            <i className="fa fa-info"></i> 
          </div>
          <div className="tools">
            <a href="javascript:;" className="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" className="config">
            </a>
            <a href="javascript:;" className="reload">
            </a>
            <a href="javascript:;" className="remove">
            </a>
          </div>
            </div>
            <div className="portlet-body" style={{height:'250px'}}>
              <div className="table-toolbar">
              <div style={{overflow:'scroll', height:'230px'}}>
								<div className="row">
									<div className="col-md-6">
                  <div className="portlet-body form">
                  <div className="form-body">
                  <div style={{paddingTop:'1px'}} className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                  <label for="form_control_1"><b style={{color:'#3c763d'}}>Date From</b></label>
                  <DatePicker
                  dateFormat="dd/MM/yyyy"
                  selected={this.state.startDate}
                  onChange={date => this.setStartDate(date)}
                  customInput={<ExampleCustomInput/>}
                />
                  </div>
                </div>
                  <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                    <input defaultValue={this.state.AccountNumber} onChange={this.onChangeOfAccountNumber} type="number" className="form-control edited"/>
                    <label for="form_control_1"><b>Teller ID</b></label>
                    <span style={{fontSize:'12px', color:this.state.AccountFieldColor}} className="help-block">{this.state.AccountFieldMessage}</span>
                    <i className="fa fa-key"></i>
                  </div>
                </div>
                
                <label for="form_control_1"><b>Transaction Type</b></label>
                <div style={{overflow:'scroll',height: '80px'}}  className="form-group form-md-line-input has-success form-md-floating-label">
                    <div   className="checkbox-list col-sm-4">
                    <label>
                    <input  onChange={this.onclickResourceCheckbox}  type="checkbox"/>Withdrawal</label>
                  </div>
                  <div   className="checkbox-list col-sm-4">
                  <label>
                  <input  onChange={this.onclickResourceCheckbox}  type="checkbox"/>Deposit</label>
                </div>
              </div>

                  </div>
                  </div>
                  </div>

                  <div className="col-md-6">
                  <div className="portlet-body form">
                  <div className="form-body">
                  <div style={{paddingTop:'1px'}} className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                  <label for="form_control_1"><b style={{color:'#3c763d'}}>Date To</b></label>
                  <DatePicker
                  dateFormat="dd/MM/yyyy"
                  selected={this.state.endDate}
                  onChange={date => this.setEndDate(date)}
                  customInput={<ExampleCustomInput/>}
                />
                  </div>
                </div>
                <div className="form-group form-md-line-input has-success form-md-floating-label">
                  <label style={{paddingLeft:'0px'}} className="control-label col-md-3"><b>Amount Range</b></label>
                  <div className="col-md-4">
                    <div className="input-group input-large">
                      <input type="text" className="form-control edited" name="from"/>
                      <span className="input-group-addon">To</span>
                      <input type="text" className="form-control edited" name="to"/>
                    </div>
                    <span className="help-block">Select date range</span>
                  </div>
                </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
            </div>
            </div>
            <div className="col-md-12">
            <div className="portlet box grey-cascade">
              <div className="portlet-title">
              <div className="caption">
              <i className="fa fa-info"></i> Transaction Details
            </div>
            <div className="tools">
              <a href="javascript:;" className="collapse">
              </a>
              <a href="#portlet-config" data-toggle="modal" className="config">
              </a>
              <a href="javascript:;" className="reload">
              </a>
              <a href="javascript:;" className="remove">
              </a>
            </div>
              </div>
              <div className="portlet-body">
                <div className="table-toolbar">
                <div style={{overflow:'scroll', height:'290px'}}>
                  <table className="table table-striped table-bordered table-hover" id="sample_1">
                  <thead>
                  <tr>
                  <th>Date Treated</th>
                  <th>Transaction Reference</th>
                    <th>
                      Teller ID
                    </th>
                    <th>
                      User
                    </th>
                    <th>
                   Account Number
                   </th>
                   <th>
                 Narration
                  </th>
                  <th>Transaction Type</th>
                  <th>Approval Status</th>
                  <th>Approval By</th>
                  <th>Disapproved By</th>
                  <th>
                 Currency
                    </th>
                   <th>
                  Amount
                    </th>
                  </tr>
                  </thead>
                  <tbody>
                  {
                    this.state.Transaction.map(x=> 
                      <tr>
                      <td>{this.ConvertDateTime(x.CreationDate)}</td>
                    <td>{x.TransRef}</td>
                    <td>{x.TellerId}</td>
                    <td>{x.TransacterName}</td>
                    <td>{x.AccountNumber}</td>
                    <td>{x.Narration}</td>
                    <td>{x.TransTypeName}</td>
                    <td>{x.Status === 1 ? 'Pending' : x.Status === 2 ? "Approved" : "Disapproved" }</td>
                    <td>{x.ApprovedBy}</td>
                    <td>{x.DisapprovedBy}</td>
                    <td>{x.Currency}</td>
                   <td style={{textAlign:'right'}}>{x.TotalAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                    </tr>
                      
                      )
                    
                  }
                  </tbody>
                  </table>
                    </div>
                    </div>
                    </div>
              </div>
              </div><br/>

            </div>
            <div style={{paddingLeft:'0px', textAlign:'right'}}>
            <GenerateExcel data={this.state.transferPDFdetails} columns={this.state.columns} />
            <button style={{marginLeft:'5px'}} onClick={this.GeneratePDF} type="button" className="btn btn-primary"><i class="fa fa-file-pdf-o"></i> Generate PDF  {this.state.SaveTransExecuting === true ? <i id="spinner" class="fa fa-spinner fa-spin"></i> : ''}</button></div>
        </section>
        </section>

      </div>
    )
  }
}

export default TransactionReport