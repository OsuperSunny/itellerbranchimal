import React, { Component } from "react";
import MockJson from '../../apiService/mockJson';
import axios from 'axios';
import apiUrl from '../../apiService/config'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'react-notifications/lib/notifications.css';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import GenerateToken from '../../shared/token'
import GenerateExcel from '../../shared/generateExcel'
import $ from 'jquery';
import Datatable from "../../shared/dataTable";
import ReactExport from 'react-data-export';
import sterlingLogo from '../../sterling.png'
import { jsPDF } from "jspdf";
import pdfGeneration from "../../shared/pdfGeneration";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const userName = localStorage.getItem('Id')
const ExampleCustomInput = ({ value, onClick }) => (
  <input className="form-control label-success" onClick={onClick} value={value} style={{ width: '150px' }} />
);

// var generateData = function(amount) {
//   var result = [];
//   var data = {
//     coin: "100",
//     game_group: "GameGroup",
//     game_name: "XPTO2",
//     game_version: "25",
//     machine: "20485861",
//     vlt: "0"
//   };
//   for (var i = 0; i < amount; i += 1) {
//     data.id = (i + 1).toString();
//     result.push(Object.assign({}, data));
//   }
//   return result;
// };

function createHeaders(keys) {
  var result = [];
  for (var i = 0; i < keys.length; i += 1) {
    result.push({
      id: keys[i],
      name: keys[i],
      prompt: keys[i],
      width: 65,
      align: "center",
      padding: 0
    });
  }
  return result;
}



// var doc = new jsPDF({ putOnlyUsedFonts: true, orientation: "landscape" });
// doc.table(1, 1, generateData(100), headers, { autoSize: true });



function loadAmountFormat() {
  $("input[data-type='currency']").on({
    keyup: function () {
      console.log("exec")
      formatCurrency($(this));
    },
    blur: function () {
      console.log("exec")
      formatCurrency($(this), "blur");
    }
  });

  function formatNumber(n) {
    // format number 1000000 to 1,234,567
    return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
  }

  function formatCurrency(input, blur) {
    // appends $ to value, validates decimal side
    // and puts cursor back in right position.

    // get input value
    var input_val = input.val();

    // don't validate empty input
    if (input_val === "") { return; }

    // original length
    var original_len = input_val.length;

    // initial caret position 
    var caret_pos = input.prop("selectionStart");

    // check for decimal
    if (input_val.indexOf(".") >= 0) {

      // get position of first decimal
      // this prevents multiple decimals from
      // being entered
      var decimal_pos = input_val.indexOf(".");

      // split number by decimal point
      var left_side = input_val.substring(0, decimal_pos);
      var right_side = input_val.substring(decimal_pos);

      // add commas to left side of number
      left_side = formatNumber(left_side);

      // validate right side
      right_side = formatNumber(right_side);

      // On blur make sure 2 numbers after decimal
      if (blur === "blur") {
        right_side += "00";
      }

      // Limit decimal to only 2 digits
      right_side = right_side.substring(0, 2);

      // join number by .
      input_val = left_side + "." + right_side;

    } else {
      // no decimal entered
      // add commas to number
      // remove all non-digits
      input_val = formatNumber(input_val);
      input_val = input_val;

      // final formatting
      if (blur === "blur") {
        input_val += ".00";
      }
    }

    // send updated string to input
    input.val(input_val);

    // put caret back in the right position
    var updated_len = input_val.length;
    caret_pos = updated_len - original_len + caret_pos;
    input[0].setSelectionRange(caret_pos, caret_pos);
  }
}

class VaultReport extends Component {
  constructor(props) {
    super(props)
    this.state = {
      WithdrawalAmount: 0.00,
      AmountTobePaid: '0.00',
      startDate: new Date(),
      endDate: new Date(),
      Transaction: [],
      TellerFieldMessage: 'Filter by teller Id',
      TellerFieldColor: 'green',
      OriginalTransaction: [],
      IsVaultIn: false,
      IsVaultOut: false,
      selectedOption: '',
      // columns:['dateTreated','TellerName','tellerId','branch','toTellerId','currency','transactionType','amount'],
      columns:[
      { title: 'dateTreated' ,width: {wpx: 80}}, { title: 'tellerId',width: {wpx: 80} },
      { title: 'TellerName',width: {wpx: 80} },
      { title: 'branch' ,width: {wpx: 80}},
      { title: 'toTellerId' ,width: {wpx: 80}},
      { title: 'currency' ,width: {wpx: 80}},
      { title: 'transactionType' ,width: {wpx: 80}},
      { title: 'amount' ,width: {wpx: 80}}],
      vaultPDFdetails: [],
      CashDenominations: [],
      Abbrev: ''
    }
  }

  onChangeOfTellerID = e => {
    let transaction = this.state.Transaction;
    this.setState({ TellerFieldColor: '', TellerFieldMessage: '' })
    transaction = transaction.filter(function (data) {
      return data.TellerId === e.target.value;
    })
    if (transaction.length <= 0) {
      this.setState({ TellerFieldColor: 'red', TellerFieldMessage: 'No record found for teller' })
    } else {
      this.setState({ Transaction: transaction, TellerFieldColor: '', TellerFieldMessage: '' });
    }
    let vaultDetails = [];
    let vaultPDFdetails = [];
    transaction.forEach(function (element) {
      let data = [];
      let vauldata = {
        tellerId: element.TellerId, tellerName: element.TellerName, toTellerId: element.ToTellerId, currency: element.Currency,
        transactionType: element.IsVaultIn === true ? "VaultIn" : "VaultOut",
        amount: element.TotalAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
      }
      let tellerId = { value: element.TellerId, style: { font: { sz: "18" } } };
      let TellerName = { value: element.TellerName, style: { font: { sz: "18" } } };
      let branch = { value: element.Branch, style: { font: { sz: "18" } } };
      let toTellerId = { value: element.ToTellerId, style: { font: { sz: "18" } } };
      let currency = { value: element.Currency, style: { font: { sz: "18" } } };
      let transactionType = { value: element.IsVaultIn === true ? "VaultIn" : "VaultOut", style: { font: { sz: "18" } } };
      let amount = {
        value: element.TotalAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
        style: { font: { sz: "18" }, alignment: { horizontal: 'right' } }
      };
      data.push(tellerId);
      data.push(TellerName);
      data.push(branch);
      data.push(toTellerId);
      data.push(currency);
      data.push(transactionType);
      data.push(amount);
      vaultDetails.push(data);
      vaultPDFdetails.push(vauldata);
    });
    this.setState({ vaultPDFdetails });
   
  }

  ChangeCurrency = e => {
    console.log(e.target.value);
    let abbrev = e.target.value
    //let currencyId = parseInt(e.target.value);
    this.setState({ Abbrev: e.target.value })
    let transaction = this.state.OriginalTransaction;
    transaction = transaction.filter(function (data) {
      return data.Currency === abbrev
    })
    this.setState({ Transaction: transaction });
    this.GenerateFileData(transaction);
  }

  onclickVaultInCheckbox = e => {
    let transaction = this.state.Transaction;
    console.log(e.target.checked)
    if (e.target.checked) {
      if (this.state.IsVaultOut === true) {
        transaction = this.state.OriginalTransaction;
        this.setState({ Transaction: transaction, IsVaultIn: true })
      } else {
        transaction = transaction.filter(function (data) {
          return data.IsVaultIn === true;
        })
        this.setState({ Transaction: transaction, IsVaultIn: true })
      }
    } else {
      if (this.state.IsVaultOut === true) {
        transaction = transaction.filter(function (data) {
          return data.IsVaultIn === false;
        })
        this.setState({ Transaction: transaction, IsVaultIn: false })
      } else {
        transaction = this.state.OriginalTransaction;
        this.setState({ Transaction: transaction, IsVaultIn: false })
      }
    }
    this.GenerateFileData(transaction);

  }

  onclickVaultOutCheckbox = e => {
    let transaction = this.state.Transaction;
    if (e.target.checked) {
      if (this.state.IsVaultIn === true) {
        transaction = this.state.OriginalTransaction;
        this.setState({ Transaction: transaction, IsVaultOut: true })
      } else {
        transaction = transaction.filter(function (data) {
          return data.IsVaultIn === false;
        })
        this.setState({ Transaction: transaction, IsVaultOut: true })
      }
    } else {
      if (this.state.IsVaultIn === true) {
        transaction = transaction.filter(function (data) {
          return data.IsVaultIn === true;
        })
        this.setState({ Transaction: transaction, IsVaultOut: false })
      } else {
        transaction = this.state.OriginalTransaction;
        this.setState({ Transaction: transaction, IsVaultOut: false })
      }
    }
    this.GenerateFileData(transaction);
  }

  GenerateFileData(transaction) {
    let vaultDetails = [];
    let vaultPDFdetails = [];
    let currentComponent = this;
    transaction.forEach(function (element) {
      let data = [];
      let vauldata = {
        DateTreated: currentComponent.ConvertDateTime(element.CreationDate), TransactionReference: element.TransRef, tellerId: element.TellerId, tellerName: element.TellerName, toTellerId: element.ToTellerId, currency: element.Currency,
        transactionType: element.IsVaultIn === true ? "VaultIn" : "VaultOut",
        amount: element.TotalAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
      }
      let dateTreated = { value: currentComponent.ConvertDateTime(element.CreationDate), style: { font: { sz: "18" } } };
      let transRef = { value: element.TransRef, style: { font: { sz: "18" } } };
      let tellerId = { value: element.TellerId, style: { font: { sz: "18" } } };
      let TellerName = { value: element.TellerName, style: { font: { sz: "18" } } };
      let branch = { value: element.Branch, style: { font: { sz: "18" } } };
      let toTellerId = { value: element.ToTellerId, style: { font: { sz: "18" } } };
      let currency = { value: element.Currency, style: { font: { sz: "18" } } };
      let transactionType = { value: element.IsVaultIn === true ? "VaultIn" : "VaultOut", style: { font: { sz: "18" } } };
      let amount = {
        value: element.TotalAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
        style: { font: { sz: "18" }, alignment: { horizontal: 'right' } }
      };
      data.push(dateTreated)
      data.push(transRef)
      data.push(tellerId);
      data.push(TellerName);
      data.push(branch);
      data.push(toTellerId);
      data.push(currency);
      data.push(transactionType);
      data.push(amount);
      vaultDetails.push(data);
      vaultPDFdetails.push(vauldata);
    });
    this.setState({ vaultPDFdetails });
    
  }

  ClearState = e => {
    this.setState({ CashWithDrawal: {} })
  }

  async setStartDate(date) {
    console.log(date);
    let currentComponent = this;
    this.setState({ startDate: date })
    let startDate = currentComponent.ConvertDate(date);
    let endDate = currentComponent.ConvertDate(currentComponent.state.endDate);
    await axios.get(apiUrl.Report.VaultTransaction + 'From/' + startDate + '/To/' + endDate)
      .then(function (response) {
        console.log(response.data)
        let transaction = response.data.VaultTransaction.filter(function(data){
          return data.CBA === "IMAL"
       })
        if(!localStorage.getItem("Branch").includes("ALL")){
          transaction = transaction.filter(function(element){
            return element.BranchCode === localStorage.getItem("Branch")
       })
      }
        currentComponent.setState({ Transaction: transaction, OriginalTransaction: transaction })
        currentComponent.GenerateFileData(transaction);
      })
  }

  async setEndDate(date) {
    let currentComponent = this;
    console.log(date);
    let startDate = currentComponent.ConvertDate(currentComponent.state.startDate);
    let endDate = currentComponent.ConvertDate(date);
    await axios.get(apiUrl.Report.VaultTransaction + 'From/' + startDate + '/To/' + endDate)
      .then(function (response) {
        console.log(response.data)
        let transaction = response.data.VaultTransaction.filter(function(data){
          return data.CBA === "IMAL"
       })
        if(!localStorage.getItem("Branch").includes("ALL")){
          transaction = transaction.filter(function(element){
            return element.BranchCode === localStorage.getItem("Branch")
       })
      }
        currentComponent.setState({ Transaction: transaction, OriginalTransaction: transaction })
        currentComponent.GenerateFileData(transaction);
      })

    //   window.$('#sample_1').DataTable( {
    //     destroy: true,
    //     searching: false
    // } );
    window.$('#sample_1').DataTable({
      "language": {
        "zeroRecords": " " //Change your default empty table message
      },
      retrieve: true,
      paging: true
    });
    this.setState({ endDate: date })
  }



  GetDate(date) {
    var today = new Date(date);
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = dd + '/' + mm + '/' + yyyy;
    console.log(today);
    return today
  }

  ConvertDate(date) {
    var today = new Date(date);
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = yyyy + '-' + mm + '-' + dd;
    console.log(today);
    return today
  }

  ConvertDateTime(date) {
    var today = new Date(date);
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    var hours = today.getHours();
    var min = today.getMinutes();
    if (min === 0) {
      min = "00"
    }
    let minute = "" + min;
    if (minute.length === 1) {
      min = "0" + min
    }
    today = dd + '/' + mm + '/' + yyyy + " " + hours + ':' + min;
    console.log(today);
    return today
  }

  GetDateForAPI(date) {
    var today = new Date(date);
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    var hours = today.getHours();
    var min = today.getMinutes();
    var sec = today.getSeconds();
    today = yyyy + '-' + mm + '-' + dd + 'T' + hours + ':' + min + ':' + sec + '+01:00';
    return today
  }

  GeneratePDF = e => {
    let currentComponent = this;
    console.log(currentComponent.state.vaultPDFdetails);

    const headers = [
      { text: 'dateTreated', style: 'tableHeader' },
      { text: 'tellerId', style: 'tableHeader' },
      { text: 'TellerName', style: 'tableHeader' },
      { text: 'branch', style: 'tableHeader' },
      { text: 'toTellerId', style: 'tableHeader' },
      { text: 'currency', style: 'tableHeader' },
      { text: 'transactionType', style: 'tableHeader' },
      { text: 'amount', style: 'tableHeader' }];


    pdfGeneration(currentComponent.state.vaultPDFdetails, headers, 'vaultReport');
  
  }

  async componentDidMount() {
    loadAmountFormat();
   // this.Excel();
    let currentComponent = this;
    let startDate = currentComponent.ConvertDate(currentComponent.state.startDate);
    let endDate = currentComponent.ConvertDate(currentComponent.state.endDate);
    await axios.get(apiUrl.Report.VaultTransaction + 'From/' + startDate + '/To/' + endDate)
      .then(function (response) {
        console.log(response.data)
        let transaction = response.data.VaultTransaction.filter(function(data){
           return data.CBA === "IMAL"
        }).sort((a, b) => b.CreationDate - a.CreationDate)
        if(!localStorage.getItem("Branch").includes("ALL")){
          transaction = transaction.filter(function(element){
            return element.BranchCode === localStorage.getItem("Branch")
       })
      }
        currentComponent.setState({ Transaction: transaction, OriginalTransaction: transaction })
        // currentComponent.GenerateFileData(transaction);
      })
    window.$('#sample_1').DataTable({
      "language": {
        "zeroRecords": " " //Change your default empty table message
      },
      retrieve: true,
      paging: true
    });
    await axios.get(apiUrl.Security.GetCurrency)
      .then(function (response) {
        let getDenominations = response.data.data;
        console.log(getDenominations);
        currentComponent.setState({ CashDenominations: getDenominations })
      }) 
  }
  render() {
    const {state} = this;
    return (
      <div>
        <NotificationContainer />
        <section id="main-content">
          <section className="wrapper">
            <div className="row">
              <div className="col-md-12">
                <h3 className="page-title">
                  Vault Report <small></small>
                </h3>
              </div>
            </div>
            {
              this.state.ShowError === true ? <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                  <b>Close</b></button>
                <span class="glyphicon glyphicon-hand-right"></span> <strong>{this.state.ErrorMessage}</strong>  </div> : ''
            }
            <div className="row">
              <div className="col-md-12">
                <div className="portlet box grey-cascade">
                  <div className="portlet-title">
                    <div className="caption">
                      <i className="fa fa-info"></i>
                    </div>
                    <div className="tools">
                      <a href="javascript:;" className="collapse">
                      </a>
                      <a href="#portlet-config" data-toggle="modal" className="config">
                      </a>
                      <a href="javascript:;" className="reload">
                      </a>
                      <a href="javascript:;" className="remove">
                      </a>
                    </div>
                  </div>
                  <div className="portlet-body" style={{ height: '225px' }}>
                    <div className="table-toolbar">
                      <div style={{ overflow: 'scroll', height: '200px' }}>
                        <div className="row">
                          <div className="col-md-6">
                            <div className="portlet-body form">
                              <div className="form-body">
                                <div style={{ paddingTop: '1px' }} className="form-group form-md-line-input has-success form-md-floating-label">
                                  <div className="input-icon right">
                                    <label for="form_control_1"><b style={{ color: '#3c763d' }}>Date From</b></label>
                                    <DatePicker
                                      dateFormat="dd/MM/yyyy"
                                      selected={this.state.startDate}
                                      onChange={date => this.setStartDate(date)}
                                      customInput={<ExampleCustomInput />}
                                    />
                                  </div>
                                </div>
                                <div className="form-group form-md-line-input has-success form-md-floating-label">
                                  <div className="input-icon right">
                                    <input onChange={this.onChangeOfTellerID} type="text" className="form-control edited" />
                                    <label for="form_control_1"><b>Teller ID</b></label>
                                    <span style={{ fontSize: '12px', color: this.state.TellerFieldColor }} className="help-block">{this.state.TellerFieldMessage}</span>
                                    <i className="fa fa-key"></i>
                                  </div>
                                </div>

                                <div className="form-group form-md-line-input has-success form-md-floating-label">
                                  <div className="input-icon right">
                                    <select className="form-control edited" onClick={this.ChangeCurrency}>
                                      <option id="0" value={0} disabled selected={true}>---Select Currency---</option>
                                      {
                                        this.state.CashDenominations.map(x =>
                                          <option id={x.Currency} value={x.Abbrev}>{x.Currency}</option>
                                        )
                                      }
                                    </select>
                                    <label for="form_control_1"><b>Currency</b></label>
                                    <span style={{ fontSize: '12px' }} className="help-block">Select the currency of the amount stipulated..</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="col-md-6">
                            <div className="portlet-body form">
                              <div className="form-body">
                                <div style={{ paddingTop: '1px', marginBottom: '30px' }} className="form-group form-md-line-input has-success form-md-floating-label">
                                  <div className="input-icon right">
                                    <label for="form_control_1"><b style={{ color: '#3c763d' }}>Date To</b></label>
                                    <DatePicker
                                      dateFormat="dd/MM/yyyy"
                                      selected={this.state.endDate}
                                      onChange={date => this.setEndDate(date)}
                                      customInput={<ExampleCustomInput />}
                                    />
                                  </div>
                                </div>
                                <label for="form_control_1"><b>Transaction Type</b></label>
                                <div style={{ overflow: 'scroll', height: '60px', marginTop: '-15px' }} className="form-group form-md-line-input has-success form-md-floating-label">
                                  <div className="checkbox-list col-sm-4">
                                    <label>
                                      <input onChange={this.onclickVaultInCheckbox} type="checkbox" /> Vault In</label>
                                  </div>
                                  <div className="checkbox-list col-sm-4">
                                    <label>
                                      <input onChange={this.onclickVaultOutCheckbox} type="checkbox" /> Vault Out</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-12">
                <div className="portlet box grey-cascade">
                  <div className="portlet-title">
                    <div className="caption">
                      <i className="fa fa-info"></i> Vault Details
            </div>
                    <div className="tools">
                      <a href="javascript:;" className="collapse">
                      </a>
                      <a href="#portlet-config" data-toggle="modal" className="config">
                      </a>
                      <a href="javascript:;" className="reload">
                      </a>
                      <a href="javascript:;" className="remove">
                      </a>
                    </div>
                  </div>
                  <div className="portlet-body">
                    <div className="table-toolbar">
                      <div style={{ overflow: 'scroll', height: '290px' }}>
                        <table className="table table-striped table-bordered table-hover" id="sample_1">
                          <thead>
                            <tr>
                              <th>
                                Date Treated
                    </th>
                              <th style={{ width: '150px' }}>
                                Transaction Reference
                  </th>
                              <th>
                                Teller ID
                    </th>
                              <th>
                                Teller Name
                    </th>
                              <th>
                                Vault Teller ID
                    </th>
                              <th>
                                Currency
                   </th>
                              <th>Transaction Type</th>
                              <th>
                                Amount
                  </th>
                            </tr>
                          </thead>
                          <tbody>
                            {
                              this.state.Transaction.map(x =>
                                <tr>
                                  <td>{this.ConvertDateTime(x.CreationDate)}</td>
                                  <td>{x.TransRef}</td>
                                  <td>{x.TellerId}</td>
                                  <td>{x.TellerName}</td>
                                  <td>{x.ToTellerId}</td>
                                  <td>{x.Currency}</td>
                                  <td>{x.IsVaultIn === true ? 'Vault In' : 'Vault Out'}</td>
                                  <td style={{ textAlign: 'right' }}>{x.TotalAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                                </tr>

                              )
                            }
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div><br />

            </div>
            <div style={{ paddingLeft: '0px', textAlign: 'right' }}>
              <GenerateExcel data={state.vaultPDFdetails} columns={state.columns} />
               <button onClick={this.GeneratePDF} type="button" className="btn btn-primary"><i class="fa fa-file-pdf-o"></i> Generate PDF  </button>
               </div>

          </section>
        </section>



      </div>
    )
  }
}

export default VaultReport