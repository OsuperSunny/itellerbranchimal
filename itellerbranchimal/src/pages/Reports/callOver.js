import React, {Component} from "react";
import MockJson from '../../apiService/mockJson';
import axios from 'axios';
import apiUrl from '../../apiService/config'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import GenerateToken from '../../shared/token'
import $ from 'jquery';
import ReactExport from 'react-data-export';
import { jsPDF } from "jspdf";
import sterlingLogo from '../../sterling.png'


const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const userName = localStorage.getItem('Id')
const ExampleCustomInput = ({ value, onClick }) => (
  <input className="form-control label-success" onClick={onClick} value={value} style={{width:'150px'}}/>
);
function loadAmountFormat(){
  $("input[data-type='currency']").on({
    keyup: function() {
      console.log("exec")
      formatCurrency($(this));
    },
    blur: function() { 
      console.log("exec")
      formatCurrency($(this), "blur");
    }
});

function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val =  left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val =  input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ".00";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}  
}

function createHeaders(keys) {
  var result = [];
  for (var i = 0; i < keys.length; i += 1) {
    result.push({
      id: keys[i],
      name: keys[i],
      prompt: keys[i],
      width: 65,
      align: "center",
      padding: 0
    });
  }
  return result;
}

class CallOver extends Component {
  constructor (props){
    super(props)
    this.state = {
    WithdrawalAmount:0.00,
    AmountTobePaid:'0.00',
    startDate: new Date(),
    endDate: new Date(),
    Transaction:[],
    CashDenominations:[],
    transPDFdetails:[],
    OriginalTransaction:[]
}
}

onChangeOfTellerID = e =>{
  let transaction = this.state.Transaction;
  this.setState({TellerFieldColor: '', TellerFieldMessage:''})
  transaction = transaction.filter(function(data){
          return data.TellerId === e.target.value;
  })
  if(transaction.length <= 0){
    this.setState({TellerFieldColor: 'red', TellerFieldMessage:'No record found for teller'})
  }else{
    this.setState({Transaction: transaction,TellerFieldColor: '', TellerFieldMessage:'' });
  }
  this.GenerateFileData(transaction);
}

ChangeCurrency = e =>{
  console.log(e.target.value);
  let abbrev = e.target.value
  //let currencyId = parseInt(e.target.value);
  this.setState({Abbrev: e.target.value})
  let transaction = this.state.OriginalTransaction;
  transaction = transaction.filter(function(data){
     return  data.Currency === abbrev
  })
  this.setState({Transaction: transaction});
  this.GenerateFileData(transaction);
}

ChangeCBA = e =>{
  console.log(e.target.value);
  let cba = e.target.value
  //let currencyId = parseInt(e.target.value);
  this.setState({Abbrev: e.target.value})
  let transaction = this.state.OriginalTransaction;
  transaction = transaction.filter(function(data){
     return  data.CBA === cba
  })
  this.setState({Transaction: transaction});
  this.GenerateFileData(transaction);
}

GenerateFileData(transaction){
  let tillDetails = [];
  let transPDFdetails = [];
  let currentComponent = this;
  transaction.forEach(function(element){
    let data = [];
    let transdata = {DateTreated: currentComponent.ConvertDateTime(element.CreationDate),TransactionReference: element.TransRef, DebitAccount:element.IsWithdrawal ?   element.AccountNumber :element.BranchAccounts == null ? '' : element.BranchAccounts.DebitAccount,DebitCustomer:element.IsWithdrawal ? element.AccountName : 'Till Box',DebitAmount:element.TotalAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","), CreditAccount:element.IsDeposit ?   element.AccountNumber : element.BranchAccounts == null ? '' : element.BranchAccounts.CreditAccount,CreditCustomer: element.IsDeposit ? element.AccountName : 'Till Box',CreditAmount:element.TotalAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","), Inputter: element.TellerName, ApprovedBy: element.ApprovedBy,CBA:element.CBA, currency: element.Currency}
    let dateTreated = {value: currentComponent.ConvertDateTime(element.CreationDate)};
    let transRef = {value: element.TransRef};
    let DebitAccount = {value: element.IsWithdrawal ?   element.AccountNumber :element.BranchAccounts == null ? '' : element.BranchAccounts.DebitAccount};
    let DebitCustomer = {value: element.IsWithdrawal ? element.AccountName : 'Till Box'};
    let DebitAmount = {value: element.TotalAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")};
    let CreditAccount = {value: element.IsDeposit ?   element.AccountNumber : element.BranchAccounts == null ? '' : element.BranchAccounts.CreditAccount};
    let CreditCustomer = {value: element.IsDeposit ? element.AccountName : 'Till Box'}
    let CreditAmount= {value: element.TotalAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")};
    let TellerName = {value: element.TellerName};
    let ApprovedBy = {value: element.ApprovedBy};
    let CBA = {value: element.CBA};
    let currency = {value: element.Currency};
     data.push(dateTreated)
     data.push(transRef)
     data.push(DebitAccount)
     data.push(DebitCustomer)
     data.push(DebitAmount)
     data.push(CreditAccount)
     data.push(CreditCustomer)
     data.push(CreditAmount)
     data.push(TellerName)
    data.push(ApprovedBy);
    data.push(CBA);
    data.push(currency);
    tillDetails.push(data);
    transPDFdetails.push(transdata);
  });
  this.setState({transPDFdetails});
  this.ImportToExcel(tillDetails);
}

async setStartDate(date){
  console.log(date);
  let currentComponent = this;
 this.setState({ startDate: date})
 let startDate = currentComponent.ConvertDate(date);
  let endDate = currentComponent.ConvertDate(currentComponent.state.endDate);
 await axios.get(apiUrl.Report.CallOver + 'From/' + startDate + '/To/' + endDate)
  .then(function(response){
  console.log(response.data)
  let transaction = response.data.Transaction.filter(function(data){
    return data.IsWithdrawal === true || data.IsDeposit === true && data.CBA === "IMAL"
});
if(!localStorage.getItem("Branch").includes("ALL")){
transaction = transaction.filter(function(element){
  return element.BranchCode === localStorage.getItem("Branch")
})
}
currentComponent.setState({Transaction: transaction, OriginalTransaction: transaction})
   currentComponent.GenerateFileData(transaction);
  })
}

async setEndDate(date){
  let currentComponent = this;
  console.log(date);
  let startDate = currentComponent.ConvertDate(currentComponent.state.startDate);
  let endDate = currentComponent.ConvertDate(date);
  await axios.get(apiUrl.Report.CallOver + 'From/' + startDate + '/To/' + endDate)
  .then(function(response){
  console.log(response.data)
  let transaction = response.data.Transaction.filter(function(data){
    return data.IsWithdrawal === true || data.IsDeposit === true && data.CBA === "IMAL"
});
if(!localStorage.getItem("Branch").includes("ALL")){
transaction = transaction.filter(function(element){
  return element.BranchCode === localStorage.getItem("Branch")
})
}
currentComponent.setState({Transaction: transaction, OriginalTransaction: transaction})
  currentComponent.GenerateFileData(transaction);
  })

//   window.$('#sample_1').DataTable( {
//     destroy: true,
//     searching: false
// } );
window.$('#sample_1').DataTable( {
  "language": {
    "zeroRecords": " " //Change your default empty table message
    },
  retrieve: true,
  paging: true
});
 this.setState({ endDate: date})
}

async ImportToExcel(VaultDetails){
  console.log(VaultDetails)
       const multiDataSet = [
     
           {
       columns: [
        {title: "Date Treated", width: {wpx: 80}},
        {title: "Transaction Reference",width: {wpx: 80}},
         {title: "Debit Account", width: {wpx: 80}},//pixels width 
         {title: "Debit Customer", width: {wpx: 80}},//pixels width 
         {title: "Debit Amount", width: {wpx: 80}},//pixels width 
         {title: "Credit Account", width: {wpx: 80}},//pixels width 
         {title: "Credit Customer", width: {wpx: 80}},//pixels width 
         {title: "Credit Amount", width: {wpx: 80}},//pixels width 
         {title: "Inputter",width: {wpx: 80}},//char width 
         {title: "Approved By",width: {wpx: 80}},
         {title: "CBA",width: {wpx: 80}},
         {title: "Currency", width: {wpx: 80}}
       ],
       data: VaultDetails
     }
   ];
   // const columns =  [
   // 	{title: "", width: {wpx: 80}},//pixels width 
   // 	{title: "Precise Financial Systems", width: {wch: 70}, 
   // 	style: {font: {sz: "24", bold: true}, alignment:{horizontal:'center'}}},//char width 
   // 	{title: "", width: {wpx: 90}},
   // ]
       this.setState({multiDataSet:multiDataSet})
   }

   GeneratePDF = e =>{
    let currentComponent = this;
    console.log(currentComponent.state.transPDFdetails);
      const headers = createHeaders([
        "DateTreated",
        "TransactionReference",
        "DebitAccount",
        "DebitCustomer",
        "DebitAmount",
        "CreditAccount",
        "CreditCustomer",
        "CreditAmount",
        "Inputter",
        "ApprovedBy",
        "CBA",
        "currency"
      ]);
    var doc = new jsPDF({ putOnlyUsedFonts: true, orientation: "landscape" });
    doc.addImage(sterlingLogo, "PNG", 130, 10, 20, 20);
    doc.text("Sterling Bank", 130,40);
    doc.table(1, 60, currentComponent.state.transPDFdetails, headers, { autoSize: true,fontSize:5});
    doc.save("sterling_bank_call_over_report_" + "_from_" + this.ConvertDate(this.state.startDate) + "_to_" + this.ConvertDate(this.state.endDate) + ".pdf");
    }


ClearState = e => {
  this.setState({ CashWithDrawal:{}})
}


GetDate(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();

  today = dd  + '/' + mm + '/' + yyyy;
  console.log(today);
  return today
}
ConvertDateTime(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();
  var hours = today.getHours();
  var min = today.getMinutes();
  if(min === 0){
    min = "00"
  }
  let minute = "" + min;
  if(minute.length  === 1){
    min = "0" + min
  }
  today = dd + '/' + mm + '/' + yyyy + " " +  hours + ':' + min;
  console.log(today);
  return today
}
ConvertDate(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();
  today = yyyy + '-' + mm + '-' + dd;
  console.log(today);
  return today
}

GetDateForAPI(date){
  var today = new Date(date);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();
   var hours = today.getHours();
  var min = today.getMinutes();
  var sec = today.getSeconds();
  today = yyyy + '-' + mm + '-' + dd + 'T' + hours + ':' + min + ':' + sec + '+01:00';
  console.log(today);
  return today
}
  async componentDidMount(){
    loadAmountFormat();
   await GenerateToken();
    let currentComponent = this;
    let startDate = currentComponent.ConvertDate(currentComponent.state.startDate);
    let endDate = currentComponent.ConvertDate(currentComponent.state.endDate);
    await axios.get(apiUrl.Report.CallOver + 'From/' + startDate + '/To/' + endDate)
    .then(function(response){
    console.log(response.data)
    let transaction = response.data.Transaction.filter(function(data){
          return data.IsWithdrawal === true || data.IsDeposit === true && data.CBA === "IMAL"
    });
    if(!localStorage.getItem("Branch").includes("ALL")){
      transaction = transaction.filter(function(element){
        return element.BranchCode === localStorage.getItem("Branch")
   })
    }
    currentComponent.setState({Transaction: transaction, OriginalTransaction: transaction})
    currentComponent.GenerateFileData(transaction)
    })
    window.$('#sample_1').DataTable( {
      "language": {
        "zeroRecords": " " //Change your default empty table message
        },
      retrieve: true,
      paging: true
    });
    await axios.get(apiUrl.Security.GetCurrency)
    .then(function(response){
      let getDenominations = response.data.data;
      console.log(getDenominations);
      currentComponent.setState({CashDenominations:getDenominations})
    })
  }
  render(){
    return (
       <div>
       <NotificationContainer/>
      <section id="main-content">
      <section className="wrapper">
        <div className="row">
        <div className="col-md-12">
        <h3 className="page-title">
        Call Over Report <small></small>
        </h3>
        </div>
        </div>
        {
          this.state.ShowError === true ? <div  class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
              <b>Close</b></button>
          <span class="glyphicon glyphicon-hand-right"></span> <strong>{this.state.ErrorMessage}</strong>  </div>: ''
        }
        <div className="row">
				<div className="col-md-12">
					<div className="portlet box grey-cascade">
            <div className="portlet-title">
            <div className="caption">
            <i className="fa fa-info"></i> 
          </div>
          <div className="tools">
            <a href="javascript:;" className="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" className="config">
            </a>
            <a href="javascript:;" className="reload">
            </a>
            <a href="javascript:;" className="remove">
            </a>
          </div>
            </div>
            <div className="portlet-body" style={{height:'225px'}}>
              <div className="table-toolbar">
              <div style={{overflow:'scroll', height:'200px'}}>
								<div className="row">
									<div className="col-md-6">
                  <div className="portlet-body form">
                  <div className="form-body">
                  <div style={{paddingTop:'1px'}} className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                  <label for="form_control_1"><b style={{color:'#3c763d'}}>Date From</b></label>
                  <DatePicker
                  dateFormat="dd/MM/yyyy"
                  selected={this.state.startDate}
                  onChange={date => this.setStartDate(date)}
                  customInput={<ExampleCustomInput/>}
                />
                  </div>
                </div>
                <div className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                  <input  onChange={this.onChangeOfTellerID} type="text" className="form-control edited"/>
                  <label for="form_control_1"><b>Teller ID(Sender)</b></label>
                  <span style={{fontSize:'12px', color:this.state.TellerFieldColor}} className="help-block">{this.state.TellerFieldMessage}</span>
                  <i className="fa fa-key"></i>
                </div>
              </div>
                <div className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                <select className="form-control edited" onClick={this.ChangeCurrency}>
                <option id="0" value={0} disabled selected={true}>---Select Currency---</option>
                {
                  this.state.CashDenominations.map(x=>
                    <option id={x.Currency} value={x.Abbrev}>{x.Currency}</option>
                    )
                }
                </select>
                <label for="form_control_1"><b>Currency</b></label>
                <span style={{fontSize:'12px'}} className="help-block">Select the currency of the amount stipulated..</span>
              </div>
              </div>
                  </div>
                  </div>
                  </div>

                  <div className="col-md-6">
                  <div className="portlet-body form">
                  <div className="form-body">
                  <div style={{paddingTop:'1px'}} className="form-group form-md-line-input has-success form-md-floating-label">
                  <div className="input-icon right">
                  <label for="form_control_1"><b style={{color:'#3c763d'}}>Date To</b></label>
                  <DatePicker
                  dateFormat="dd/MM/yyyy"
                  selected={this.state.endDate}
                  onChange={date => this.setEndDate(date)}
                  customInput={<ExampleCustomInput/>}
                />
                  </div>
                </div>
                <div className="form-group form-md-line-input has-success form-md-floating-label">
                <div className="input-icon right">
                <select  className="form-control edited" onClick={this.ChangeCBA}>
                <option id="0" value={""} disabled selected={true}>---Select CBA---</option><option id="1" value={"T24"} >T24 </option><option id="2" value={"IMAL"} >IMAL</option>
                </select>
                <label for="form_control_1"><b>CBA</b></label>
                <span style={{fontSize:'12px'}} className="help-block">Select the currency of the amount stipulated..</span>
              </div>
              </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
            </div>
            </div>
            <div className="col-md-12">
            <div className="portlet box grey-cascade">
              <div className="portlet-title">
              <div className="caption">
              <i className="fa fa-info"></i>Transaction Details
            </div>
            <div className="tools">
              <a href="javascript:;" className="collapse">
              </a>
              <a href="#portlet-config" data-toggle="modal" className="config">
              </a>
              <a href="javascript:;" className="reload">
              </a>
              <a href="javascript:;" className="remove">
              </a>
            </div>
              </div>
              <div className="portlet-body">
                <div className="table-toolbar">
                <div style={{overflow:'scroll', height:'290px'}}>
                  <table className="table table-striped table-bordered table-hover" id="sample_1">
                  <thead>
                  <tr>
                  <th>
                  Date Treated
                </th>
                <th>
                Transaction Reference
              </th>
                    <th>
                     Debit Account
                    </th>
                    <th>
                     Debit Customer
                    </th>
                    <th>
                    Debit Amount
                   </th>
                    <th>
                  Credit Account
                       </th>
                   <th>
               Credit Customer
                   </th>
                   <th>
                    Credit Amount
                   </th>
                   <th>Inputter</th>
                   <th>
                   Approved By
                      </th>
                   <th>Currency</th>
                  <th>CBA</th>
                  </tr>
                  </thead>
                  <tbody>
                  {
                    this.state.Transaction.map(x=> 
                      <tr>
                      <td>{this.ConvertDateTime(x.CreationDate)}</td>
                      <td>{x.TransRef}</td>
                      <td>{x.IsWithdrawal ?   x.AccountNumber : x.BranchAccounts == null ? '' : x.BranchAccounts.DebitAccount}</td>
                      <td>{x.IsWithdrawal ? x.AccountName : 'Till Box'}</td>
                       <td style={{textAlign:'right'}}>{x.TotalAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                       <td>{x.IsDeposit ?   x.AccountNumber : x.BranchAccounts == null ? '' : x.BranchAccounts.CreditAccount}</td>
                      <td>{x.IsDeposit ? x.AccountName : 'Till Box'}</td>
                       <td style={{textAlign:'right'}}>{x.TotalAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                    <td>{x.TellerName}</td>
                      <td>{x.ApprovedBy}</td>
                      <td>{x.Currency}</td>
                      <td>{x.CBA}</td>
                      </tr>
                      
                      )
                  }
                  </tbody>
                  </table>
                    </div>
                    </div>
                    </div>
              </div>
              </div><br/>

            </div>
            <div style={{paddingLeft:'0px', textAlign:'right'}}>
            <ExcelFile filename={"sterling_call_over_report"+ "_from_" + this.ConvertDate(this.state.startDate) + "_to_" + this.ConvertDate(this.state.endDate)} element={<button  className="btn btn-primary">
						<i className="fa fa-file-excel-o"> </i>  Generate Excel</button>}>
						<ExcelSheet dataSet={this.state.multiDataSet} name={"Sterling Bank"}>
					</ExcelSheet>
								</ExcelFile> <button onClick={this.GeneratePDF}  type="button" className="btn btn-primary"><i class="fa fa-file-pdf-o"></i> Generate PDF  </button></div>
        </section>
        </section>

      </div>
    )
  }
}

export default CallOver