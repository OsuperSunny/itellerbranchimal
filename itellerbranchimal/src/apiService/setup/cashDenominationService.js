const CashDenominationService = (state = [], action) => {
   switch(action.type){
    case 'GetCashDenominations':
      state = GetCashDenominations().Get
    return state;
    default: return state;
   }
};

function GetCashDenominations(){
 return {
  Get:[
    {"Currency": "Naira", "Subunitname": "Kobo", "RelationshipUnit": 100,
    "Denominations":[{"Name": "1kobo", "Value": 0.100}, {"Name": "N5", "Value": 5.00},{"Name": "N10", "Value": 10.00},{"Name": "N100", "Value": 100.00},{"Name": "N200", "Value": 200.00},{"Name": "N500", "Value": 500.00},{"Name": "N1000", "Value": 1000.00}]
    },
    {"Currency": "Cedis", "Subunitname": "Pesewas", "RelationshipUnit": 100,
    "Denominations":[{"Name": "1 Pesewas", "Value": 0.100}, {"Name": "1 Cedi", "Value": 5.00},{"Name": "5 Cedis", "Value": 5.00},{"Name": "10 Cedis", "Value": 10.00},{"Name": "50 Cedis", "Value": 50.00},{"Name": "100 Cedis", "Value": 100.00},{"Name": "1000 Cedis", "Value": 1000.00}]
    },
    {"Currency": "Dollar", "Subunitname": "Cent", "RelationshipUnit": 100,
    "Denominations":[]
    },
    {"Currency": "Euro", "Subunitname": "Cent", "RelationshipUnit": 100,
    "Denominations":[]
    }
    ]
 };
} 

export default CashDenominationService