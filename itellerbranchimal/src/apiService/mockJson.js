const MockJson = {

  Role:[{"ID": 1, "RoleName": "System Admin"},
  {"ID": 2, "RoleName": "Cashier"}],
  Modules:[{ID:1, Name:'Setup'}, {ID:2, Name:'Management'}, {ID:3, Name:'Vault Movement'},{ID:3, Name:'Transaction'}],
  Resources:[{ModuleID: 1, Resources:[{ID:1, Name:'User'},{ID:1, Name:'Till'},{ID:2, Name:'Branch'},{ID:3, Name:'Role Resources'},{ID:4, Name:'GL Account'}]},
  {ModuleID: 2, Resources:[{ID:5, Name:'User'},{ID:2, Name:'Till'},{ID:3, Name:'Branch'},{ID:4, Name:'Role Resources'},{ID:5, Name:'GL Account'}]}
],
Branch:[{ID:1,BranchCode:"NG100006", BranchName:"Lekki", BranchAddress:"No 2 adetokunbo street lekki", SortCode:"12345678", InterBranchAccount:"1234567890", SuspenseAccount:"1234567890", Status: true},{ID:2,BranchCode:"NG100002", BranchName:"Yaba", BranchAddress:"yaba", SortCode:"12345678", InterBranchAccount:"1234567890", SuspenseAccount:"1234567890", Status: true}],
  GetCashDenominations:[
    {"ID":1,"Currency": "Naira", "Subunitname": "Kobo", "RelationshipUnit": 100,
    "Denominations":[{"ID": 1,"Name": "1kobo", "Value": 0.01}, {"ID": 2,"Name": "N5", "Value": 5.00},{"ID": 3,"Name": "N10", "Value": 10.00},{"ID": 4,"Name": "N100", "Value": 100.00},{"ID": 5,"Name": "N200", "Value": 200.00},{"ID": 6,"Name": "N500", "Value": 500.00},{"ID": 7,"Name": "N1000", "Value": 1000.00}]
    },
    {"ID":2,"Currency": "Cedis", "Subunitname": "Pesewas", "RelationshipUnit": 100,
    "Denominations":[{"ID": 1,"Name": "1 Pesewas", "Value": 0.100}, {"ID": 2,"Name": "1 Cedi", "Value": 5.00},{"ID": 3,"Name": "5 Cedis", "Value": 5.00},{"ID": 4,"Name": "10 Cedis", "Value": 10.00},{"ID": 5,"Name": "50 Cedis", "Value": 50.00},{"ID": 6,"Name": "100 Cedis", "Value": 100.00},{"ID": 7,"Name": "1000 Cedis", "Value": 1000.00}]
    },
    {"ID":3,"Currency": "Dollar", "Subunitname": "Cent", "RelationshipUnit": 100,
    "Denominations":[]
    },
    {"ID":4,"Currency": "Euro", "Subunitname": "Cent", "RelationshipUnit": 100,
    "Denominations":[]
    }
    ],
    GetCashWithDrawal: [
      {"AccountNumber": "0201234567", "AccountStatus": "Active", "Branch": "Lagos", "CustomerName": "Aro Muyiwa", "Phone": "07065949501", "EffectiveBalance": 10000000.00,"LedgerBalance": 15000000.00, "SweepBalance": 10000000.00,"Beneficiary":"Muyiwa Aro","Narration":"Payment for contract work","WithdrawalAmount":5000000.00,"Currency":"Naira","AccountType":"Savings","BenefAccount": "0213456785","ChequeNumber":"450002", "Denominations":[ {"ID": 2,"Name": "N5","Value": 5.00, "Amount": 0.00},{"ID": 3,"Name": "N10","Value": 10.00, "Amount": 0.00},{"ID": 4,"Name": "N100","Value": 100.00, "Amount": 0.00},{"ID": 5,"Name": "N200","Value": 200.00, "Amount": 0.00},{"ID": 6,"Name": "N500","Value": 500.00, "Amount": 0.00},{"ID": 7,"Name": "N1000","Value": 1000.00, "Amount": 0.00}]
      }, {"AccountNumber": "0207654321", "AccountStatus": "Active", "Branch": "Lagos", "Currency":"Naira","CustomerName": "Sunday Oladiran", "Phone": "07065949501", "EffectiveBalance": 20000000.00,"LedgerBalance": 25000000.00, "SweepBalance": 20000000.00,"Beneficiary":"Sunday Oladiran","Narration":"Payment for contract work","WithdrawalAmount":7000000.00,"AccountType":"Current","BenefAccount": "0213456785", "ChequeNumber":"450003","Denominations":[ {"ID": 3,"Name": "N5","Value": 5.00, "Amount": 0.00},{"ID": 3,"Name": "N10","Value": 10.00, "Amount": 0.00},{"ID": 4,"Name": "N100","Value": 100.00, "Amount": 0.00},{"ID": 5,"Name": "N200","Value": 200.00, "Amount": 0.00},{"ID": 6,"Name": "N500", "Value": 500.00,"Amount": 0.00},{"ID": 7,"Name": "N1000","Value": 1000.00, "Amount": 0.00}]
    }
    ],
    GetCheque: [
      {"AccountNumber": "0201234567", "AccountStatus": "Active", "Branch": "Lagos", "CustomerName": "Aro Muyiwa", "Phone": "07065949501", "EffectiveBalance": 10000000.00,"LedgerBalance": 15000000.00, "SweepBalance": 10000000.00,"Beneficiary":"Muyiwa Aro","Narration":"Payment for contract work","WithdrawalAmount":5000000.00,"ChequeNumber":"450002", "Denominations":[]
      }, {"AccountNumber": "0207654321", "AccountStatus": "Active", "Branch": "Lagos", "CustomerName": "Sunday Oladiran", "Phone": "07065949501", "EffectiveBalance": 20000000.00,"LedgerBalance": 25000000.00, "SweepBalance": 20000000.00,"Beneficiary":"Sunday Oladiran","Narration":"Payment for contract work","WithdrawalAmount":7000000.00, "ChequeNumber":"450003","Denominations":[]
    }
    ],

    GetTill:[
      {"Selected":false,"TillNumber": "T101", "TillDescription": "First","GLAcctNo":"0201234567", "GLAccName": "Newspaper and Magazine", "GLAccCurrency": "Naira", "MinTillAmount": 10000000.00, "MaxTillAmount": 20000000.00},
      {"Selected":false,"TillNumber": "T102", "TillDescription": "First","GLAcctNo":"0201234567", "GLAccName": "Cashier Reserved Till", "GLAccCurrency": "Dollar", "MinTillAmount": 10000000.00, "MaxTillAmount": 20000000.00},
      {"Selected":false,"TillNumber": "T103", "TillDescription": "First","GLAcctNo":"0201234567", "GLAccName": "Cahier 1 Till", "GLAccCurrency": "Naira", "MinTillAmount": 10000000.00, "MaxTillAmount": 20000000.00},
      {"Selected":false,"TillNumber": "T104", "TillDescription": "First","GLAcctNo":"0201234567", "GLAccName": "Computer stationaries", "GLAccCurrency": "Naira", "MinTillAmount": 10000000.00, "MaxTillAmount": 20000000.00},
      {"Selected":false,"TillNumber": "T105", "TillDescription": "First","GLAcctNo":"0201234567", "GLAccName": "Entertainment", "GLAccCurrency": "Naira", "MinTillAmount": 10000000.00, "MaxTillAmount": 20000000.00}
    ],
    UserTill:[
      {"ID":1,"IsNew": false,"Disabled": true,"Selected":false,"TillNumber": "T101", "TillDescription": "First","GLAccCurrency": "Naira","UserName":"Muyiwa Aro", "TransactionLimit": 10000000.00},
      {"ID":2,"IsNew": false,"Disabled": true,"Selected":false,"TillNumber": "T102", "TillDescription": "First","GLAccCurrency": "Naira","UserName":"Sunday Oladiran", "TransactionLimit": 20000000.00},
      {"ID":3,"IsNew": false,"Disabled": true,"Selected":false,"TillNumber": "T103", "TillDescription": "First","GLAccCurrency": "Naira","UserName":"Tosin Olabisi", "TransactionLimit": 15000000.00},
      {"ID":4,"IsNew": false,"Disabled": true,"Selected":false,"TillNumber": "T104", "TillDescription": "First","GLAccCurrency": "Naira","UserName":"Seye Isola", "TransactionLimit": 17000000.00},
      {"ID":5,"IsNew": false,"Disabled": true,"Selected":false,"TillNumber": "T105", "TillDescription": "First","GLAccCurrency": "Naira","UserName":"Alfred Illoh", "TransactionLimit": 18000000.00}
    ],
    AutoCompleteGLAccount:[
      { value: '0201234567', label: '0201234567 (Cashier 1 Reserved Till)' },
      { value: '0301234568', label: '0301234568 (Cashier 2 Reserved Till)' },
      { value: '0501934567', label: '0501934567 (Cashier 3 Reserved Till)' },
      { value: '0201234562', label: '0201234562 (Cashier 4 Reserved Till)' },
      { value: '0211234561', label: '0211234561 (Cashier 5 Reserved Till)' },
      { value: '0231234568', label: '0231234568 (Cashier 6 Reserved Till)' },
      { value: '0301234564', label: '0301234564 (Cashier 7 Reserved Till)' },
      { value: '0201234579', label: '0201234579 (Cashier 8 Reserved Till)' },
      { value: '0261234566', label: '0261234566 (Cashier 9 Reserved Till)' },
      { value: '0251234543', label: '0251234543 (Cashier 10 Reserved Till)' },
      { value: '0201234511', label: '0201234511 (Cashier 11 Reserved Till)' },
    ],
    GLAccount:[
      { GLAccountNumber: '0201234567', GLAccChannel: 'Naira' },
      { GLAccountNumber: '0301234568', GLAccChannel: 'Naira' },
      { GLAccountNumber: '0501934567', GLAccChannel: 'Pounds' },
      { GLAccountNumber: '0201234562', GLAccChannel: 'Naira' },
      { GLAccountNumber: '0211234561', GLAccChannel: 'Naira' },
      { GLAccountNumber: '0231234568', GLAccChannel: 'Dollar' },
      { GLAccountNumber: '0301234564', GLAccChannel: 'Dollar' },
      { GLAccountNumber: '0201234579', GLAccChannel: 'Naira' },
      { GLAccountNumber: '0261234566', GLAccChannel: 'Naira' },
      { GLAccountNumber: '0251234543', GLAccChannel: 'Cedis' },
      { GLAccountNumber: '0201234511', GLAccChannel: 'Naira' },
    ],
    AutoCompleteUsers:[
      { value: 'U101', label: 'Muyiwa Aro' },
      { value: 'U101', label: 'Sunday Oladiran' },
      { value: 'U101', label: 'Tosin Olabisi' },
      { value: 'U101', label: 'Alfred Illoh' },
      { value: 'U101', label: 'Seye Isola' }
    ],
    AutoCompleteTill:[
      { value: 'T101', label: 'T101 (Till desc sample...)',  },
      { value: 'T102', label: 'T102 (Till desc sample...)' },
      { value: 'T102', label: 'T103 (Till desc sample...)' },
      { value: 'T104', label: 'T104 (Till desc sample...)' },
      { value: 'T105', label: 'T105 (Till desc sample...)' },
      { value: 'T106', label: 'T106 (Till desc sample...)' },
      { value: 'T107', label: 'T107 (Till desc sample...)' },
      { value: 'T108', label: 'T108 (Till desc sample...)' },
      { value: 'T109', label: 'T109 (Till desc sample...)' },
      { value: 'T110', label: 'T110 (Till desc sample...)' },
      { value: 'T111', label: 'T111 (Till desc sample...)' },
      { value: 'T112', label: 'T112 (Till desc sample...)' },
      { value: 'T1113', label: 'T1113 (Till desc sample...)' }
    ]
}

export default MockJson