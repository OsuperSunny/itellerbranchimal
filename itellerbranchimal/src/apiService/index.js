import CashDenominationService from './setup/cashDenominationService';
import {combineReducers} from 'redux'
const AllServices = combineReducers({
  CashDenominationService
})

export default AllServices