import React from "react";
import axios from 'axios';
import $ from 'jquery';

//redirect to login page if 401 unauthorized
// axios.interceptors.response.use(response => {
//     return response;
//   }, error => {
//   if (error.response.status === 401) {
//     window.location.href ='../../source/admin/extra_lock.html'
//   }
// });

// //if user does not exist
// if ("Username" in localStorage) {
// }else{
//     window.location.href = '/login.html'
// }

if ("LogIn" in localStorage) {
}else{
window.location.href = '/login.html'
sessionStorage.clear();
}
if(sessionStorage.getItem('IsLocked') === 'true'){
window.location.href = '/lock_screen.html'
sessionStorage.clear();
}
// if ("IsLocked" in sessionStorage) {
// }else{
// window.location.href = '/lock_screen.html'
// sessionStorage.clear();
// }

// if ("LogIn" in localStorage) {
// }else{
//     window.location.href = '/login.html'
// }
// if(localStorage.getItem('IsLocked') === 'true'){
//   window.location.href = '/lock_screen.html'
// }
// API documentation url: https://payitlite.docs.apiary.io/#reference/0/security/create-new-roles?console=1
//mock api domain = https://private-anon-62b09f4bd3-payitlite.apiary-mock.com/
// const AUTH_TOKEN = 'bearer ' + localStorage.getItem('Token');
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
  // Request headers you wish to allow
    const apiDomain =    localStorage.getItem('Burl');       //"https://private-anon-62b09f4bd3-payitlite.apiary-mock.com/"   
     localStorage.setItem('Burl', apiDomain);
     let userdata = {AD_Username: localStorage.getItem('Id')};
     axios.post(apiDomain + 'User/GetDetails',userdata )
     .then(function(response){
       let apiResponse = response.data.data.UserDetail
       console.log(apiResponse);
       localStorage.setItem('UserName', apiResponse.UserName);
       localStorage.setItem('Email', apiResponse.Email);
       localStorage.setItem('Role', apiResponse.Role);
     })
    const controllers = {
        Roles: "/Role",
        User: "/User",
        Users: "/Users",
        Currency: "Currency",
        Till: "Till"
    };
    
    const apiServices = {
             Security: {
                GetToken: apiDomain + 'token',
                GetCurrency: apiDomain +controllers.Currency,
                CreateCurrency: apiDomain + 'Create/' +  controllers.Currency,
                UpdateCurrency: apiDomain + 'Update/' +  controllers.Currency,
                ValidateOTP: apiDomain + 'OTP/Transaction'
             },
             Setup: {
                GetTill: apiDomain  + controllers.Till,
                CreateTill: apiDomain + 'Create/' + controllers.Till,
                UpdateTill: apiDomain + 'Update/' + controllers.Till,
                GetUserTill: apiDomain + 'User/' + controllers.Till,
                AssignTill: apiDomain + 'Assign/' + controllers.Till,
                DeleteUserTill: apiDomain + 'Delete/Assign/' + controllers.Till,
                GetUsers: apiDomain + 'Users',
                CreateUser: apiDomain + 'Create/Users',
                UpdateUser: apiDomain + 'Update/Users',
                GetGiverTill: apiDomain + 'Giver/Till',
                RoleResources: apiDomain + 'RoleResource',
                UpdateRole: apiDomain + 'Update/Role',
                CreateRole: apiDomain + 'Create/Role',
                DeleteRole:apiDomain + 'Delete/Role',
                UpdateResources: apiDomain + 'Assign/Role',
                SystemConfiguration: apiDomain + 'SystemConfiguration/'
            },
            Management:{
              RequestTill: apiDomain + 'Request/TillTransfer',
              GetRequestedTill: apiDomain + 'ImalGetRequest/TillTransfer/',
              GetTillTransaction: apiDomain + 'ImalTellerTransaction',
              ConfirmBalance: apiDomain + 'ConfirmBalance/Till'
            },
            BankService:{
              CustomerDetails: apiDomain + 'ImalNameEnquiry/',
              ChequeDetails: apiDomain + 'ChequeEnquiry/',
              GetMandate: apiDomain + 'GetImalAccountMandate',
              CreateCashWithdrawal: apiDomain + 'ImalTransaction/Create/CashWithdrawal',
              CreateWithdrawalApproval: apiDomain + 'Create/Imal/TempCashWithdrawal',
              CreateDepositApproval: apiDomain + 'Create/IMAL/TempCashDeposit',
              CreateCashDeposit: apiDomain + 'ImalTransaction/Create/CashTrans',
              CreateVaultIn: apiDomain + 'ImalTransaction/Create/VaultIn',
              CreateVaultOut: apiDomain + 'ImalTransaction/Create/VaultOut',
              GetTransaction: apiDomain + 'getCashTrans',
              AcceptTransferTill: apiDomain + 'Approve/ImalTransaction/TillTransfer',
              RejectTill: apiDomain + 'Reject/TillTransferImal',
              ReverseTransaction: apiDomain + 'Transaction/TellerReversal',
              GenerateToken: apiDomain + 'Token',
              GetUserDetails: apiDomain + 'User/UserDetails?',
              OpenTill: apiDomain + 'Open/Till',
              CloseTill: apiDomain + 'Close/Till',
              TillTransfer: apiDomain + 'ImalTransaction/TillTransfer',
              GetTellerDetails: apiDomain + 'TellerAccount/'
            },
            Approval:{
              GetTillApproval: apiDomain + 'Get/Approve/Till',
              ApproveTill: apiDomain + 'Approve/Till',
              DisapproveTill: apiDomain + 'Disapprove/Till',
              ApproveTransaction: apiDomain + 'ImalTransaction/Approve/CashTrans',
              DisapproveTransaction: apiDomain +  'Disapprove/Transaction'
            },
            Report:{
              VaultTransaction: apiDomain + 'Report/VaultTransaction/',
              TillTransfer: apiDomain + 'Report/GetTillReport/',
              TellerCast: apiDomain + 'Report/GetTellerCastReport/',
              CallOver: apiDomain + 'Report/GetPostedCallOverReport/',
              Transaction: apiDomain + 'Report/GetTransactionReport/',
              AuditTrail: apiDomain + 'Report/GetAuditReport/'
            }
    };




export default apiServices