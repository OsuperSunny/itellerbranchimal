import React from 'react';
import apiUrl from '../apiService/config'
import axios from 'axios';

async function GenerateToken(){
  let token = "";
  await axios.get(apiUrl.BankService.GenerateToken)
   .then(function(response){
     if(response.data.success){
      console.log(response.data);
      localStorage.setItem('access_token', response.data.details.access_token)
     }else
     {
      console.log(response.data);
     }
   
  }).catch(function(error){
      console.log(error);
  })
}

export default GenerateToken;