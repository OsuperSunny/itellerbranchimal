import React from 'react';
import apiUrl from '../apiService/config'
import axios from 'axios';

async function CheckLogin(){
 // window.location.href =   '../../lock_screen.html'
  let token = "";
  let data = {UserId: localStorage.getItem('Id'), Branch:""};
  await axios.post(apiUrl.Setup.UserLogin, data)
   .then(function(response){
    console.log("Created user Login Activity",response.data);
    if(Date.parse(response.data.LastLoginDate) > Date.parse(sessionStorage.getItem('LastLoginDate'))){
      sessionStorage.setItem('IsLocked', 'true');
      window.location.href =   '../../lock_screen.html'
      
    }
  }).catch(function(error){
      console.log(error);
  })
 
}


export default CheckLogin;
