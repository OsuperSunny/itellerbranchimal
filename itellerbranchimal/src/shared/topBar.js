import React, {Component} from "react";
import sterlingLogo from '../sterling.png'
import sterlingFullLogo from '../sterling-logo-balanced-dark.png'
class TopBar extends Component {
  LogOut = e =>{
     window.location.href = '../../login.html'
     localStorage.clear();
  }
  LockScreen = e =>{
    window.location.href = '../../lock_screen.html'
    localStorage.setItem('IsLocked', 'true');
  }
  render(){
    return (
<div>
    <header class="header black-bg">
    <div class="sidebar-toggle-box">
      <div style={{color:'white'}} class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
    </div>
    <a href="index.html" class="logo"><b style={{fontSize:'19px'}}> iTeller<span>Branch</span></b></a>
    <div class="nav notify-row" id="top_menu">
      <ul class="nav top-menu">
      <li><span style={{paddingLeft:'500px',color:'white',fontSize:'17px',fontWeight:'bold'}}>IMAL</span></li>
      </ul>
    </div>
    <div class="top-menu">
    <ul  style={{paddingTop:'12px'}} class="nav pull-right top-menu"><li className="logout"><img style={{height:'40px'}} src={sterlingFullLogo}/></li></ul>
      <ul className="nav pull-right top-menu" >
        <li  ><a style={{backgroundColor:'#808080'}} onClick={this.LogOut} className="logout">Logout</a></li>
      </ul>
      <ul class="nav pull-right top-menu">
      <li><a style={{backgroundColor:'#808080'}} onClick={this.LockScreen} class="logout"><i className="fa fa-lock"></i></a></li> 
    </ul>
   
    </div>
  </header>
</div>
    );
  }
}

export default TopBar