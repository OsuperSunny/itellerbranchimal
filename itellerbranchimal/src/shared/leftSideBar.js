import React, {Component} from "react";
import userLogo from '../user.png';
import {NavLink} from 'react-router-dom';
import axios from 'axios';
import apiUrl from '../apiService/config'
import GenerateToken from "./token";
import CheckLogin from "./checkLogin";
const userName = localStorage.getItem('UserName')
class LeftSideBar extends Component {
  constructor (props){
    super(props)
    this.state = {
      IsInputer:4,
      UserName:'',
      RoleResources:[]
}
}
async componentDidMount(){
  let currentComponent = this;
  await CheckLogin();
  await GenerateToken();
  await axios.get(apiUrl.BankService.GetUserDetails + 'userId=' + localStorage.getItem("Id") +  '&access_token=' + localStorage.getItem('access_token'))
  .then(function(response){
    console.log(response.data);
    if(response.data.success){
      currentComponent.setState({ UserName: response.data.UserDetails.UserName})
      if(response.data.UserDetails.UserRole.includes("ALL")){
        currentComponent.setState({IsInputer: 0})
        localStorage.setItem("Branch","ALL")           
     }else if(response.data.UserDetails.UserRole.includes("INP")){
        currentComponent.setState({IsInputer: 1})  
        localStorage.setItem("Branch",response.data.UserDetails.UserBranch)
     }else if(response.data.UserDetails.UserRole.includes("MGR") ){
       currentComponent.setState({IsInputer: 2})   
       localStorage.setItem("Branch",response.data.UserDetails.UserBranch) 
     } 
     else if(response.data.UserDetails.UserRole.includes("AUDTCTRLC") ){
       currentComponent.setState({IsInputer: 3})   
       localStorage.setItem("Branch","ALL") 
     } 
    }
    
  }).catch(function(error){
    console.log(error);
  })


  await axios.get(apiUrl.Setup.RoleResources)
  .then(function(response){
    console.log(response.data);
    let resources = []
    let roleName = '';
    if(currentComponent.state.IsInputer === 0)
    roleName = "Admin"
    else if(currentComponent.state.IsInputer === 1)
    roleName = "Inputer"
    else if(currentComponent.state.IsInputer === 2)
    roleName = "Authorizer"
    else if(currentComponent.state.IsInputer === 3)
    roleName = "Auditor"
    else
    roleName = ""
    console.log(roleName)
    let roleResources = response.data.data.RoleResources.filter(function(data){
         return   data.RoleDesc === roleName
    })
    roleResources.forEach(element => {
      resources.push(element.ResourceName);
    });
    console.log(resources);
    currentComponent.setState({RoleResources:resources});
  }).catch(function(error){
    console.log(error);
  })

 

}
  render(){
    return (
      <div>
    <aside>
    <div id="sidebar" className="nav-collapse ">
      <ul className="sidebar-menu" id="nav-accordion">
        <p className="centered"><img src={userLogo} className="img-circle" width="80"/></p>
        <h5 className="centered">{this.state.UserName}</h5>
        <li className="mt">
          <NavLink to="/" className="active">
            <i className="fa fa-dashboard"></i>  <span>Dashboard</span></NavLink>
        </li>
        {
          this.state.RoleResources.includes("Audit Trail")  ?
        <li  className="sub-menu">
          <NavLink to="/pages/auditTrail" >
            <i className="fa fa-list"></i>  <span>Audit Trail</span></NavLink>
        </li>:''
        }
        {
          this.state.RoleResources.includes("Balance") ||   this.state.RoleResources.includes("Balance") ?
          <li className="sub-menu">
          <NavLink to="/pages/vaultMovement/vaultIn" className="active">
            <i className="fa fa-exchange"></i>  <span>Vault Movement</span></NavLink>
         </li> :""

        }
      
        <li className="sub-menu">
        <a href="javascript:;">
          <i className="fa fa-tasks"></i>  <span>Management</span>
          </a>
        
          <ul className="sub">
           {
            this.state.RoleResources.includes("Balance")  ?
          <li>
          <NavLink to="/pages/management/balance"> <i className="fa fa-balance-scale"></i> Check Balance</NavLink>
          </li>:''
          }
              {
            this.state.RoleResources.includes("Request Till")  ?
           <li>
            <NavLink to="/pages/management/transferTill"> <i className="fa fa-exchange"></i>  Till Transfer</NavLink>
            </li> :''
          }
        </ul>
        
        
      </li>
        
        <li className="sub-menu">
        <a href="javascript:;">
          <i className="fa fa-credit-card"></i>  <span>Transaction</span>
          </a>
         
            <ul className="sub">
            {
              this.state.RoleResources.includes("Cash Withdrawal")  ?
            <li>
            <NavLink to="/pages/cashWithdrawal">
              <i className="fa fa-money"></i>  <span>Cash Withdrawal</span>
              </NavLink>
          </li>:''
            }
          
          {
              this.state.RoleResources.includes("Cash Withdrawal Cheque")  ?
          <li>
          <NavLink to="/pages/cashwithdrawalCheque">
            <i className="fa fa-money"></i>  <span style={{fontSize:'11px'}}>Cash Withdrawal(Cheque)</span>
            </NavLink>
        </li>:''
            }
            {
              this.state.RoleResources.includes("Cash Deposit")  ?
          <li>
          <NavLink to="/pages/depositWithdrawal">
            <i className="fa fa-bank"></i>  <span>Deposit</span>
            </NavLink>
        </li>:''
            }
       
       {
              this.state.RoleResources.includes("Reverse Transaction")  ? 
        <li>
        <NavLink to="/pages/reverseTransaction">
          <i className="fa fa-arrow-circle-o-left"></i>  <span>Reverse Transaction</span>
          </NavLink>
      </li>:''
            }
            </ul>
       
      </li>


      <li className="sub-menu">
      <a href="javascript:;">
        <i className="fa fa-thumbs-up"></i>  <span>Approvals</span>
        </a>
      <ul className="sub">
      {
        this.state.IsInputer === 3 ? '':
        <li>
        <NavLink to="/pages/Approval/acceptTillTransfer">
       <i className="fa fa-thumbs-up"></i>  <span>Accept Till Transfer</span>
       </NavLink>
   </li>
      }
      {
        this.state.IsInputer === 3 ? '' : this.state.IsInputer === 1 ? '' :
        <li>
        <NavLink to="/pages/Approval/transactionApproval">
          <i className="fa fa-money"></i>  <span>Transaction</span>
          </NavLink>
      </li>
      }
      </ul>
    </li>

    <li className="sub-menu">
    <a href="javascript:;">
      <i className="fa fa-area-chart"></i>  <span>Reports</span>
      </a>
      {
        this.state.IsInputer === 3 ? '' :
        <ul className="sub">
        <li>
          <NavLink to="/pages/Reports/vaultReport">
          <i className="fa fa-bar-chart"></i>  <span>Vault Transactions</span>
          </NavLink>
        </li>
        <li>
        <NavLink to="/pages/Reports/tillTransferReport">
          <i className="fa fa-line-chart"></i>  <span>Till Transfer</span>
          </NavLink>
        </li>
        <li>
        <NavLink to="/pages/Reports/tellerCast">
          <i className="fa fa-area-chart"></i>  <span>Teller Cast</span>
          </NavLink>
        </li>
        <li>
        <NavLink to="/pages/Reports/callOver">
          <i className="fa fa-bookmark"></i>  <span>Call Over</span>
          </NavLink>
        </li>
        <li>
        <NavLink to="/pages/Reports/transactionReport">
          <i className="fa fa-line-chart"></i>  <span>Transaction</span>
          </NavLink>
        </li>
            </ul> 
      }
   
  </li>
    

      </ul>
    </div>
  </aside>
      </div>
    )
  }
}

export default LeftSideBar