
import pdfMake from 'pdfmake/build/pdfmake';
import vfsFonts from 'pdfmake/build/vfs_fonts';
import sterlingLogo from '../sterling.png'
var formattedData = [];

function formatElements(object) {
	var formattedArray = [];
	for (const key in object) {
		if (object.hasOwnProperty(key)) {
			const element = object[key];
			formattedArray.push({ text: element })
		}
	}
	return (formattedArray);
}
const _format = (data) => {
	 
	return data.map((item, key) => {
	return formatElements(item);
		 
	});

}

export default (pdfData, columnNames, reportName) => {
	const { vfs } = vfsFonts.pdfMake;
	pdfMake.vfs = vfs;


	const formattedData = _format(pdfData);
	console.log(formattedData);
	const documentDefinition = {
		pageSize: 'A4',
		pageOrientation: 'landscape',
		content: [
			{
				stack: [
					{
						image: sterlingLogo,
						width: 10,
						height: 10,
						fit: [50, 50]
					},

					{ text: 'Sterling Bank', style: 'subheader' },
				],
				style: 'header',
			},
			{
				columns: [
					{ width: '*', text: '' },
					{
						width: 'auto',
						table: {
							headerRows: 1,
							// widths: ["16%","16%","16%","16%","16%","16%","16%","16%"],
							width: 'auto',
							dontBreakRows: true,
							body: [
								columnNames,
								...formattedData,
							],
							alignment: "center"
						},
						style: 'table',

					},
					{ width: '*', text: '' },
				]
			},

			{

			}
		],
		styles: {
			header: {
				fontSize: 18,
				bold: true,
				alignment: 'center',
				margin: [0, 10, 0, 40]
			},
			subheader: {
				fontSize: 14,
				margin:[0,-22,-153,0]
			},
			superMargin: {
				margin: [0, 0, 0, 0],
				fontSize: 15
			},
			table: {
				margin: [0, 0, 0, 0],
				fontSize: 6,
			},
			tableHeader: {
				bold: true,
				fontSize: 9,
				color: 'black'
			}
		}
	};

	pdfMake.createPdf(documentDefinition).download('sterling_bank_' + reportName);
}