import React from 'react';
import jQuery from 'jquery';
import $ from 'jquery';
import '../scripts/dataTables.bootstrap'

$.DataTable = require('datatables.net');


function Datatable(){
	var table = $('#sample_1'); 
	table.dataTable({
		"language": {
			"aria": {
				"sortAscending": ": activate to sort column ascending",
				"sortDescending": ": activate to sort column descending"
			},
			"emptyTable": "No data available in table",
			"info": "Showing _START_ to _END_ of _TOTAL_ records",
			"infoEmpty": "No records found",
			"infoFiltered": "(filtered1 from _MAX_ total records)",
			"lengthMenu": "Show _MENU_ records",
			"search": "Search:",
			"zeroRecords": "No matching records found",
			"paginate": {
				"previous":"Prev",
				"next": "Next",
				"last": "Last",
				"first": "First"
			}
		},
		"bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

	
		"lengthMenu": [
			[5, 15, 20, -1],
			[5, 15, 20, "All"] // change per page values here
		],
		// set the initial value
		"pageLength": 5,            
		"pagingType": "bootstrap_full_number",
		"columnDefs": [{  // set default column settings
			'orderable': false,
			'targets': [0]
		}, {
			"searchable": false,
			"targets": [0]
		}],
		"order": [
			[1, "asc"]
		] // set first column as a default sort by asc
	});
	var tableWrapper = jQuery('#sample_1_wrapper');

	table.find('.group-checkable').change(function () {
		var set = jQuery(this).attr("data-set");
		var checked = jQuery(this).is(":checked");
		jQuery(set).each(function () {
			if (checked) {
				$(this).attr("checked", true);
				$(this).parents('tr').addClass("active");
			} else {
				$(this).attr("checked", false);
				$(this).parents('tr').removeClass("active");
			}
		});
		jQuery.uniform.update(set);
	});
	table.on('change', 'tbody tr .checkboxes', function () {
		$(this).parents('tr').toggleClass("active");
	});
	tableWrapper.find('.dataTables_length select').addClass("form-control input-xsmall input-inline"); // modify table per page dropdown
}

 
export default Datatable;