import React, {Component} from "react";
class Footer extends Component {
  render(){
    return (
<div>
<footer class="site-footer">
<div class="text-center">
  <p>
    &copy; Copyrights <strong> 2020 Precise Financial Systems Ltd</strong>. All Rights Reserved
  </p>
  <div class="credits">
    Visit us @<a href="https://thepfs.biz/">thepfs.biz</a>
  </div>
  <a href="index.html#" class="go-top">
    <i class="fa fa-angle-up"></i>
    </a>
</div>
</footer>
</div>
    );
  }
}

  export default Footer