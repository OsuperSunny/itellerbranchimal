import React from 'react';
import ReactExport from 'react-data-export';
import GenerateToken from './token';
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
 
function formatElements(object) {
  var formattedArray = [];
  for (const key in object) {
    if (object.hasOwnProperty(key)) {
      var element = object[key];
      if (element == null) {
        element = "";
      }
      formattedArray.push({ value: element, style: { } })
      // formattedArray.push(element )
    }
  }

  return formattedArray;
}

const _format = (data) => {

  return data.map((item, key) => {
   return formatElements(item);
  });

}
function GenerateExcel(data) {
 
  const multiDataSet = [
    {

      columns: data.columns,
      data: _format(data.data)
      
    },

  ]

  console.log(multiDataSet)
  return (
    <ExcelFile filename={"sterling_bank_transactionReport"} element={<button className="btn btn-primary">
      <i className="fa fa-file-excel-o"> </i>  Generate Excel</button>}>
      <ExcelSheet dataSet={multiDataSet} name={"Sterling Bank"}>
      </ExcelSheet>
    </ExcelFile> 
  );

}
export default GenerateExcel
